const globalData = {}

export const global = {
  url: 'https://www.uniwebmarketing.com/images/capitaland/',
  live: 'https://www.uniwebmarketing.com/capitaland-server/api/files/image/'
}

export function set (key, val) {
  globalData[key] = val
}

export function get (key) {
  return globalData[key]
}
