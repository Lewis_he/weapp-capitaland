export { default as ABS } from './atomos-bg.svg';
export { default as ALS } from './atomos-l.svg';
export { default as COS } from './circle-oval.svg';
export { default as CLV } from './capitaland-logo.svg';
export { default as CAPLL } from './capll-logo.svg';
export { default as CAPLM } from './caplm-logo.svg';
export { default as CAPLS } from './capls-logo.svg';

export { default as MS } from './merchant-search.svg';
export { default as MSA } from './merchant-search-active.svg';
export { default as MI } from './mall-summary.svg';
export { default as MIA } from './mall-summary-active.svg';
export { default as TR } from './traffic-routes.svg';
export { default as TRA } from './traffic-routes-active.svg';
export { default as ES } from './equipment-service.svg';
export { default as ESA } from './equipment-service-active.svg';
export { default as PF } from './parking-fee.svg';
export { default as PFA } from './parking-fee-active.svg';
export { default as ML } from './mall-locale.svg';
export { default as MO } from './mall-operating.svg';
export { default as MR } from './map-right.svg';

export { default as MRT } from './mrt.svg';
export { default as TAXI } from './taxi.svg';
export { default as BUS } from './bus.svg';
export { default as P } from './p.svg';

export { default as WIFI } from './wifi.svg';
export { default as CSS } from './customer-service.svg';
export { default as NR } from './nursing-room.svg';
export { default as FS } from './family-service.svg';
export { default as ATM } from './atm.svg';
export { default as CE } from './currency-exchange.svg';
export { default as CLINIC } from './clinic.svg';
export { default as DCS } from './dry-cleaner.svg';
export { default as RS } from './repair-shop.svg';

export { default as PARKING } from './parking.svg';
export { default as CAR } from './car.svg';
export { default as MOTOR } from './motor.svg';
export { default as BIKE } from './bike.svg';
export { default as BSG } from './blue-sg.svg';
export { default as RT } from './river-taxi.svg';
export { default as FBS } from './free-bus.svg';
export { default as ETS } from './entrance.svg';

export { default as BOB } from './brand-oval-bg.svg';
export { default as LOVE } from './love.svg';
export { default as LOVED } from './loved.svg';
export { default as MP } from './mp.svg';
export { default as MPA } from './mp-a.svg';
export { default as BS } from './search.svg';
export { default as BSA } from './search-a.svg';

export { default as ALO } from './account-loved.svg';
export { default as AVC } from './account-voucher.svg';
export { default as AQA } from './account-qa.svg';
export { default as AFB } from './account-feedback.svg';

export { default as PIP } from './promotions-image.png';
export { default as PHD } from './ph-deal.png';
export { default as DTF } from './dintaifung.png';

export { default as CGWL } from './cgwl.png';
export { default as BATIK } from './batik.png';
export { default as MAP } from './map.png';
export { default as CQLOGO } from './cq-logo.png';
export { default as FNLOGO } from './funan.png';
export { default as RAFLOGO } from './raffles-logo.png';
export { default as BJLOGO } from './bj-logo.png';
export { default as BPLOGO } from './bugis+logo.png';
export { default as IMMLOGO } from './imm-logo.png';
export { default as PSLOGO } from './plaza-sga-logo.png';

export { default as B2T } from './back2top.png';

export { default as AXS } from './axs.png';
export { default as DBS } from './dbs.png';
export { default as CITI } from './citi.png';
export { default as HSBC } from './hsbc.png';
export { default as OCBC } from './ocbc.png';
export { default as UOB } from './uob.png';

export { default as CC } from './capita-card.png';
export { default as ECV } from './e-capita-voucher.png';
export { default as CV } from './capita-voucher.png';
export { default as CCS } from './capita-card-starx.png';
export { default as WP } from './wechat-pay.png';
export { default as AP } from './alipay.png';
export { default as GP } from './grab-pay.png';
export { default as DB } from './deal-badge.png';

export { default as CLOSE } from './close.svg';

export { default as LMO } from './loved-mc-oval.svg';

export { default as SEP } from './search-empty.png';

export { default as EPI } from './eventpage-info.svg';
export { default as EPIA } from './eventpage-info-a.svg';
export { default as EPR } from './eventpage-rules.svg';
export { default as EPRA } from './eventpage-rules-a.svg';
export { default as EPL } from './eventpage-locale.svg';
export { default as EPLA } from './eventpage-locale-a.svg';

export { default as CGS } from './cart-gray.svg';
export { default as CGG } from './cart-green.svg';
export { default as LGS } from './locale-green.svg';
export { default as RGS } from './rules-green.svg';
export { default as MPS } from './clickbanner.png';

export { default as BTL } from './bt-logo.png';

export { default as TBP } from './topbanner.png';
export { default as GBP } from './green_banner.png';
export { default as RBP } from './red_banner.png';
export { default as BBP } from './blue_banner.png';

//assets pngs
export { default as MK } from '../assets/mk.png';
export { default as VS } from '../assets/vs.png';
export { default as BBW } from '../assets/bbw.png';
export { default as BL } from '../assets/bl.png';
export { default as MUJI } from '../assets/muji.png';
export { default as LH } from '../assets/lht.png';
export { default as LC } from '../assets/lc.png';
export { default as CROCS } from '../assets/crocs.png';
export { default as KIM } from '../assets/kimage.png';
export { default as G2000 } from '../assets/g2000.png';
export { default as TDG } from '../assets/tdg.png';
export { default as FOS } from '../assets/fossil.png';
export { default as IPP } from '../assets/ippudo.png';
export { default as HI } from '../assets/hi.png';
export { default as INN } from '../assets/innisfree.png';
export { default as FAC } from '../assets/face.png';
export { default as LOC } from '../assets/loc.png';
export { default as IOR } from '../assets/iora.png';
export { default as NOV } from '../assets/novela.png';
export { default as FUR } from '../assets/furla.png';
export { default as APM } from '../assets/apm.png';
export { default as SK } from '../assets/sk.png';
export { default as NJI } from '../assets/nj.png';

//footer
export { default as PBU } from './powerdbyuniweb.png';
