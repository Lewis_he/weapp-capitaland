import Api from '../config/index';
import { server as request } from '../utils/user';

const OPTION_NO_CACHE_AND_EVICT = { cache: false, evict: true };
const OPTION_NO_CACHE = { cache: false, evict: false };

export const Demo = () => request.post();

/**
 * get favorites
 * */
export const getLoved = (payload) => request.get(Api.GetFavorites, payload);
/**
 * global search
 * */
export const gbSearch = (payload, options) => request.get(Api.GBSearch, payload, options);
export const mainCategory = (payload) => request.get(Api.Category, payload);
/**
 * explore search
 * */
export const exploreSearch = (payload, options) => request.get(Api.ExploreSearch, payload, options);
export const bannerSearch = (payload, options) => request.get(Api.BannerSearch, payload, options);
/**
 * outlets search
 * */
export const outletSearch = (payload, options) => request.get(Api.OutletSearch, payload, options);
export const outletDetail = (payload) => request.get(Api.Outlet + payload);
export const favouritesAdd = (payload) => request.post(Api.AddFavorite, payload, OPTION_NO_CACHE_AND_EVICT);
export const favouritesRemove = (payload) => request.post(Api.RemoveFavorite, payload, OPTION_NO_CACHE_AND_EVICT);

/**
 * events search
 * */
export const eventsSearch = (payload, options) => request.get(Api.EventSearch, payload, options);
export const eventDetail = (payload, options) => request.get(Api.Event + payload.id, payload.locale, options);
/**
 * malls
 * */
export const mallSearch = (payload, options) => request.get(Api.MallSearch, payload, options);
export const mallById = (payload) => request.get(Api.MallSearch + `/${payload.id}`, payload.locale);
/**
 * account
 * */
export const lovedOutlets = () => request.get(Api.LovedSearch, {}, OPTION_NO_CACHE_AND_EVICT);
