import request from '../utils/fetch';
import Api from '../config/index';

export async function loginByWeChat(payload) {
  return request.mini_post(Api.AuthLoginByWechat, payload);
}

export async function authorizeUser() {
  return request.get(Api.AuthUserInfo);
}

