export default {
  pages: ['pages/explore/index', 'pages/mall/index', 'pages/activity/index', 'pages/me/index'],
  permission: {
    'scope.userLocation': {
      desc: '你的位置信息将用于小程序位置接口的效果展示'
    }
  },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#2E58FF',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'white'
  },
  tabBar: {
    custom: true,
    backgroundColor: 'white',
    borderStyle: 'white',
    selectedColor: '#5C7EFF',
    color: '#5E5E69',
    list: [
      {
        pagePath: 'pages/explore/index',
        iconPath: './static/images/explore.png',
        selectedIconPath: './static/images/explore-a.png',
        text: '发现'
      },
      {
        pagePath: 'pages/mall/index',
        iconPath: './static/images/mall.png',
        selectedIconPath: './static/images/mall-a.png',
        text: '商场'
      },
      {
        pagePath: 'pages/activity/index',
        iconPath: './static/images/activity.png',
        selectedIconPath: './static/images/activity-a.png',
        text: '活动'
      },
      {
        pagePath: 'pages/me/index',
        iconPath: './static/images/me.png',
        selectedIconPath: './static/images/me-a.png',
        text: '账户'
      }
    ]
  },
  usingComponents: {
    'van-image': './components/vant-weapp/dist/image/index'
  },
  navigateToMiniProgramAppIdList: ['wx887a6fc1e95cd883'],
  subpackages: [
    {
      root: 'subpages/event/',
      name: 'eventPage',
      pages: ['index']
    },
    {
      root: 'subpages/mall/',
      name: 'mallPage',
      pages: ['index']
    },
    {
      root: 'subpages/brand/',
      name: 'brandPage',
      pages: ['index']
    },
    {
      root: 'subpages/love/',
      name: 'lovedMerchant',
      pages: ['index']
    },
    {
      root: 'subpages/search/',
      name: 'searchPage',
      pages: ['index']
    },
    {
      root: 'subpages/oa/',
      name: 'oaPage',
      pages: ['article']
    },
    {
      root: 'subpages/promotion/',
      name: 'promotionPage',
      pages: ['index']
    }
  ]
};
