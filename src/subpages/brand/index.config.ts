export default {
  navigationBarTitleText: '商户主页',
  navigationStyle: 'custom',
  enableShareAppMessage: true,
  usingComponents: {
    'van-transition': '../../components/vant-weapp/dist/transition/index',
    'van-loading': '../../components/vant-weapp/dist/loading/index'
  }
};
