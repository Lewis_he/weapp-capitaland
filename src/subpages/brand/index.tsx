import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { Button, Image, ScrollView, Text, View } from '@tarojs/components';
import { connect } from 'react-redux';

import { StateType } from '../../models/brandModel';
import { ConnectProps, ConnectState } from '../../models/connect';

import './index.scss';
import * as images from '../../static/images';
import { BrandCard } from '../../components/ResponsiveCard/BrandCard';
// import { DealCard } from '../../components/DealCard/DealCard';
import { EndLine } from '../../components/EndLine/EndLine';
import { BrandCardL } from '../../components/BrandCard/BrandCardL';
import { global } from '../../../global.data';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;
@connect(({ brand, loading }: ConnectState) => ({
  ...brand,
  ...loading
}))
class Brand extends React.Component<IProps, OwnState> {
  $instance = getCurrentInstance();
  state = {
    sharing: false,
    loved: false,
    section: 'promotions',
    open: false,
    hasMore: true,
    pageNo: 0,
    oncePageNo: -1
  };
  componentDidMount() {
    const outletId = this.$instance.router?.params.oid;
    const brandId = this.$instance.router?.params.bid;
    this.getOutletDetail(outletId);
    this.getAllOutletByBrand(brandId);
    this.setState({
      sharing: this.$instance.router?.params.share
    });
  }

  componentDidShow() {
    const duplicate = Taro.getStorageSync('opBackTo');
    if (duplicate) {
      // @ts-ignore
      const { oid, bid } = this.$instance.router?.params;
      this.getOutletDetail(oid);
      this.getAllOutletByBrand(bid);
    }
  }

  componentWillUnmount() {
    Taro.setStorageSync('opBackTo', true);
  }

  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    const { oid, bid } = this.$instance.router?.params;
    return {
      title: '新加坡凯德商场',
      path: `/subpages/brand/index?oid=${oid}&bid=${bid}&share=true`
    };
  }

  getAllOutletByBrand(id) {
    this.props.dispatch({
      type: 'brand/siblingsOutlet',
      payload: {
        brandId: id
      }
    });
  }

  getOutletDetail = (id) => {
    this.props
      .dispatch({
        type: 'brand/getOutlet',
        payload: id
      })
      .then((res) => this.setState({ loved: res }));
  };

  onNavBack = () => {
    const { sharing } = this.state;
    sharing
      ? Taro.reLaunch({
          url: '/pages/explore/index'
        })
      : Taro.navigateBack();
  };

  toggleLoved = () => {
    const outletId = this.$instance.router?.params.oid;
    this.setState({ loved: !this.state.loved }, () => {
      this.props.dispatch({
        type: 'brand/handleFavorite',
        payload: {
          isAdd: this.state.loved,
          outlet: { outletId }
        }
      });
    });
  };

  makeCall = () => {
    const { outlet } = this.props;
    const phone = outlet?.contactNumber;
    if (phone) {
      Taro.makePhoneCall({
        phoneNumber: phone
      });
    } else {
      Taro.showToast({
        title: '该商户暂无联系方式',
        icon: 'none',
        duration: 1500
      });
    }
  };

  toggleTab = (tab) => {
    this.setState({ section: tab });
  };

  renderCard = () => {
    const { outlet } = this.props;
    const dgc: string[] = [];
    const pay: string[] = [];
    const tag = (outlet.category?.titleCn ?? '') + (outlet.subCategory?.titleCn ? ' / ' + outlet.subCategory?.titleCn : '');
    const mallName = (outlet.mall?.titleCn ? outlet.mall.titleCn + ' ' : '') + outlet.mall?.title;
    const title = (outlet?.brand?.titleCn ? outlet.brand.titleCn + ' ' : '') + outlet?.brand?.title;
    const logo = outlet.brand?.logo ? global.live + outlet.brand.logo.id : images.CGWL;
    const desc = outlet.brand?.description;
    if (outlet.wechatPay) {
      pay.push('WeChatPay');
    }
    if (outlet.aliPay) {
      pay.push('Alipay');
    }
    if (outlet.grabPay) {
      pay.push('GrabPay');
    }
    if (outlet.capitaVoucher) {
      dgc.push('CapitaVoucher');
    }
    if (outlet.capitaCard) {
      dgc.push('CapitaCard');
    }
    if (outlet.eCapitaVoucher) {
      dgc.push('eCapitaVoucher');
    }
    if (outlet.starXtra) {
      dgc.push('STARXtra');
    }
    return (
      <BrandCard logo={logo} title={title} desc={desc} dgc={dgc} payment={pay} category={tag} mall={mallName} unit={outlet?.storeCode} />
    );
  };

  renderSiblingCard = () => {
    const { siblingOutlets } = this.props;
    return siblingOutlets.map((item) => {
      const dgc: string[] = [];
      const pay: string[] = [];
      const tag = (item.category?.titleCn ?? '') + (item.subCategory?.titleCn ? ' / ' + item.subCategory?.titleCn : '');
      const mallName = (item.mall?.titleCn ? item.mall.titleCn + ' ' : '') + item.mall?.title;
      const title = (item?.brand.titleCn ? item.brand.titleCn + ' ' : '') + item?.brand.title;
      const logo = item.brand?.logo ? global.live + item.brand.logo.id : images.CGWL;
      if (item.capitaVoucher) {
        dgc.push('CapitaVoucher');
      }
      if (item.capitaCard) {
        dgc.push('CapitaCard');
      }
      if (item.eCapitaVoucher) {
        dgc.push('eCapitaVoucher');
      }
      if (item.starXtra) {
        dgc.push('STARXtra');
      }
      if (item.wechatPay) {
        pay.push('WeChatPay');
      }
      if (item.aliPay) {
        pay.push('Alipay');
      }
      if (item.grabPay) {
        pay.push('GrabPay');
      }
      return (
        <BrandCardL
          margin="0 auto 17px"
          logo={logo}
          title={title}
          dgc={dgc}
          payment={pay}
          category={tag}
          mall={mallName}
          unit={item?.storeCode}
          onClick={() => this.viewSiblings(item.id, item.brand?.id)}
        />
      );
    });
  };

  viewSiblings = (outlet, brand) => {
    Taro.navigateTo({ url: `/subpages/brand/index?oid=${outlet}&bid=${brand}` });
    Taro.setStorageSync('opBackTo', false);
  };

  upperLoadMore = (e) => {
    const brandId = this.$instance.router?.params.bid;
    let { hasMore, pageNo, oncePageNo, section } = this.state;
    if (section != 'search') return;
    if (!hasMore || oncePageNo == pageNo) {
      return;
    }

    this.setState({ open: true, oncePageNo: pageNo });

    pageNo++;

    this.props
      .dispatch({
        type: 'brand/search',
        payload: {
          brandId,
          page: pageNo
        }
      })
      .then((res) => {
        this.setState({ pageNo, open: false, hasMore: res });
      });
  };

  render() {
    const { outlet } = this.props;
    const { loved, section, open, hasMore, sharing } = this.state;
    return (
      <View className="brand-page">
        {/*status bar*/}
        <View className="bp-status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
        {/*nav bar*/}
        <View className="bp-navbar">
          <View className="bp-navbar-navback" onClick={this.onNavBack}>
            <View className={sharing ? 'bp-icon-home' : 'bp-ion-back'} />
          </View>
          <Text className="bp-navbar-title">{outlet?.title}</Text>
        </View>

        <ScrollView className="bp-sv" scrollY scrollWithAnimation onScrollToLower={this.upperLoadMore}>
          <View className="bp-header">
            <Image className="bp-header-bg" src={images.BOB} mode="aspectFill" />

            <View className="bp-header-card">{this.renderCard()}</View>

            <View className="bp-header-menu-bar">
              <View className="bp-menu-item" onClick={() => this.toggleTab('promotions')}>
                <Image src={section == 'promotions' ? images.MPA : images.MP} className="bp-menu-icon" />
                商户优惠
              </View>
              <View className="bp-menu-item" onClick={() => this.toggleTab('search')}>
                <Image src={section == 'search' ? images.BSA : images.BS} className="bp-menu-icon" />
                所有门店
              </View>
              <View className="bp-menu-item" onClick={this.toggleLoved}>
                <Image src={loved ? images.LOVED : images.LOVE} className="bp-menu-icon" />
                {loved ? '已收藏' : '收藏该店'}
              </View>
              <View className="bp-menu-item" onClick={this.makeCall}>
                <View className="bp-contact-icon" />
                联系商户
              </View>
              <Button className="bp-menu-item" openType="share">
                <View className="bp-share-icon" />
                分享好友
              </Button>
            </View>
          </View>

          {section == 'promotions' && (
            <View className="bp-promotions">
              {/*<Image className="bp-promotions-title" src={images.PIP} />*/}
              {/*<DealCard margin='0 auto 12px' title='鼎泰丰 Din Tai Fung' desc='特色手工小笼包，第二份半价优惠券' image={images.XLB} />*/}
              {/*<DealCard margin='0 auto 12px' title='鼎泰丰 Din Tai Fung' desc='消费满100元，8折代金券 全天可用包括双休日' image={images.DTF} />*/}
              {/*<DealCard margin='0 auto 12px' title='鼎泰丰 Din Tai Fung' desc='消费满200元，75折代金券 全天可用包括双休日' image={images.DTF} />*/}
              {/*<DealCard margin='0 auto 12px' title='鼎泰丰 Din Tai Fung' desc='消费满300元，7折代金券 全天可用包括双休日' image={images.DTF} />*/}
              <Image src={images.PHD} className="bp-promotions-placeholder" />
            </View>
          )}
          {section == 'search' && <View className="bp-siblings">{this.renderSiblingCard()}</View>}
          {hasMore ? (
            <van-loading customClass={open ? 'bp-loading' : 'bp-loading dismiss'} type="spinner" vertical={true}>
              加载中...
            </van-loading>
          ) : (
            <View className="bp-loading">没有更多了</View>
          )}

          <EndLine margin="16px auto 0" />
        </ScrollView>
      </View>
    );
  }
}
export default Brand as ComponentClass<OwnProps>;
