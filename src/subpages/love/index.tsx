import React, { ComponentClass } from 'react';
import Taro from '@tarojs/taro';
import { Image, ScrollView, Text, View } from '@tarojs/components';

import { connect } from 'react-redux';
import { StateType } from '../../models/accountModel';

import { ConnectProps, ConnectState } from '../../models/connect';
import './index.scss';
import * as images from '../../static/images';
import { global } from '../../../global.data';
import { MallCardL } from '../../components/MallCard/MallCardL';
import { EndLine } from '../../components/EndLine/EndLine';
import { BrandCard } from '../../components/ResponsiveCard/BrandCard';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;
@connect(({ account, loading }: ConnectState) => ({
  ...account,
  ...loading
}))
class LovedPage extends React.Component<IProps, OwnState> {
  lastPos = 0;
  state = {};
  componentDidMount() {}

  componentDidShow() {
    this.props.dispatch({
      type: 'account/getLovedOutlets'
    });
  }

  onNavBack = () => {
    Taro.navigateBack();
  };

  renderLovedOutlets = () => {
    const { lovedOutlets } = this.props;
    return lovedOutlets.map((item) => {
      const dgc: string[] = [];
      const pay: string[] = [];
      const tag = (item.category?.titleCn ?? '') + (item.subCategory?.titleCn ? ' / ' + item.subCategory?.titleCn : '');
      const mallName = (item.mall?.titleCn ? item.mall.titleCn + ' ' : '') + item.mall?.title;
      const title = (item?.brand.titleCn ? item.brand.titleCn + ' ' : '') + item.brand.title;
      const logo = item.brand.logo ? global.live + item.brand.logo.id : images.CGWL;
      const desc = item.brand?.description;
      if (item.wechatPay) {
        pay.push('WeChatPay');
      }
      if (item.aliPay) {
        pay.push('Alipay');
      }
      if (item.grabPay) {
        pay.push('GrabPay');
      }
      if (item.capitaVoucher) {
        dgc.push('CapitaVoucher');
      }
      if (item.capitaCard) {
        dgc.push('CapitaCard');
      }
      if (item.eCapitaVoucher) {
        dgc.push('eCapitaVoucher');
      }
      if (item.starXtra) {
        dgc.push('STARXtra');
      }
      return (
        <BrandCard
          margin="0 auto 20px"
          logo={logo}
          title={title}
          desc={desc}
          dgc={dgc}
          payment={pay}
          category={tag}
          mall={mallName}
          unit={item?.storeCode}
          onClick={() => Taro.navigateTo({ url: `/subpages/brand/index?oid=${item.id}&bid=${item.brand.id}` })}
        />
      );
    });
  };

  render() {
    const {} = this.state;
    const mTop = 64 + Taro.$statusBarHeight;
    return (
      <View className="loved-merchant-page">
        <Image src={images.LMO} className="lmp-nav" mode="aspectFill">
          <View className="lmp-nav-header">
            <View className="lmp-status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
            <View className="lmp-nav-bar">
              <View className="lmp-navback" onClick={this.onNavBack}>
                <View className="lmp-ion-back" />
              </View>
              <Text className="lmp-title">收藏的商户</Text>
            </View>
          </View>
        </Image>

        <ScrollView className="lmp-sv" style={{ marginTop: mTop + 'px' }} scrollY scrollWithAnimation>
          {this.renderLovedOutlets()}
          <EndLine margin="0 auto" />
        </ScrollView>
      </View>
    );
  }
}

export default LovedPage as ComponentClass<OwnProps>;
