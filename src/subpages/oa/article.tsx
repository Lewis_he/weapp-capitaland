import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { WebView } from '@tarojs/components';

class Article extends React.Component {
  $instance = getCurrentInstance();
  state = {
    url: ''
  };

  componentDidMount() {
    const url = this.$instance.router?.params.url;
    this.setState({ url });
  }

  render() {
    const { url } = this.state;
    return <WebView src={url} />;
  }
}

export default Article as ComponentClass;
