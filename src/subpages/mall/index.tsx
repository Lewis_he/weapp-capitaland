import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { Image, ScrollView, View, Text, Picker } from '@tarojs/components';

import { connect } from 'react-redux';
import { StateType } from '../../models/mallPageModel';

import { ConnectProps, ConnectState } from '../../models/connect';
import './index.scss';
import * as images from '../../static/images';
import { MallCardXL } from '../../components/MallCard/MallCardXL';
import { BrandCard } from '../../components/ResponsiveCard/BrandCard';
import { EndLine } from '../../components/EndLine/EndLine';
import { RafflesCity } from '../../components/MallDetail/RafflesCity';
import { BugisJunction } from '../../components/MallDetail/BugisJunction';
import { BugisPlus } from '../../components/MallDetail/Bugis+';
import { Funan } from '../../components/MallDetail/Funan';
import { ClarkQuay } from '../../components/MallDetail/ClarkQuay';
import { IMM } from '../../components/MallDetail/IMM';
import { PlazaSingapura } from '../../components/MallDetail/PlazaSingapura';
import { global } from '../../../global.data';
import { BugisStreet } from '../../components/MallDetail/BugisStreet';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}

interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;

@connect(({ common, mallPage, brand }: ConnectState) => ({
  ...mallPage,
  ...common,
  ...brand
}))
class MallPage extends React.Component<IProps, OwnState> {
  lastPost = 0;
  $instance = getCurrentInstance();
  state = {
    sharing: false,
    show: false,
    top: 0,
    section: 'search',
    selectorChecked: { id: null, titleCn: '所有商户类型' },
    subSelectorChecked: { id: null, titleCn: '所有子类别' },
    triggered: false,
    open: false,
    hasMore: true,
    pageNo: 0,
    oncePageNo: -1,
    subActive: false
  };

  componentDidMount() {
    // 获取路由参数
    const mallID = this.$instance.router?.params.id;
    this.props.dispatch({
      type: 'mallPage/mock',
      payload: {
        id: mallID
      }
    });
    this.getCategory(mallID);
    this.getOutletsUnderMall(mallID);
    this.setState({
      sharing: this.$instance.router?.params.share
    });
  }

  componentDidShow() {}

  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    const mallID = this.$instance.router?.params.id;
    return {
      title: '新加坡凯德商场',
      path: `/subpages/mall/index?id=${mallID}&share=true`
    };
  }

  getCategory = (mallID) => {
    this.props.dispatch({
      type: 'common/getCategory',
      payload: { mallID }
    });
  };
  getSubCategory = (id) => {
    this.props
      .dispatch({
        type: 'common/getSubCategory',
        payload: {
          parentId: id
        }
      })
      .then((res) => {
        this.setState({ subActive: res });
      });
  };

  getOutletsUnderMall = (id) => {
    this.props.dispatch({
      type: 'brand/search',
      payload: {
        mallId: id
      }
    });
  };

  onNavBack = () => {
    const { sharing } = this.state;
    sharing
      ? Taro.reLaunch({
          url: '/pages/explore/index'
        })
      : Taro.navigateBack();
  };

  onPageScroll = (e) => {
    let st = e.detail.scrollTop;
    if (st >= 330 && this.lastPost < 330) {
      this.setState({ show: true });
    } else if (st <= 330 && this.lastPost > 330) {
      this.setState({ show: false });
    }
    this.lastPost = st;
  };

  backToTop = () => {
    // to make sure the state updated
    this.setState({ top: -1 }, () => this.setState({ top: 0 }));
  };

  onSectionChanged = (section) => {
    // console.log('owners method called: ', section);
    this.setState({ section });
  };

  onPickerChange = (e) => {
    const index = e.detail.value;
    const { category } = this.props;
    this.setState(
      {
        selectorChecked: category[index],
        subSelectorChecked: { id: null, titleCn: '所有子类别' },
        pageNo: 0,
        hasMore: true,
        oncePageNo: -1
      },
      () => {
        this.getSubCategory(category[index].id);
        this.getOutletsWithParams();
      }
    );
  };
  onSubPickerChange = (e) => {
    const index = e.detail.value;
    const { subCategory } = this.props;
    this.setState(
      {
        subSelectorChecked: subCategory[index],
        pageNo: 0,
        hasMore: true,
        oncePageNo: -1
      },
      () => {
        this.getOutletsWithParams();
      }
    );
  };

  getOutletsWithParams = () => {
    const mallId = this.$instance.router?.params.id;
    const { selectorChecked, subSelectorChecked } = this.state;
    const payload = { mallId: mallId };
    if (selectorChecked.id) payload['categoryId'] = selectorChecked.id;
    if (subSelectorChecked.id) payload['subCategoryId'] = subSelectorChecked.id;
    console.log('Params: ', payload);
    this.props
      .dispatch({
        type: 'brand/search',
        payload: payload
      })
      .then(() => {
        this.setState({ triggered: false, pageNo: 0, hasMore: true, oncePageNo: -1 });
      });
  };

  refresherRefresh = () => {
    const { section } = this.state;
    if (section != 'search') return;
    this.setState({ triggered: true });
    this.getOutletsWithParams();
  };

  upperLoadMore = (e) => {
    const mallId = this.$instance.router?.params.id;
    let { hasMore, pageNo, oncePageNo, section, selectorChecked, subSelectorChecked } = this.state;
    if (section != 'search') return;
    if (!hasMore || oncePageNo == pageNo) {
      return;
    }

    this.setState({ open: true, oncePageNo: pageNo });

    pageNo++;

    const payload = { mallId: mallId, page: pageNo };
    if (selectorChecked.id) payload['categoryId'] = selectorChecked.id;
    if (subSelectorChecked.id) payload['subCategoryId'] = subSelectorChecked.id;
    this.props
      .dispatch({
        type: 'brand/search',
        payload: payload
      })
      .then((res) => {
        this.setState({ pageNo, open: false, hasMore: res });
      });
  };

  renderOutlets = () => {
    const { outlets } = this.props;
    return outlets.map((item) => {
      const dgc: string[] = [];
      const pay: string[] = [];
      const tag = (item.category?.titleCn ?? '') + (item.subCategory?.titleCn ? ' / ' + item.subCategory?.titleCn : '');
      const mallName = (item.mall?.titleCn ? item.mall.titleCn + ' ' : '') + item.mall?.title;
      const title = (item?.brand.titleCn ? item.brand.titleCn + ' ' : '') + item.brand.title;
      const logo = item.brand.logo ? global.live + item.brand.logo.id : images.CGWL;
      const desc = item.brand?.description;
      if (item.wechatPay) {
        pay.push('WeChatPay');
      }
      if (item.aliPay) {
        pay.push('Alipay');
      }
      if (item.grabPay) {
        pay.push('GrabPay');
      }
      if (item.capitaVoucher) {
        dgc.push('CapitaVoucher');
      }
      if (item.capitaCard) {
        dgc.push('CapitaCard');
      }
      if (item.eCapitaVoucher) {
        dgc.push('eCapitaVoucher');
      }
      if (item.starXtra) {
        dgc.push('STARXtra');
      }
      return (
        <BrandCard
          margin="0 auto 20px"
          logo={logo}
          title={title}
          desc={desc}
          dgc={dgc}
          payment={pay}
          category={tag}
          mall={mallName}
          unit={item?.storeCode}
          onClick={() => Taro.navigateTo({ url: `/subpages/brand/index?oid=${item.id}&bid=${item.brand.id}` })}
        />
      );
    });
  };

  render() {
    const { mallData, category, subCategory } = this.props;
    const { show, top, section, sharing, selectorChecked, subSelectorChecked, triggered, open, hasMore, subActive } = this.state;
    return (
      <View className="mall-page">
        {/*status bar*/}
        <View className="mp-status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
        {/*nav bar*/}
        <View className="mp-navbar">
          <View className="mp-navbar-navback" onClick={this.onNavBack}>
            <View className={sharing ? 'mp-icon-home' : 'mp-ion-back'} />
          </View>
          <Text className="mp-navbar-title">{mallData.nav}</Text>
        </View>

        <ScrollView
          className="mp-sv"
          scrollY
          scrollTop={top}
          scrollWithAnimation
          onScroll={this.onPageScroll}
          refresherEnabled
          refresherTriggered={triggered}
          onRefresherRefresh={this.refresherRefresh}
          onScrollToLower={this.upperLoadMore}
        >
          <View className={section == 'search' ? 'mp-header' : 'mp-header header-short'}>
            <Image className="mp-header-bg" src={mallData.bgImage} />
            {section == 'search' && (
              <View className="mp-header-picker-wrap">
                <Picker className="mpp-main-cate" mode="selector" rangeKey="titleCn" range={category} onChange={this.onPickerChange}>
                  <View className="mp-picker">
                    {selectorChecked.titleCn}
                    <View className="mp-cate-icon" />
                  </View>
                </Picker>
                {subActive && <View className="mpp-next" />}
                {subActive && (
                  <Picker className="mpp-sub-cate" mode="selector" rangeKey="titleCn" range={subCategory} onChange={this.onSubPickerChange}>
                    <View className="mp-picker">
                      {subSelectorChecked.titleCn}
                      <View className="mp-cate-icon" />
                    </View>
                  </Picker>
                )}
              </View>
            )}
            <View className="mp-header-card">
              <MallCardXL
                id={mallData.id}
                logo={mallData.logo}
                address={mallData.address}
                title={mallData.title}
                la={mallData.la}
                lo={mallData.lo}
                onChangeSection={this.onSectionChanged}
              />
            </View>
          </View>
          {/*search section*/}
          {section == 'search' && <View className="mp-section">{this.renderOutlets()}</View>}
          {/*summary section*/}
          {section != 'search' && mallData.id == 6 && <RafflesCity section={section} />}
          {section != 'search' && mallData.id == 19 && <BugisStreet section={section} />}
          {section != 'search' && mallData.id == 12 && <BugisJunction section={section} />}
          {section != 'search' && mallData.id == 13 && <BugisPlus section={section} />}
          {section != 'search' && mallData.id == 18 && <Funan section={section} />}
          {section != 'search' && mallData.id == 9 && <ClarkQuay section={section} />}
          {section != 'search' && mallData.id == 3 && <IMM section={section} />}
          {section != 'search' && mallData.id == 15 && <PlazaSingapura section={section} />}

          <EndLine margin="10px auto 0" />
          {hasMore ? (
            <van-loading customClass={open ? 'mp-loading' : 'mp-loading dismiss'} type="spinner" vertical={true}>
              加载中...
            </van-loading>
          ) : (
            <View className="mp-loading">没有更多了</View>
          )}
        </ScrollView>

        <van-transition customClass="mp-back-top" show={show} name="fade-right">
          <Image className="mp-back-top-icon" src={images.B2T} onClick={this.backToTop} />
        </van-transition>
      </View>
    );
  }
}

export default MallPage as ComponentClass<OwnProps>;
