import React, { ComponentClass } from 'react';
import Taro from '@tarojs/taro';
import { Image, ScrollView, View, Text, Input } from '@tarojs/components';

import { connect } from 'react-redux';
import { StateType } from '../../models/mallPageModel';

import { ConnectProps, ConnectState } from '../../models/connect';
import './index.scss';
import * as images from '../../static/images';
import { global } from '../../../global.data';
import { MallCardL } from '../../components/MallCard/MallCardL';
import { dateCN, distanceCN } from '../../utils/util';
import { EventCard } from '../../components/EventCard/EventCard';
import { BrandCard } from '../../components/ResponsiveCard/BrandCard';
import { NearbyCard } from '../../components/NearbyCard/NearbyCard';

interface OwnProps {
  value: number;
}

interface OwnState {}

type IProps = StateType & ConnectProps & OwnProps;

@connect(({ common, loading }: ConnectState) => ({
  ...common,
  ...loading
}))
class SearchPage extends React.Component<IProps, OwnState> {
  state = {
    keywords: '',
    open: false,
    hasMore: true,
    pageNo: 0,
    oncePageNo: -1,
    triggered: false
  };

  componentDidMount() {}

  onNavBack = () => {
    Taro.navigateBack();
    this.props.dispatch({
      type: 'common/clear'
    });
  };

  blurSearch = () => {
    this.setState({ keywords: '' }, () => {
      this.props.dispatch({
        type: 'common/clear'
      });
    });
  };

  inputChange = (e) => {
    const keywords = e.detail.value;
    this.setState({ keywords, pageNo: 0, hasMore: true, oncePageNo: -1 });
    const location = Taro.getStorageSync('location');
    const payload = { filter: keywords };
    if (location.latitude) payload['latitude'] = location.latitude;
    if (location.longitude) payload['longitude'] = location.longitude;
    this.props.dispatch({
      type: 'common/search',
      payload: payload
    });
  };

  upperLoadMore = (e) => {
    let { hasMore, pageNo, oncePageNo, keywords } = this.state;
    if (!hasMore || oncePageNo == pageNo) {
      return;
    }

    this.setState({ open: true, oncePageNo: pageNo });

    pageNo++;

    const local = Taro.getStorageSync('location');
    const payload = { filter: keywords, page: pageNo };
    if (local.latitude) payload['latitude'] = local.latitude;
    if (local.longitude) payload['longitude'] = local.longitude;
    this.props
      .dispatch({
        type: 'common/search',
        payload: payload
      })
      .then((res) => {
        this.setState({ pageNo, open: false, hasMore: res });
      });
  };

  renderSearchCard = () => {
    const { searchRes } = this.props;
    return searchRes.map((item) => {
      let el;
      if (item.entityType == 'MALL') {
        const bg = global.live + item.thumbnail?.id;
        const logo = global.live + item.logo.id;
        const distance = item.distance ? distanceCN(item.distance) : '暂无距离';
        const title = (item.titleCn ? item.titleCn + ' ' : '') + item.title;
        const description = item.description;
        el = (
          <MallCardL
            margin="0 auto 22px"
            bg={bg}
            logo={logo}
            distance={distance}
            title={title}
            desc={description}
            onClick={() => {
              Taro.navigateTo({ url: `/subpages/mall/index?id=${item.id}` });
            }}
          />
        );
      } else if (item.entityType == 'EVENT') {
        const logo = global.live + item.thumbnail.id;
        const title = item.title;
        const date = dateCN(item.startDate) + '  -  ' + dateCN(item.endDate);
        const locale = (item.malls?.[0]?.titleCn ? item.malls[0].titleCn + ' ' : '') + item.malls?.[0]?.title;
        el = (
          <EventCard
            margin="0 auto 22px"
            logo={logo}
            title={title}
            date={date}
            location={locale}
            onClick={() => {
              Taro.navigateTo({ url: `/subpages/event/index?id=${item.id}` });
            }}
          />
        );
      } else if (item.entityType == 'OUTLET') {
        const dgc: string[] = [];
        const pay: string[] = [];
        const tag = (item.category?.titleCn ?? '') + (item.subCategory?.titleCn ? ' / ' + item.subCategory?.titleCn : '');
        const mallName = (item.mall?.titleCn ? item.mall.titleCn + ' ' : '') + item.mall?.title;
        const title = (item?.brand.titleCn ? item.brand.titleCn + ' ' : '') + item?.brand.title;
        const logo = item.brand.logo ? global.live + item.brand.logo.id : images.CGWL;
        const desc = item.brand?.description;
        if (item.wechatPay) {
          pay.push('WeChatPay');
        }
        if (item.aliPay) {
          pay.push('Alipay');
        }
        if (item.grabPay) {
          pay.push('GrabPay');
        }
        if (item.capitaVoucher) {
          dgc.push('CapitaVoucher');
        }
        if (item.capitaCard) {
          dgc.push('CapitaCard');
        }
        if (item.eCapitaVoucher) {
          dgc.push('eCapitaVoucher');
        }
        if (item.starXtra) {
          dgc.push('STARXtra');
        }
        el = (
          <BrandCard
            margin="0 auto 22px"
            logo={logo}
            title={title}
            desc={desc}
            dgc={dgc}
            payment={pay}
            category={tag}
            mall={mallName}
            unit={item?.storeCode}
            onClick={() => Taro.navigateTo({ url: `/subpages/brand/index?oid=${item.id}&bid=${item.brand.id}` })}
          />
        );
      } else if (item.entityType == 'PLACE') {
        const bg = global.live + item?.thumbnail.id;
        const distance = item.distance ? distanceCN(item.distance) : '暂无距离';
        const title = (item.titleCn ? item.titleCn + ' ' : '') + item.title;
        const description = item.description;
        el = (
          <NearbyCard
            margin="0 auto 22px"
            bg={bg}
            distance={distance}
            title={title}
            desc={description}
            onClick={() => item.link && this.jump2OA(item.link)}
          />
        );
      }
      return el;
    });
  };

  jump2OA = (link) => {
    Taro.navigateTo({ url: `/subpages/oa/article?url=${link}` });
  };

  refresherRefresh = () => {
    this.setState({ triggered: true });
    this.getNewLBSInfo();
  };

  getNewLBSInfo = () => {
    const that = this;
    Taro.getSetting({
      success: function(res) {
        if (res.authSetting['scope.userLocation']) {
          Taro.getLocation({
            type: 'wgs84',
            success: function(result) {
              const longitude = result.longitude;
              const latitude = result.latitude;
              const local = { longitude, latitude };
              Taro.setStorageSync('location', local);
            }
          });
        }
      },
      complete: function(res) {
        that.searchResultNoCache();
      }
    });
  };

  searchResultNoCache = () => {
    const { keywords } = this.state;
    const local = Taro.getStorageSync('location');
    const payload = { refresh: true, filter: keywords };
    if (local.latitude) payload['latitude'] = local.latitude;
    if (local.longitude) payload['longitude'] = local.longitude;
    this.props
      .dispatch({
        type: 'common/search',
        payload: payload
      })
      .then((res) => {
        this.setState({ pageNo: 0, oncePageNo: -1, hasMore: true, triggered: false });
      });
  };

  render() {
    const { searchRes } = this.props;
    const { keywords, open, hasMore, triggered } = this.state;
    const empty = searchRes.length == 0;
    return (
      <View className="search-page">
        {/*status bar*/}
        <View className="sp-status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
        {/*nav bar*/}
        <View className="sp-navbar">
          <View className="sp-navbar-navback" onClick={this.onNavBack}>
            <View className="sp-ion-back" />
          </View>
          <Text className="sp-navbar-title">新加坡凯德商场</Text>
        </View>

        <View className="sp-head-focus">
          <View className="sp-head-focus-sb">
            <View className="sbf-icon" />
            <Input
              value={keywords}
              className="sbf-input"
              focus
              placeholder="搜索品牌/商户/活动/优惠…"
              placeholderClass="sbf-ph"
              onInput={this.inputChange}
            />
          </View>
          <View className="sp-head-focus-cancel" onClick={this.blurSearch}>
            取消
          </View>
        </View>

        {empty && keywords.length > 0 && (
          <View className="sp-empty-wrap">
            <View className="sp-empty-wrap-no-results">No Results</View>
            <View className="sp-empty-wrap-sorry">非常抱歉，没有找到您搜索的结果</View>
            <Image src={images.SEP} className="sp-empty-wrap-image" />
          </View>
        )}
        {!empty && (
          <ScrollView
            className="sp-sv"
            scrollY
            scrollWithAnimation
            lowerThreshold={100}
            onScrollToLower={this.upperLoadMore}
            refresherEnabled
            refresherTriggered={triggered}
            onRefresherRefresh={this.refresherRefresh}
          >
            {this.renderSearchCard()}

            {hasMore ? (
              <van-loading customClass={open ? 'sp-loading' : 'sp-loading dismiss'} type="spinner" vertical={true}>
                加载中...
              </van-loading>
            ) : (
              <View className="sp-loading">没有更多了</View>
            )}
          </ScrollView>
        )}
      </View>
    );
  }
}

export default SearchPage as ComponentClass<OwnProps>;
