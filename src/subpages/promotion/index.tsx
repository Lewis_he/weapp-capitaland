import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { Button, Image, View } from '@tarojs/components';

import { connect } from 'react-redux';
import { StateType } from '../../models/accountModel';

import { ConnectProps, ConnectState } from '../../models/connect';
import './index.scss';
import '@tarojs/taro/html.css';
import * as images from '../../static/images';
import { EndLine } from '../../components/EndLine/EndLine';
import { MallCardM } from '../../components/MallCard/MallCardM';
import { BrandCardXS } from '../../components/BrandCard/BrandCardXS';
import { global } from '../../../global.data';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}

interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;

@connect(({ event, loading }: ConnectState) => ({
  ...event,
  ...loading
}))
class EventPage extends React.Component<IProps, OwnState> {
  $instance = getCurrentInstance();
  state = {
    sharing: false,
    tab: 0
  };

  componentDidMount() {
    const tabId = this.$instance.router?.params.id;
    this.setState({
      tab: tabId,
      sharing: this.$instance.router?.params.share
    });
  }

  componentDidShow() {}

  onShareAppMessage(res) {
    const { tab } = this.state;
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    return {
      title: '新加坡凯德商场',
      path: `/subpages/promotion/index?id=${tab}&share=true`
    };
  }

  onNavBack = () => {
    const { sharing } = this.state;
    sharing
      ? Taro.reLaunch({
          url: '/pages/explore/index'
        })
      : Taro.navigateBack();
  };

  onTabChange = (index: number) => {
    this.setState({ tab: index });
  };

  renderPromoBrands = () => {
    const brands = [
      { id: 0, logo: images.MK, category: '时尚/服装/配饰', title: 'Michael Kors (Valiram)' },
      { id: 1, logo: images.VS, category: '时尚/服装/配饰', title: "Victoria's Secret (Valiram)" },
      { id: 2, logo: images.BBW, category: '护肤/美妆', title: 'Bath & Body Works (Valiram)' },
      { id: 3, logo: images.BL, category: '化妆品 & 香水', title: 'Beauty Love (Valiram)' },
      { id: 4, logo: images.MUJI, category: '百货商店 & 超值商店', title: '无印良品 MUJI' },
      { id: 5, logo: images.LH, category: '餐饮/食物小亭 & 小吃', title: '里喝茶 LiHO TEA' },
      { id: 6, logo: images.LC, category: '首饰/珠宝', title: 'Love & Co.' },
      { id: 7, logo: images.CROCS, category: '时尚潮流/鞋包', title: '卡骆驰 Crocs' },
      { id: 8, logo: images.KIM, category: '美容健康', title: 'KIMAGE' },
      { id: 9, logo: images.G2000, category: '时尚/服装/配饰', title: 'G2000' },
      { id: 10, logo: images.TDG, category: '餐饮/咖啡 & 甜点吧', title: 'The Dark Gallery' },
      { id: 11, logo: images.FOS, category: '时尚潮流/珠宝&手表', title: '化石 Fossil' },
      { id: 12, logo: images.IPP, category: '餐饮/食物小亭 & 小吃', title: '一风堂Ippudo' },
      { id: 13, logo: images.HI, category: '餐饮/食物小亭 & 小吃', title: '海底涝 Haidilao' },
      { id: 14, logo: images.INN, category: '护肤/美妆', title: '悦诗风吟 Innisfree' },
      { id: 15, logo: images.FAC, category: '护肤/美妆', title: '菲诗小铺 The Face Shop' },
      { id: 16, logo: images.LOC, category: '护肤/美妆', title: "欧舒丹 L'Occitane" },
      { id: 17, logo: images.IOR, category: '时尚/服装/配饰', title: 'iORA' },
      { id: 18, logo: images.NOV, category: '护肤/美妆', title: 'Novela' },
      { id: 19, logo: images.FUR, category: '时尚/服装/配饰', title: '芙拉 Furla' },
      { id: 20, logo: images.APM, category: '首饰/珠宝', title: 'APM Monaco' },
      { id: 21, logo: images.SK, category: '首饰/珠宝', title: 'SK Jewellery' },
      { id: 22, logo: images.SK, category: '首饰/珠宝', title: 'SK Gold' },
      { id: 23, logo: images.NJI, category: '餐饮/美食', title: '南京大排档 Najing Impression' }
    ];
    return brands.map((item) => {
      return <BrandCardXS margin="0 auto 17px" logo={item.logo} title={item.title} category={item.category} />;
    });
  };

  renderEventMalls = () => {
    const live = global.live;
    const event = [
      { id: 6, logo: images.RAFLOGO, bg: live + 1324, title: '来福士城 Raffles City' },
      { id: 18, logo: images.FNLOGO, bg: live + 1322, title: '福南 Funan' },
      { id: 12, logo: images.BJLOGO, bg: live + 700, title: '白沙浮广场 Bugis Junction' },
      { id: 13, logo: images.BPLOGO, bg: live + 1225, title: '白沙浮娱乐广场 Bugis+' },
      { id: 9, logo: images.CQLOGO, bg: live + 1321, title: '克拉码头 Clarke Quay' },
      { id: 15, logo: images.PSLOGO, bg: live + 1323, title: '狮城大厦 Plaza Singapura' }
    ];
    return event.map((item) => {
      const bg = item.bg;
      const logo = item.logo;
      const title = item.title;
      return (
        <MallCardM
          margin="0 auto 25px"
          bg={bg}
          logo={logo}
          title={title}
          onClick={() => {
            Taro.navigateTo({ url: `/subpages/mall/index?id=${item.id}` });
          }}
        />
      );
    });
  };

  navigate2Mp = () => {
    Taro.navigateToMiniProgram({
      appId: 'wx887a6fc1e95cd883',
      path: '/pages/actpage/actpage?actId=b68e3877-bba0-4d50-9959-d0ff3296c3ae',
      success: function(res) {
        // 打开成功
      }
    });
  };

  render() {
    const { tab, sharing } = this.state;
    const pbg = Taro.$statusBarHeight + 40;
    return (
      <View className="promotion-page">
        <View className="promotion-page-header">
          <View className="ep-status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
          <View className="ep-nav-bar">微信支付活动详情</View>
        </View>
        <View className="navigate-back" style={{ top: Taro.$statusBarHeight + 'px' }} onClick={this.onNavBack}>
          <View className={sharing ? 'icon-home' : 'icon-back'} />
        </View>
        <View className="promotion-page-sv">
          <Image className="promotion-page-bg" style={{ marginTop: pbg + 'px' }} src={images.TBP} mode="aspectFill" />

          <View className="func-panel sticky" style={{ top: pbg + 'px' }}>
            <View className="func-panel-title">微信支付 X 新加坡凯德购物商场 {'\n'}累计消费满1000元立享50元现金券返现</View>
            <Button className="func-panel-share" openType="share">
              <View className="ep-share" />
              分享
            </Button>
            <View className="func-panel-timeline">
              <View className="fptl-icon" />
              活动举办时间： 即日起至2022年3月20日
            </View>
            <View className="func-panel-tabs">
              <View className={tab == 0 ? 'tab-s tab-active' : 'tab-s'} onClick={() => this.onTabChange(0)}>
                <Image src={tab == 0 ? images.CGG : images.CGS} className="tab-icon-m" />
                参与商户
              </View>
              <View className={tab == 1 ? 'tab-s tab-active' : 'tab-s'} onClick={() => this.onTabChange(1)}>
                <Image src={tab == 1 ? images.LGS : images.EPL} className="tab-icon-s" />
                参与商场
              </View>
              <View className={tab == 2 ? 'tab-m tab-active' : 'tab-m'} onClick={() => this.onTabChange(2)}>
                <Image src={tab == 2 ? images.RGS : images.EPR} className="tab-icon-s" />
                活动说明
              </View>
            </View>
          </View>
          <Image className="mid-banner" src={images.MPS} onClick={this.navigate2Mp} />
          {/*tabs*/}
          {/*0*/}
          {tab == 0 && (
            <View className="promotion-brands">
              <View className="pb-header">推荐商户</View>
              {this.renderPromoBrands()}
              <View className="pb-list">
                <View className="pb-list-title">所有参与活动商户列表</View>
                <View className="pb-list-content">
                  Seoarae {'\n'}
                  Paris Baguette Bugis Junction {'\n'}
                  Kiehls Bugis Junction {'\n'}
                  NYX Bugis Junction {'\n'}
                  Shu Uemura Bugis Junction {'\n'}
                  GUESS Bugis Junction {'\n'}
                  Pazzion Bugis Junction {'\n'}
                  IORA Bugis Junction {'\n'}
                  SANS & SANS Bugis Junction {'\n'}
                  Seoul Yummy Bugis Junction {'\n'}
                  Adidas Bugis Junction {'\n'}
                  MARK NASON BY SKECHERS Bugis Junction {'\n'}
                  Innisfree - Bugis Junction {'\n'}
                  Etude House@Bugis Junction {'\n'}
                  SK Jewellery Bugis Junction #01-34 {'\n'}
                  The Face Shop Bugis Junction {'\n'}
                  SK GOLD Bugis Junction #01-24 {'\n'}
                  NOVELA BUGIS JUNCTION {'\n'}
                  TRT @ Bugis Junction {'\n'}
                  Cath Kidston Bugis Junction {'\n'}
                  Kappa Bugis Junction {'\n'}
                  YOUNG HEARTS BUGIS JUNCTION {'\n'}
                  KIMOJ BUGIS+ {'\n'}
                  Sephora Bugis+ {'\n'}
                  Maclink Funan {'\n'}
                  FOSSIL Funan {'\n'}
                  Dark Gallery @ Funan {'\n'}
                  Casterly Laptop Funan {'\n'}
                  Kiehls Plaza Singapura {'\n'}
                  SST&C Plaza Singapura {'\n'}
                  DOROTHY PERKINS Plaza Singapura {'\n'}
                  CROCS Plaza Singapura {'\n'}
                  G2000 Plaza Singapura {'\n'}
                  Nanjing Impression Plaza Singapura {'\n'}
                  Innisfree - Plaza Singapura {'\n'}
                  Kimage Plaza Singapura {'\n'}
                  Etude House@Plaza Singapura {'\n'}
                  Laneige@Plaza Singapura {'\n'}
                  Sephora Plaza Singapura {'\n'}L OCCITANE Plaza Singapura {'\n'}
                  SK Jewellery Plaza Singapura {'\n'}
                  Love & Co Plaza Singapura {'\n'}
                  The Face Shop Plaza Singapura {'\n'}
                  Bakers Brew Plaza Singapura {'\n'}
                  OSIM Plaza Singapura {'\n'}
                  Wacoal Plaza Singapura {'\n'}
                  Pierre Cardin Lingerie PLAZA SINGAPURA {'\n'}
                  YOUNG HEARTS PLAZA SINGAPURA {'\n'}
                  Ramen Keisuke Lobster King Clarke Quay {'\n'}
                  Hush Puppies Apparel IMM {'\n'}
                  ECCO @IMM {'\n'}
                  GUESS IMM {'\n'}
                  Novela IMM {'\n'}
                  Mr Coconut IMM {'\n'}
                  FOX IMM {'\n'}
                  G2000 IMM {'\n'}
                  PUMA IMM {'\n'}
                  CROCS IMM {'\n'}
                  OTO Bodycare IMM {'\n'}
                  Furla IMM {'\n'}
                  SK Jewellery IMM {'\n'}
                  ZTP IMM {'\n'}
                  FOSSIL IMM {'\n'}
                  The North Face IMM {'\n'}
                  OSIM IMM {'\n'}
                  Kappa IMM {'\n'}
                  Beauty Love-IMM {'\n'}
                  Victoria's Secret-IMM {'\n'}
                  TUMI - IMM {'\n'}
                  MICHAEL KORS - IMM {'\n'}
                  Bath & Bodyworks IMM {'\n'}
                  Pierre Cardin Lingerie IMM {'\n'}
                  SORELLA IMM {'\n'}
                  Yves Saint Laurent Raffles City {'\n'}
                  Jo Malone London Raffles City {'\n'}
                  COCOMI RAFFLES CITY {'\n'}
                  DOROTHY PERKINS Raffles City {'\n'}
                  Warehouse Raffles City {'\n'}
                  Bee Cheng Hiang Raffles City {'\n'}
                  G2000 Raffles City {'\n'}
                  Innisfree - Raffles City {'\n'}
                  CROCS Raffles City {'\n'}
                  Furla Raffles City {'\n'}
                  The Face Shop Raffles City {'\n'}L OCCITANE Raffles City {'\n'}
                  CHANEL FBP RAFFLES CITY {'\n'}
                  APM Monaco Raffles City {'\n'}
                  PUMA - Bugis {'\n'} {'\n'}
                  *因商业环境变化，具体微信支付接受情况需跟店员核实
                </View>
              </View>
            </View>
          )}
          {/*1*/}
          {tab == 1 && <View className="promotion-malls">{this.renderEventMalls()}</View>}
          {/*2*/}
          {tab == 2 && (
            <View className="promotion-roles">
              <View className="promotion-roles-content">
                <View className="role-title">活动规则：</View>
                <View className="role-p">1.本优惠活动于2021年12月20日至2022年3月20日期间进行。</View>
                <View className="role-p">2.参与资格：所有微信支付大陆钱包用户，付款时需使用微信支付。</View>
                <View className="role-p">
                  3.参与方法：用户在新加坡凯德活动商户累计消费满人民币1000元，即可获得人民币50元代金券一张（满100元可用），活动期间每人最多领取一张。
                </View>
                <View className="role-p">
                  4.推广期内每一位微信支付钱包内地用户最多可获得1张人民币50元新加坡凱德代金券，用户须于活动时间内亲临消费商场出示代金券，经商场职员核对无误后即可核销。
                </View>
                <View className="role-p">
                  5.上述”每一位微信支付钱包内地用户”是指使用微信支付电子货币包服务的及具有法律行为能力的一名自然人．而非-一𠆤微信账户。于以下情况，有关账号将被视为同-用户。同一微信号、同一手机号码、同一银行卡号、同一身份证号码、同一手机设备：符合其中一个条件者视为同一位用户。为确保用户的领奖资格及账号安全，微信支付会保留暂停或永久停止向相关异常账号发放奖赏的权利及追究权利。
                </View>
                <View className="role-p">6.累计交易发生退款时，代金券可能被收。</View>
                <View className="role-p">
                  7.如果用户以欺诈或不当行为获取和/或使用代金券，微信支付有权取消用户参与推广活动和使用换领券的资格。
                </View>
                <View className="role-p">8.用户参加本活动即表示接受并同意遵守本条款及细则。</View>
                <View className="role-p">9.如有疑问，请致电微信支付顾客服务热线(+86-86013860-3)</View>
                <View className="role-p">10.部分商户无法累计，请注意商户店内活动海报，活动最终解释权归商场和商户所有。</View>
              </View>
            </View>
          )}
          <EndLine margin="10px auto 0" />
        </View>
      </View>
    );
  }
}

export default EventPage as ComponentClass<OwnProps>;
