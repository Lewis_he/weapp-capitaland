import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { Button, Image, ScrollView, View } from '@tarojs/components';

import { connect } from 'react-redux';
import { StateType } from '../../models/accountModel';

import { ConnectProps, ConnectState } from '../../models/connect';
import './index.scss';
import '@tarojs/taro/html.css';
import * as images from '../../static/images';
import { EndLine } from '../../components/EndLine/EndLine';
import { MallCardM } from '../../components/MallCard/MallCardM';
import { global } from '../../../global.data';
import { dateCN, distanceCN } from '../../utils/util';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}

interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;

@connect(({ event, loading }: ConnectState) => ({
  ...event,
  ...loading
}))
class EventPage extends React.Component<IProps, OwnState> {
  lastPosb = 0;
  $instance = getCurrentInstance();
  state = {
    sharing: false,
    show: false,
    tab: 0,
    triggered: false
  };

  componentDidMount() {
    // console.log(this.$instance.router);
    const eventId = this.$instance.router?.params.id;
    this.getEventDetail(eventId);
    this.setState({
      sharing: this.$instance.router?.params.share
    });
  }

  componentDidShow() {}

  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    const eventId = this.$instance.router?.params.id;
    return {
      title: '新加坡凯德商场',
      path: `/subpages/event/index?id=${eventId}&share=true`
    };
  }

  getEventDetail = (id) => {
    const local = Taro.getStorageSync('location');
    const payload = { id: id, locale: {} };
    if (local.latitude) payload.locale['latitude'] = local.latitude;
    if (local.longitude) payload.locale['longitude'] = local.longitude;
    this.props.dispatch({
      type: 'event/getOutlet',
      payload: payload
    });
  };

  onNavBack = () => {
    const { sharing } = this.state;
    sharing
      ? Taro.reLaunch({
          url: '/pages/explore/index'
        })
      : Taro.navigateBack();
  };

  onPageScroll = (e) => {
    // console.log('竖向滚动条位置: ', e.detail.scrollTop); 224 312
    let p = e.detail.scrollTop;
    if (p >= 224 && this.lastPosb < 224) {
      this.setState({ show: true });
    } else if (p <= 224 && this.lastPosb > 224) {
      this.setState({ show: false });
    }
    this.lastPosb = p;
  };

  onTabChange = (index: number) => {
    this.setState({ tab: index }, () => {
      if (index == 1) {
        const _this = this;
        Taro.authorize({
          scope: 'scope.userLocation',
          success: function() {
            console.log('do have scope.userLocation');
            // _this.fetchDataWithLBS();
          },
          fail: function(err) {
            // 处理用户拒绝后的引导...
            if (err.errMsg === 'authorize:fail auth deny') {
              // console.log('firstly get userLocation fail', errMsg);
              Taro.showToast({
                title: '定位失败，请重试。',
                icon: 'none',
                duration: 1500
              });
            } else {
              // console.log('other time get userLocation fail', errMsg);
              _this.openSettingToAuthorize();
            }
          }
        });
      }
    });
  };

  renderEventMalls = () => {
    const { event } = this.props;
    return event.malls.map((item) => {
      const bg = global.live + item.thumbnail?.id;
      // const logo = global.live + item.logo.id;
      const logo = item.logo ? global.live + item.logo.id : images.CGWL;
      const title = (item.titleCn ? item.titleCn + ' ' : '') + item.title;
      const distance = item.distance ? distanceCN(item.distance) : '暂无距离';
      return (
        <MallCardM
          margin="0 auto 15px"
          bg={bg}
          logo={logo}
          title={title}
          distance={distance}
          onClick={() => {
            Taro.navigateTo({ url: `/subpages/mall/index?id=${item.id}` });
          }}
        />
      );
    });
  };

  openSettingToAuthorize = () => {
    const that = this;
    Taro.showModal({
      title: '温馨提示',
      confirmText: '去授权',
      content: '您没有授权地理位置信息，是否授权?',
      success: function({ confirm }) {
        if (confirm) {
          Taro.openSetting({
            success: function(res) {
              // console.log('other time userL openSetting ok: ', res.authSetting)//true ,false
              if (res.authSetting['scope.userLocation']) {
                that.fetchDataWithLBS();
              } else {
                Taro.showToast({
                  title: '授权失败，请重试',
                  icon: 'none',
                  duration: 1000
                });
              }
            }
          });
        } else {
          Taro.showToast({
            title: '定位失败，请重试',
            icon: 'none',
            duration: 1000
          });
        }
      }
    });
  };

  fetchDataWithLBS = () => {
    console.log('fetching data with LBS...');
    const eventId = this.$instance.router?.params.id;
    const that = this;
    Taro.getLocation({
      type: 'wgs84',
      success: function(result) {
        const longitude = result.longitude;
        const latitude = result.latitude;
        const local = { longitude, latitude };
        Taro.setStorageSync('location', local);
        that.getEventDetail(eventId);
      }
    });
  };

  refresherRefresh = () => {
    this.setState({ triggered: true });
    this.getNewLBSInfo();
  };

  getNewLBSInfo = () => {
    const that = this;
    Taro.getSetting({
      success: function(res) {
        if (res.authSetting['scope.userLocation']) {
          Taro.getLocation({
            type: 'wgs84',
            success: function(result) {
              const longitude = result.longitude;
              const latitude = result.latitude;
              const local = { longitude, latitude };
              Taro.setStorageSync('location', local);
            }
          });
        }
      },
      complete: function(res) {
        that.fetchEventNoCache();
      }
    });
  };

  fetchEventNoCache = () => {
    const id = this.$instance.router?.params.id;
    const local = Taro.getStorageSync('location');
    const payload = { refresh: true, id: id, locale: {} };
    if (local.latitude) payload.locale['latitude'] = local.latitude;
    if (local.longitude) payload.locale['longitude'] = local.longitude;
    this.props
      .dispatch({
        type: 'event/getOutlet',
        payload: payload
      })
      .then(() => {
        this.setState({ triggered: false });
      });
  };

  render() {
    const { event } = this.props;
    const { show, tab, sharing, triggered } = this.state;
    let details = event.details ?? '';
    let tac = event.termsAndConditions ?? '';
    details = details.replace(/&amp;/g, '&');
    details = details.replace(/&gt;/g, '>');
    details = details.replace(/&lt;/g, '<');
    tac = tac.replace(/&amp;/g, '&');
    tac = tac.replace(/&gt;/g, '>');
    tac = tac.replace(/&lt;/g, '<');
    return (
      <View className="event-page">
        <van-transition customClass="event-page-header" show={show} name="fade-down">
          <View className="ep-status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
          <View className="ep-nav-bar">新加坡凯德商场</View>
        </van-transition>
        <View className="navigate-back" style={{ top: Taro.$statusBarHeight + 'px' }} onClick={this.onNavBack}>
          <View className={sharing ? 'icon-home' : 'icon-back'} />
        </View>
        <ScrollView
          className="event-page-sv"
          scrollY
          scrollWithAnimation
          onScroll={this.onPageScroll}
          refresherEnabled
          refresherTriggered={triggered}
          onRefresherRefresh={this.refresherRefresh}
        >
          <Image className="event-page-bg" src={event.thumbnail ? global.live + event.thumbnail.id : ''} />

          <View className="func-panel sticky">
            <View className="func-panel-title">{event?.title + (event.titleCn || '')}</View>
            <Button className="func-panel-share" openType="share">
              <View className="ep-share" />
              分享
            </Button>
            <View className="func-panel-timeline">
              <View className="fptl-icon" />
              活动举办时间： {dateCN(event.startDate) + '-' + dateCN(event.endDate)}
            </View>
            <View className="func-panel-tabs">
              <View className={tab == 0 ? 'tab-s tab-active' : 'tab-s'} onClick={() => this.onTabChange(0)}>
                <Image src={tab == 0 ? images.EPIA : images.EPI} className="tab-icon-m" />
                活动说明
              </View>
              <View className={tab == 1 ? 'tab-s tab-active' : 'tab-s'} onClick={() => this.onTabChange(1)}>
                <Image src={tab == 1 ? images.EPLA : images.EPL} className="tab-icon-s" />
                参与商场
              </View>
              <View className={tab == 2 ? 'tab-m tab-active' : 'tab-m'} onClick={() => this.onTabChange(2)}>
                <Image src={tab == 2 ? images.EPRA : images.EPR} className="tab-icon-s" />
                条款及细则
              </View>
            </View>
          </View>
          {/*0*/}
          {tab == 0 && (
            <View className="event-instruction">
              <View key={'details' + event.id} className="taro_html" dangerouslySetInnerHTML={{ __html: details }}></View>
            </View>
          )}
          {/*1*/}
          {tab == 1 && <View className="event-malls">{this.renderEventMalls()}</View>}
          {/*2*/}
          {tab == 2 && (
            <View className="event-roles">
              <View key={'tac' + event.id} className="taro_html" dangerouslySetInnerHTML={{ __html: tac }}></View>
            </View>
          )}
          <EndLine margin="10px auto 0" />
        </ScrollView>
      </View>
    );
  }
}

export default EventPage as ComponentClass<OwnProps>;
