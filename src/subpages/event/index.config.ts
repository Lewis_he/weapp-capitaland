export default {
  navigationBarTitleText: '活动页',
  navigationStyle: 'custom',
  enableShareAppMessage: true,
  usingComponents: {
    'van-transition': '../../components/vant-weapp/dist/transition/index'
  }
};
