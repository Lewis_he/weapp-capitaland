import { Reducer } from 'redux';
import { Model } from 'dva';
import * as Api from '../service/apiService';

export interface StateType {
  events: object[];
  event: object;
}

interface ModelType {
  namespace: string;
  state: StateType;
  effects: {};
  reducers: {
    save: Reducer;
  };
}

const model: Model & ModelType = {
  namespace: 'event',
  state: {
    events: [],
    event: {}
  },
  effects: {
    *search({ payload }, { call, put, select }) {
      const options = payload.refresh ? { cache: false, evict: true } : {};
      const res = yield call(Api.eventsSearch, payload, options);
      if (payload.page) {
        const res_origin = yield select((state) => state.event.events);
        const combine = [...res_origin, ...res.content];
        yield put({ type: 'result', payload: combine });
        return yield select((state) => res.content.length === 10);
      } else {
        yield put({ type: 'result', payload: res.content });
      }
    },
    *getOutlet({ payload }, { call, put }) {
      const options = payload.refresh ? { cache: false, evict: true } : {};
      const res = yield call(Api.eventDetail, payload, options);
      yield put({ type: 'single', payload: res });
    }
  },
  reducers: {
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    result(state, { payload }) {
      return { ...state, events: payload };
    },
    single(state, { payload }) {
      return { ...state, event: payload };
    }
  }
};

export default model;
