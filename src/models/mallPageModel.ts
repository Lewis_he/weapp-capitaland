import { Reducer } from 'redux';
import { Model } from 'dva';
import * as Api from '../service/apiService';
import * as images from '../static/images';
import { global } from '../../global.data';

export interface StateType {
  mallData: Mall;
}

interface ModelType {
  namespace: string;
  state: StateType;
  effects: {};
  reducers: {
    save: Reducer;
  };
}

interface Mall {
  id: number;
  la: number;
  lo: number;
  nav: string;
  bgImage: string;
  logo: string;
  title: string;
  address: string;
}

const model: Model & ModelType = {
  namespace: 'mallPage',
  state: {
    mallData: {
      id: 6,
      la: 1.294,
      lo: 103.85343,
      nav: '来福士城 Raffles City',
      bgImage: global.url + 'raffles.png',
      logo: images.RAFLOGO,
      address: '252 North Bridge Road, Singapore',
      title: '来福士城 Raffles City'
    }
  },
  effects: {
    *load({ payload }, { call, put }) {
      const res = yield call(Api.Demo, { payload });
      if (res.errno === 0) {
        yield put({
          type: 'save',
          payload: {
            topData: res.data // 模拟
          }
        });
      }
    },
    *mock({ payload }, { call, put }) {
      let data = {};
      switch (payload.id) {
        case '6':
          data = {
            id: 6,
            la: 1.2937639,
            lo: 103.8512391,
            nav: '来福士城 Raffles City',
            bgImage: global.url + 'raffles.png',
            logo: images.RAFLOGO,
            address: '252 North Bridge Road, Singapore',
            title: '来福士城 Raffles City'
          };
          break;
        case '19':
          data = {
            id: 19,
            la: 1.2996009,
            lo: 103.8520251,
            nav: '白沙浮',
            bgImage: global.live + 1435,
            logo: global.live + 1434,
            address: '201 Victoria Street, Singapore 188067',
            title: 'Bugis Street 白沙浮'
          };
          break;
        case '12':
          data = {
            id: 12,
            la: 1.2993631,
            lo: 103.852916,
            nav: '白沙浮广场',
            bgImage: global.url + 'bj-bg.png',
            logo: global.live + 1436,
            address: '200 Victoria Street, Singapore 188021',
            title: '白沙浮广场 Bugis Junction'
          };
          break;
        case '13':
          data = {
            id: 13,
            la: 1.2996009,
            lo: 103.8520251,
            nav: '白沙浮娱乐广场',
            bgImage: global.url + 'bugis+.png',
            logo: images.BPLOGO,
            address: '201 Victoria Street, Singapore 188067',
            title: 'Bugis+ 白沙浮娱乐广场'
          };
          break;
        case '18':
          data = {
            id: 18,
            la: 1.2913264,
            lo: 103.8478892,
            nav: '福南 Funan',
            bgImage: global.url + 'funan-sum.png',
            logo: images.FNLOGO,
            address: '107 North Bridge Road, Singapore 179105',
            title: '福南 Funan'
          };
          break;
        case '9':
          data = {
            id: 9,
            la: 1.2906078,
            lo: 103.8442802,
            nav: '克拉码头 Clarke Quay',
            bgImage: global.url + 'cq-bg.png',
            logo: images.CQLOGO,
            address: '3 River Valley Road, Singapore 179024',
            title: '克拉码头 Clarke Quay'
          };
          break;
        case '3':
          data = {
            id: 3,
            la: 1.3348208,
            lo: 103.7446455,
            nav: 'IMM奥特莱斯',
            bgImage: global.url + 'imm-bg.png',
            logo: images.IMMLOGO,
            address: '2 Jurong East Street 21, Singapore 609601',
            title: 'IMM奥特莱斯 IMM outlet mall'
          };
          break;
        case '15':
          data = {
            id: 15,
            la: 1.3005371,
            lo: 103.8430416,
            nav: '狮城大廈',
            bgImage: global.url + 'ps-bg.png',
            logo: images.PSLOGO,
            address: '68 Orchard Road, Singapore 238839',
            title: '狮城大廈 Plaza Singapura'
          };
          break;
      }
      yield put({
        type: 'changeMall',
        payload: data
      });
    }
  },
  reducers: {
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    changeMall(state, { payload }) {
      return { ...state, mallData: payload };
    }
  }
};

export default model;
