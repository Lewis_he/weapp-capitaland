import { Reducer } from 'redux';
import { Model } from 'dva';
import * as Api from '../service/apiService';

export interface StateType {
  malls: object[];
}

interface ModelType {
  namespace: string;
  state: StateType;
  effects: {};
  reducers: {
    save: Reducer;
  };
}

const model: Model & ModelType = {
  namespace: 'mall',
  state: {
    malls: []
  },
  effects: {
    *search({ payload }, { call, put }) {
      const options = payload.refresh ? { cache: false, evict: true } : {};
      const res = yield call(Api.mallSearch, payload, options);
      const original = [...res];
      const siblings = original.filter((mall) => mall.entityName.startsWith('bugis'));
      const findIndex = original.findIndex((mall) => mall.entityName.startsWith('bugis'));
      for (let i = 1; i < siblings.length; i++) {
        let index = original.indexOf(siblings[i]);
        index > 0 && original.splice(index, 1);
      }
      const bugisTown = { id: 'bt', name: 'Bugis Town', logo: '', desc: '', distance: siblings[0]?.distance, siblings };
      original.splice(findIndex, 1, bugisTown);
      // console.log('Bugis Town: ', original);
      yield put({ type: 'result', payload: original });
    },
    *getMall({ payload }, { call, put }) {
      const res = yield call(Api.mallById, payload);
      yield put({ type: 'single', payload: res });
    }
  },
  reducers: {
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    result(state, { payload }) {
      return { ...state, malls: payload };
    },
    single(state, { payload }) {
      return { ...state, malls: [].concat(payload) };
    }
  }
};

export default model;
