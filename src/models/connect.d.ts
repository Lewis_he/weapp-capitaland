import { AnyAction, Dispatch } from 'redux';
import { StateType as BrandState } from './brandModel';
import { StateType as CommonState } from './common';
import { StateType as TabState } from './tabs';
import { StateType as MallPageState } from './mallPageModel';
import { StateType as EventPageState } from './eventModel';
import { StateType as MallState } from './mallModel';
import { StateType as AccountState } from './accountModel';

export interface Loading {
  global: boolean;
  effects: { [key: string]: boolean | undefined };
}

export interface ConnectProps {
  dispatch: Dispatch<AnyAction>;
}

export interface ConnectState {
  loading: Loading;
  common: CommonState;
  tabs: TabState;
  brand: BrandState;
  mallPage: MallPageState;
  event: EventPageState;
  mall: MallState;
  account: AccountState;
}
