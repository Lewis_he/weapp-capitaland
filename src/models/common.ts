import Taro from '@tarojs/taro';
import { Reducer } from 'redux';
import * as Api from '../service/apiService';
import { Model } from 'dva';

export interface StateType {
  searchRes: object[];
  category: object[];
  subCategory: object[];
}

interface ModelType {
  namespace: string;
  state: StateType;
  effects: {};
  reducers: {
    save: Reducer;
  };
}

const model: Model & ModelType = {
  namespace: 'common',
  state: {
    searchRes: [],
    category: [{ id: null, titleCn: '所有商户类型' }],
    subCategory: [{ id: null, titleCn: '所有子类别' }]
  },

  effects: {
    *search({ payload }, { call, put, select }) {
      const options = payload.refresh ? { cache: false, evict: true } : {};
      const res = yield call(Api.gbSearch, payload, options);
      if (payload.page) {
        const res_origin = yield select((state) => state.common.searchRes);
        const combine = [...res_origin, ...res.content];
        yield put({ type: 'result', payload: combine });
        return yield select((state) => res.content.length === 10);
      } else {
        yield put({ type: 'result', payload: res.content });
      }
    },
    *clear({ payload }, { call, put }) {
      yield put({ type: 'result', payload: [] });
    },
    *getCategory({ payload }, { call, put }) {
      const res = yield call(Api.mainCategory, payload);
      yield put({ type: 'categories', payload: res });
    },
    *getSubCategory({ payload }, { call, put, select }) {
      if (payload.parentId) {
        const res = yield call(Api.mainCategory, payload);
        yield put({ type: 'sub_category', payload: res });
        return yield select((state) => res.length > 1);
      } else {
        yield put({ type: 'sub_category', payload: [{ id: null, titleCn: '所有子类别' }] });
        return yield select((state) => false);
      }
    }
  },

  reducers: {
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    result(state, { payload }) {
      return { ...state, searchRes: payload };
    },
    categories(state, { payload }) {
      return { ...state, category: payload };
    },
    sub_category(state, { payload }) {
      return { ...state, subCategory: payload };
    }
  }
};

export default model;
