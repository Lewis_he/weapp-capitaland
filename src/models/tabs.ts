import { Reducer } from 'redux';
import * as Api from '../service/apiService';
import { Model } from 'dva';

export interface StateType {
  explore: object[];
  banners: object[];
}

interface ModelType {
  namespace: string;
  state: StateType;
  effects: {};
  reducers: {
    save: Reducer;
  };
}

const model: Model & ModelType = {
  namespace: 'tabs',
  state: {
    explore: [],
    banners: []
  },

  effects: {
    *search({ payload }, { call, put, select }) {
      const options = payload.refresh ? { cache: false, evict: true } : {};
      const res = yield call(Api.exploreSearch, payload, options);
      if (payload.page) {
        const res_origin = yield select((state) => state.tabs.explore);
        const combine = [...res_origin, ...res.content];
        yield put({ type: 'result', payload: combine });
        return yield select((state) => res.content.length === 10);
      } else {
        yield put({ type: 'result', payload: res.content });
      }
    },
    *getBanners({ payload }, { call, put }) {
      const options = payload?.refresh ? { cache: false, evict: true } : {};
      const res = yield call(Api.bannerSearch, {}, options);
      yield put({ type: 'swiper', payload: res });
    }
  },

  reducers: {
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    result(state, { payload }) {
      return { ...state, explore: payload };
    },
    swiper(state, { payload }) {
      return { ...state, banners: payload };
    }
  }
};

export default model;
