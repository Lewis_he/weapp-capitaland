import { Reducer } from 'redux';
import { Model } from 'dva';
import * as Api from '../service/apiService';

export interface StateType {
  outlets: object[];
  siblingOutlets: object[];
  outlet: object;
}

interface ModelType {
  namespace: string;
  state: StateType;
  effects: {};
  reducers: {
    save: Reducer;
  };
}

const model: Model & ModelType = {
  namespace: 'brand',
  state: {
    outlets: [],
    outlet: {},
    siblingOutlets: []
  },
  effects: {
    *search({ payload }, { call, put, select }) {
      const options = payload.refresh ? { cache: false, evict: true } : {};
      const res = yield call(Api.outletSearch, payload, options);
      if (payload.page) {
        const res_origin = yield select((state) => state.brand.outlets);
        const combine = [...res_origin, ...res.content];
        yield put({ type: 'result', payload: combine });
        return yield select((state) => res.content.length === 10);
      } else {
        yield put({ type: 'result', payload: res.content });
      }
    },
    *getOutlet({ payload }, { call, put, select }) {
      const res = yield call(Api.outletDetail, payload);
      yield put({ type: 'merchant', payload: res });
      return yield select((state) => res.favourite);
    },
    *siblingsOutlet({ payload }, { call, put, select }) {
      const options = payload.refresh ? { cache: false, evict: true } : {};
      const res = yield call(Api.outletSearch, payload, options);
      if (payload.page) {
        const res_origin = yield select((state) => state.brand.siblingOutlets);
        const combine = [...res_origin, ...res.content];
        yield put({ type: 'siblings', payload: combine });
        return yield select((state) => res.content.length === 10);
      } else {
        yield put({ type: 'siblings', payload: res.content });
      }
    },
    *handleFavorite({ payload }, { call, put }) {
      if (payload.isAdd) {
        yield call(Api.favouritesAdd, payload.outlet); //merchant id
      } else {
        yield call(Api.favouritesRemove, payload.outlet); //merchant id
      }
    }
  },
  reducers: {
    save(state, { payload }) {
      return { ...state, ...payload };
    },
    result(state, { payload }) {
      return { ...state, outlets: payload };
    },
    merchant(state, { payload }) {
      return { ...state, outlet: payload };
    },
    siblings(state, { payload }) {
      return { ...state, siblingOutlets: payload };
    }
  }
};

export default model;
