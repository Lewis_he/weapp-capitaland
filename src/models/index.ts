import accountModel from './accountModel';
import mallPageModel from './mallPageModel';
import eventModel from './eventModel';
import mallModel from './mallModel';
import brandModel from './brandModel';
import common from './common';
import tabs from './tabs';

export default [common, tabs, mallModel, brandModel, mallPageModel, eventModel, accountModel];
