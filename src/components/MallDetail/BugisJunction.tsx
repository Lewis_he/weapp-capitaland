import React from 'react';
import './BugisJunction.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { global } from '../../../global.data';

type SelfProps = {
  section: string;
};
export const BugisJunction: React.FC<SelfProps> = ({ section, children }) => (
  <View className="bj-wrap">
    {section == 'summary' && (
      <View className="bj-wrap-summary">
        <Image className="bj-wrap-summary-image" src={global.url + 'bj-summary.png'} />
        <View className="bj-wrap-summary-introduction">
          Bugis
          Junction白沙浮广场位于新加坡市中心武吉士（Bugis）的维多利亚街，中路和北桥路，是新加坡第一个连接3条主干道的购物中心。他和白沙浮娱乐广场以及白沙浮购物街构成了一个充满活力的白沙浮生活城。
          {'\n'}
          Bugis Junction白沙浮广场位于新加坡的文化心脏地带，二层与Bugis Junction
          白沙浮娱乐广场连接；负一层与武吉士地铁站相连，交通十分便捷；毗邻新加坡管理大学，拉萨尔艺术学院和艺术学院，为在周边上课上班的年轻人和专业人士提供丰富的选择！
          {'\n'}
          Bugis
          Junction白沙浮广场拥有新加坡首个也是唯一一个空调天窗购物街，购物街两侧则是一些代表着新旧世界融合的具有历史意义的商铺。除此之外，还有各式各样的咖啡馆供您选择，让您可以在新城市里，感受旧城区的情怀。
          {'\n'}
          Bugis Junction白沙浮广场诚邀您一起在阳光下闲逛，一起来在星空下购物！
        </View>
      </View>
    )}
    {section == 'route' && (
      <View className="bj-wrap-route">
        <View className="bj-wrap-route-mrt">
          <Image className="mrt-icon" src={images.MRT} />
          搭乘地铁
        </View>
        <View className="bj-wrap-route-mrt-card">
          <View className="bj-mrt-title">武吉士地铁站（Bugis MRT station）</View>
          <View className="bj-mrt-ew">东西线EW12</View>
          <View className="bj-mrt-dt">滨海市区线DT14</View>
        </View>
        <View className="bj-wrap-route-divider" />
        <View className="bj-wrap-route-bus">
          <Image className="bus-icon" src={images.BUS} />
          搭乘巴士公交
        </View>

        <View className="bj-wrap-route-bus-card">
          <View className="bj-bus-no">2, 7, 12, 32, 33, 51, 61, 63, 80, 130, 133, 145, 175, 197, 851, 960, 980</View>
        </View>
      </View>
    )}
    {section == 'service' && (
      <View className="bj-wrap-service">
        <View className="bj-wrap-service-wifi">
          <Image className="wifi-icon" src={images.WIFI} />
          免费网络
        </View>
        <View className="bj-wrap-service-wifi-card">
          <View className="bj-wifi-content">
            链接到 SSID：CAPITALAND FREE WIFI 即可免费享用无线网络{'\n'}
            Wi-Fi密码: freewifi (区分大小写).
          </View>
        </View>
        <View className="bj-wrap-service-divider" />
        <View className="bj-wrap-service-customer-service">
          <Image className="customer-service-icon" src={images.CSS} />
          客户服务
        </View>
        <View className="bj-wrap-service-cs-card">
          <View className="bj-cs-title">客户服务柜台</View>
          <View className="bj-cs-details">• 服务柜台：位于商场2层 屈臣氏对面{'\n'}• 服务时间: 11am - 9pm daily</View>
        </View>

        <View className="bj-wrap-service-divider" />

        <View className="bj-wrap-service-customer-service">
          <Image className="nursing-room-icon" src={images.NR} />
          护理室和婴儿室
        </View>
        <View className="bj-wrap-service-nursing-room">
          <View className="bj-nr-content">
            • 婴儿换尿布和儿童保护座椅在商场一楼，二楼以及三楼的女士卫生间内{'\n'}• 婴儿护理室在BHG的三楼
          </View>
        </View>
      </View>
    )}
    {section == 'parking' && (
      <View className="bj-wrap-parking">
        <View className="bj-wrap-parking-fee">
          <Image className="parking-fee-icon" src={images.PARKING} />
          停车收费
        </View>
        <View className="bj-wrap-parking-car-card">
          <Image className="car-card-title" src={images.CAR} />
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周四</Text>
            {'\n'}
            8:00am - 5:00pm: 第一小时 S$2.00{'\n'}
            随后每10分钟 S$0.4{'\n'}
            5:00pm之后: 每次入场 S$3.00
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周五</Text>
            {'\n'}
            8:00am - 5:00pm: 首2小时 S$2.00{'\n'}
            随后每10分钟 S$0.4
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周五 5:00pm后，周六，周日及公共假日</Text>
            {'\n'}
            全天：首2小时 S$3.00{'\n'}
            随后每10分钟 S$0.3
          </View>
          <View className="car-card-panel">
            *上述价格包括7％的消费税{'\n'}
            *宽限期(Grace period)：10分钟
          </View>
        </View>

        <View className="bj-wrap-parking-motor-card">
          <Image className="motor-card-title" src={images.MOTOR} />
          <View className="motor-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周日及公众假期（主要停车场和卸货区）</Text>
            {'\n'}
            每次入场 S$1.07
          </View>
          <View className="motor-card-panel">
            *上述价格包括7％的消费税{'\n'}
            *无宽限期 (No grace period)
          </View>
        </View>
      </View>
    )}

    {children}
  </View>
);
