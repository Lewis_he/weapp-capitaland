import React from 'react';
import './ClarkQuay.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { global } from '../../../global.data';

type SelfProps = {
  section: string;
};
export const ClarkQuay: React.FC<SelfProps> = ({ section, children }) => (
  <View className="cq-wrap">
    {section == 'summary' && (
      <View className="cq-wrap-summary">
        <Image className="cq-wrap-summary-image" src={global.url + 'cq-sum.png'} />
        <View className="cq-wrap-summary-introduction">
          克拉码头 (CQ @ Clarke Quay)
          是位于新加坡河畔的一个独特的保护性地标。它由五排经过修复的19世纪店屋和仓库组成，上方覆盖着五彩缤纷的照明天篷。克拉码头是超过60家餐厅、酒吧和娱乐场所的所在地，无论是白天还是夜晚，您都可以品尝到中式、欧式、亚洲风味和本地美食的多种美味。当太阳下山后，您可以在众多设有现场乐队的酒吧中喝上一杯，或者在当地的标志性夜店Zouk
          Club和其他时尚的夜生活场所狂欢到天明。
        </View>
      </View>
    )}
    {section == 'route' && (
      <View className="cq-wrap-route">
        <View className="cq-wrap-route-mrt">
          <Image className="mrt-icon" src={images.MRT} />
          搭乘地铁
        </View>
        <View className="cq-wrap-route-mrt-card">
          <View className="cq-mrt-title">克拉码头地铁站（Clarke Quay MRT Station）</View>
          <View className="cq-mrt-ne">东北线NE5</View>
          <View className="cq-walk-1">步行约5分钟</View>
        </View>
        <View className="cq-wrap-route-mrt-card">
          <View className="cq-mrt-title">福康宁地铁站（Fort Canning MRT Station）</View>
          <View className="cq-mrt-dt">滨海市区线DT20</View>
          <View className="cq-walk-2">步行约5分钟</View>
        </View>
        <View className="cq-wrap-route-mrt-card">
          <View className="cq-mrt-title">政府大厦地铁站（City Hall MRT station）</View>
          <View className="cq-mrt-ew">东西线EW13</View>
          <View className="cq-mrt-ns">南北线NS25</View>
          <View className="cq-walk-3">步行约10分钟</View>
        </View>
        <View className="cq-wrap-route-divider" />

        <View className="cq-wrap-route-bus">
          <Image className="bus-icon" src={images.BUS} />
          搭乘巴士公交
        </View>
        <View className="cq-wrap-route-bus-card">
          <View className="cq-bus-head">
            54 位于 Scotts Road 的巴士站{'\n'}
            32, 195 位于 City Hall MRT Station的巴士站
          </View>
          <View className="cq-bus-title">SMRT Night Rider</View>
          <View className="cq-bus-no">
            NR1, NR2, NR3, NR5, NR6, NR7, NR8{'\n'}
            营业时间： 星期五，星期六和公众假期前夕 晚上11.30至4.35am
          </View>
          <View className="cq-bus-title">SBS Nite Owl</View>
          <View className="cq-bus-no">
            1N, 2N, 3N, 4N, 5N, 6N{'\n'}
            营业时间： 星期五，星期六和公众假期前夕 12:00 am至 2:00am（从城市出发）
          </View>
        </View>

        <View className="cq-wrap-route-bus">
          <Image className="blue-sg-icon" src={images.BSG} />
          BlueSG汽车共享服务
        </View>
        <View className="cq-wrap-route-bsg-card">
          <View className="cq-bsg-content">
            三楼停车场E座{'\n'}
            Block E, Level 3 Carpark
          </View>
        </View>

        <View className="cq-wrap-route-bus">
          <Image className="river-taxi-icon" src={images.RT} />
          River Taxi 水上出租车服务
        </View>
        <View className="cq-wrap-route-river-taxi-card">
          <View className="cq-river-taxi-head">Singapore River Cruise</View>
          <View className="cq-river-taxi-body">从 驳船码头Boat Quay Jetty 到 克拉码头 Clarke Quay Jetty 5分钟</View>
          <View className="cq-river-taxi-head">WaterB River Taxi</View>
          <View className="cq-river-taxi-body">
            莱佛士坊Raffles Place kiosk (靠近UOB大楼) 到 福康宁Fort Canning Kiosk (靠近Clarke Quay) 5分钟
          </View>
        </View>
      </View>
    )}
    {section == 'service' && (
      <View className="cq-wrap-service">
        <View className="cq-wrap-service-wifi">
          <Image className="wifi-icon" src={images.WIFI} />
          免费网络
        </View>
        <View className="cq-wrap-service-wifi-card">
          <View className="cq-wifi-content">链接到 SSID：CAPITALAND FREE WIFI 即可免费享用无线网络</View>
        </View>
      </View>
    )}
    {section == 'parking' && (
      <View className="cq-wrap-parking">
        <View className="cq-wrap-parking-fee">
          <Image className="parking-fee-icon" src={images.PARKING} />
          停车收费
        </View>
        <View className="cq-wrap-parking-car-card">
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周五</Text>
            {'\n'}
            7:00am - 12:29pm: 第一小时 S$1.28{'\n'}
            随后的15分钟收费为 S$0.43{'\n'}
            12:30pm - 1:29pm: 免费{'\n'}
            1:30pm - 6:59am: 第1小时为S$1.28，随后每15分钟为 S$0.43 （5pm以后 封顶 S$6.42）
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周六</Text>
            {'\n'}
            7:00am - 6:59am: 第1小时为S$1.28，随后每15分钟为 S$0.43 （5pm以后 封顶 S$6.42）
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周日及公共假日</Text>
            {'\n'}
            7:00am - 6:59am: 每次入场 S$2.68
          </View>
        </View>
      </View>
    )}

    {children}
  </View>
);
