import React from 'react';
import './BugisStreet.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { global } from '../../../global.data';

type SelfProps = {
  section: string;
};
export const BugisStreet: React.FC<SelfProps> = ({ section, children }) => (
  <View className="bs-wrap">
    {section == 'summary' && (
      <View className="bs-wrap-summary">
        <Image className="bs-wrap-summary-image" src={global.live + 1433} />
        <View className="bs-wrap-summary-introduction">
          白沙浮(Bugis Street)
          是新加坡最大的购物街之一，拥有600多家商店。它是当地人和游客都非常喜欢的购物场所，同时也是在新加坡购买纪念品，饰品，衣服和化妆品的目的地。当您在商店的迷宫中漫步时，白沙浮（Bugis
          Street）还提供许多餐饮选择，以使饥饿的购物者能够大快朵颐。
        </View>
      </View>
    )}
    {section == 'route' && (
      <View className="bs-wrap-route">
        <View className="bs-wrap-route-mrt">
          <Image className="mrt-icon" src={images.MRT} />
          搭乘地铁
        </View>
        <View className="bs-wrap-route-mrt-card">
          <View className="bs-mrt-title">武吉士地铁站（Bugis MRT station）</View>
          <View className="bs-mrt-ew">东西线EW12</View>
          <View className="bs-mrt-dt">滨海市区线DT14</View>
        </View>
        <View className="bs-wrap-route-divider" />
        <View className="bs-wrap-route-bus">
          <Image className="bus-icon" src={images.BUS} />
          搭乘巴士公交
        </View>

        <View className="bs-wrap-route-bus-card">
          <View className="bs-bus-no">
            SBS : 2, 12, 32, 51, 63, 80, 124, 142, 145, 166, 174, 174E, 197{'\n'}
            SMRT : 61, 851, 960,980, NR7
          </View>
        </View>
      </View>
    )}
    {section == 'service' && (
      <View className="bs-wrap-service">
        <View className="bs-wrap-service-customer-service">
          <Image className="customer-service-icon" src={images.CSS} />
          客户服务
        </View>
        <View className="bs-wrap-service-cs-card">
          <View className="bs-cs-details">
            • 最近的客户服务柜台位于白沙浮广场(Bugis Junction) 二层（靠近Watson’s）。服务时间每天上午11点至晚上9点。{'\n'}•
            如需更多查询或报告任何丢失的物品，您可以写信到retail@capitaland.com或在每天的上午10点至下午6点之间拨打+65 6631
            9931的客户服务热线。
          </View>
        </View>

        <View className="bs-wrap-service-divider" />

        <View className="bs-wrap-service-customer-service">
          <Image className="family-service-icon" src={images.FS} />
          家庭服务
        </View>
        <View className="bs-wrap-service-family-card">
          <View className="family-service-title">护理室</View>
          <View className="family-service-detail">位于白沙浮娱乐广场(Bugis + ) 1层至4层</View>
        </View>
      </View>
    )}
    {section == 'parking' && (
      <View className="bs-wrap-parking">
        <View className="bs-wrap-parking-fee">
          <Image className="parking-fee-icon" src={images.PARKING} />
          停车收费
        </View>
        <View className="bs-wrap-parking-car-card">
          <Image className="car-card-title" src={images.CAR} />
          <View className="car-parking-tips">最近的停车场在白沙浮娱乐广场(Bugis+ )</View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周四</Text>
            {'\n'}
            8:00am - 5:59pm: 第一小时 S$1.28{'\n'}
            随后每10分钟 S$0.54{'\n'}
            6:00pm - 7:59am(第二天): 每次入场 S$3.21
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周五</Text>
            {'\n'}
            8:00am - 5:59pm: 第一小时 S$1.28{'\n'}
            6:00pm - 7:59am(第二天): 首2小时 S$3.21 随后的每15分钟 S$0.54
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周六，周日及公共假日</Text>
            {'\n'}
            首2小时 S$3.21 随后的每15分钟 S$0.54
          </View>
          <View className="car-card-panel">
            *上述价格包括7％的消费税{'\n'}
            *宽限期(Grace period)：10分钟
          </View>
        </View>
      </View>
    )}

    {children}
  </View>
);
