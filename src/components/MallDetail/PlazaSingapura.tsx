import React from 'react';
import './PlazaSingapura.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { global } from '../../../global.data';

type SelfProps = {
  section: string;
};
export const PlazaSingapura: React.FC<SelfProps> = ({ section, children }) => (
  <View className="psg-wrap">
    {section == 'summary' && (
      <View className="psg-wrap-summary">
        <Image className="psg-wrap-summary-image" src={global.url + 'plaza-sga.png'} />
        <View className="psg-wrap-summary-introduction">
          Plaza Singapura狮城大厦
          位于新加坡主购物区乌节路和文化区的交界处，以及3条地铁线路（南北线，东北线以及环线）的上盖。商场地下2层直通地铁。Plaza
          Singapura狮城大厦进驻商铺种类广泛，可以满足各类大朋友、小朋友，也就是各位家庭成员的需求。商场内部1、3、4层均与The Atrium @ Orchard
          相连接，尤其方便！
        </View>
      </View>
    )}
    {section == 'route' && (
      <View className="psg-wrap-route">
        <View className="psg-wrap-route-mrt">
          <Image className="mrt-icon" src={images.MRT} />
          搭乘地铁
        </View>
        <View className="psg-wrap-route-mrt-card">
          <View className="psg-mrt-title">多美歌地铁站（Dhoby Ghaut MRT station）</View>
          <View className="psg-mrt-ns">南北线NS24</View>
          <View className="psg-mrt-ne">东北线NE6</View>
          <View className="psg-mrt-cc">地铁环线CC1</View>
        </View>
        <View className="psg-wrap-route-divider" />

        <View className="psg-wrap-route-bus">
          <Image className="bus-icon" src={images.BUS} />
          搭乘巴士公交
        </View>
        <View className="psg-wrap-route-bus-card">
          <View className="psg-bus-no">
            7, 14, 14e, 16, 36, 77, 106, 111, 124, 128, 162, 162M, 167, 167e, 171, 174, 174e, 175, 190, 502, 502A, 518, 518A, 625, 652, 656,
            660, 663, 665, 700, 700A, 850E, 951E, 971E, 972, 1N, 2N, 3N, 4N, 5N, 6N, NR6
          </View>
        </View>
        <View className="psg-wrap-route-divider" />

        <View className="psg-wrap-route-bus">
          <Image className="psg-entrance-icon" src={images.ETS} />
          狮城大廈入口
        </View>
        <View className="psg-wrap-route-entrance-card">
          <View className="psg-entrance-head">购物者可以通过以下入口进入狮城大廈：</View>
          <View className="psg-entrance-content">
            • 地下2层 MRT地铁站{'\n'}• 1层 出租车站，靠近土司工坊(Toastbox){'\n'}• 1层 靠近星巴克(Starbucks){'\n'}• 3层 停车场{'\n'}• 6层
            停车场
          </View>
          <View className="psg-entrance-content">上面未列出的入口将被关闭。在努力为所有人提供安全的购物环境时，我们寻求您的谅解与合作</View>
        </View>
      </View>
    )}
    {section == 'service' && (
      <View className="psg-wrap-service">
        <View className="psg-wrap-service-wifi">
          <Image className="wifi-icon" src={images.WIFI} />
          免费网络
        </View>
        <View className="psg-wrap-service-wifi-card">
          <View className="psg-wifi-content">链接到 SSID：CAPITALAND FREE WIFI 即可免费享用无线网络</View>
        </View>
        <View className="psg-wrap-service-divider" />

        <View className="psg-wrap-service-customer-service">
          <Image className="customer-service-icon" src={images.CSS} />
          客户服务
        </View>
        <View className="psg-wrap-service-cs-card">
          <View className="psg-cs-details">从2020年10月1日起，我们的客户服务柜台将每天上午11点至晚上9点开放。</View>
        </View>
        <View className="psg-wrap-service-divider" />

        <View className="psg-wrap-service-customer-service">
          <Image className="family-service-icon" src={images.FS} />
          家庭服务
        </View>
        <View className="psg-wrap-service-family-card">
          <View className="psg-fs-title">护理室</View>
          <View className="psg-fs-detail">
            • 位于2楼 靠女士洗手间{'\n'}• 位于3楼 靠女士洗手间{'\n'}• 位于6楼 靠女士洗手间
          </View>
        </View>
      </View>
    )}
    {section == 'parking' && (
      <View className="psg-wrap-parking">
        <View className="psg-wrap-parking-fee">
          <Image className="parking-fee-icon" src={images.PARKING} />
          停车收费
        </View>
        <View className="psg-wrap-parking-card">
          <Image className="car-card-title" src={images.CAR} />
          <View className="psg-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周四</Text>
            {'\n'}
            12:00am - 5:59pm: 每小时 S$1.60{'\n'}
            随后每15分钟或以下: S$0.50{'\n'}
            6:00pm-11:59pm: 每次入场 S$3.00
          </View>
          <View className="psg-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>周五以及公共假日前夕</Text>
            {'\n'}
            12:00am - 5:59pm: 每小时 S$1.60{'\n'}
            随后每15分钟或以下: S$0.50{'\n'}
            6.00pm-2.59am 第二天：每次入场 S$3.00
          </View>
          <View className="psg-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>周六</Text>
            {'\n'}
            3.00am - 5.59pm: 首个2小时 S$3.00{'\n'}
            随后每15分钟或以下: S$0.50{'\n'}
            6.00pm-2.59am 第二天：每次入场 S$3.00
          </View>
          <View className="psg-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>周日以及公共假日</Text>
            {'\n'}
            3.00am - 5.59pm: 首个2小时 S$3.00{'\n'}
            随后每15分钟或以下: S$0.50{'\n'}
            当天6.00pm-11.59pm：每次入场 S$3.00
          </View>
        </View>

        <View className="psg-wrap-parking-card">
          <Image className="motor-card-title" src={images.MOTOR} />
          <View className="psg-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周日以及公共假期</Text>
            {'\n'}
            每次入场: S$1.28{'\n'}
            高度限制: 2.1米
          </View>
        </View>

        <View className="psg-wrap-parking-gst-card">* 上述价格包括7％的消费税。</View>
      </View>
    )}

    {children}
  </View>
);
