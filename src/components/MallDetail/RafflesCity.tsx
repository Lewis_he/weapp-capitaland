import React from 'react';
import './RafflesCity.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { global } from '../../../global.data';

type SelfProps = {
  section: string;
};
export const RafflesCity: React.FC<SelfProps> = ({ section, children }) => (
  <View className="rc-wrap">
    {section == 'summary' && (
      <View className="rc-wrap-summary">
        <Image className="rc-wrap-summary-image" src={global.url + 'raf-night-scenary.png'} />
        <View className="rc-wrap-summary-introduction">
          莱佛士城由世界著名的建筑师贝聿铭设计为“城市中的城市”，将乌节路的游客和购物大动脉与莱佛士坊及其周边的商业和金融区联系在一起。
          该建筑群包括莱佛士城购物中心，莱佛士城大厦，莱佛士城会议中心， Swissotel The Stamford和新加坡费尔蒙酒店（Fairmont Singapore）。
          莱佛士城购物中心是一个主要的零售购物中心，分布在1、2、3层和1和2层地下的五层楼中。在罗宾逊百货公司和Raffles City Market
          Place（一家美食超市）的停泊处，莱佛士城购物中心目前拥有200多家特色商家。
          来福士广场购物中心直接与环线沿线的市政厅地铁换乘站和滨海地铁站相连。
        </View>
      </View>
    )}
    {section == 'route' && (
      <View className="rc-wrap-route">
        <View className="rc-wrap-route-mrt">
          <Image className="mrt-icon" src={images.MRT} />
          搭乘地铁
        </View>
        <View className="rc-wrap-route-mrt-1">
          <View className="mrt-1-title">政府大厦地铁站（City Hall MRT station）</View>
          <View className="mrt-1-ew">东西线EW13</View>
          <View className="mrt-1-ns">南北线NS25</View>
        </View>
        <View className="rc-wrap-route-mrt-2">
          <View className="mrt-2-title">滨海中心地铁站（Esplanade MRT station）</View>
          <View className="mrt-2-cc">地铁环线（CC3，G出口</View>
        </View>
        <View className="rc-wrap-route-divider" />
        <View className="rc-wrap-route-taxi">
          <Image className="taxi-icon" src={images.TAXI} />
          出租车接送点
        </View>
        <View className="rc-wrap-route-taxi-card">
          <View className="rc-taxi-p1">Stamford Road ( Robinsons 外 )</View>
          <View className="rc-taxi-pc">位置码: C14</View>
          <View className="rc-taxi-p2">North Bridge Road ( Nespresso 外)</View>
          <View className="rc-taxi-pc p2">位置码: C15</View>
        </View>
        <View className="rc-wrap-route-divider" />
        <View className="rc-wrap-route-taxi">
          <Image className="bus-icon" src={images.BUS} />
          搭乘巴士公交
        </View>

        <View className="rc-wrap-route-bus-card">
          <View className="rc-bus-title">位于Bras Basah Road的巴士站</View>
          <View className="rc-bus-no">
            14, 14E, 16, 36, 77, 106, 111, 128, 130, 131, 133, 162, 162M, 167, 171, 502, 502A, 518, 518A, 700, 700A, 850E, 857, 951E, 960
          </View>
        </View>
        <View className="rc-wrap-route-bus-card">
          <View className="rc-bus-title">位于Stamford Road的巴士站</View>
          <View className="rc-bus-no">
            7, 14, 14E, 16, 36, 77, 106, 111, 128, 131, 162, 162M, 167, 171, 175, 700, 700A, 850E, 857, 951E, 971E
          </View>
        </View>
        <View className="rc-wrap-route-bus-card">
          <View className="rc-bus-title">位于North Bridge Road的巴士站</View>
          <View className="rc-bus-no">61, 124, 145, 166, 174, 174E, 197, NR1, NR2, NR5, NR6, NR7, NR8, 1N, 2N, 3N, 4N, 5N, 6N</View>
        </View>
        <View className="rc-wrap-route-divider last" />

        <View className="rc-wrap-route-taxi">
          <Image className="car-icon" src={images.P} />
          自驾停车
        </View>
        <View className="rc-wrap-route-car-park">从B1到B3拥有1,000多个停车位。</View>
      </View>
    )}
    {section == 'service' && (
      <View className="rc-wrap-service">
        <View className="rc-wrap-service-wifi">
          <Image className="wifi-icon" src={images.WIFI} />
          免费网络
        </View>
        <View className="rc-wrap-service-wifi-card">
          <View className="rc-wifi-content">
            链接到 SSID：CAPITALAND FREE WIFI 即可免费享用无线网络{'\n'}
            Wi-Fi密码: freewifi (区分大小写).
          </View>
        </View>
        <View className="rc-wrap-service-divider" />
        <View className="rc-wrap-service-customer-service">
          <Image className="customer-service-icon" src={images.CSS} />
          客户服务
        </View>
        <View className="rc-wrap-service-cs-card">
          <View className="rc-cs-title">CONCIERGE 礼宾部</View>
          <View className="rc-cs-details">
            • 服务柜台：位于商场1层 #01-36G{'\n'}• 服务时间: 11am - 9pm daily{'\n'}• 联络方式: +65 6318 0238
          </View>
        </View>
        <View className="rc-wrap-service-cs-card">
          <View className="rc-cs-title">其他服务</View>
          <View className="rc-cs-details">
            • CapitaVoucher 销售 ( 5, 10, 50新币面额){'\n'}• CapitaStar 积分兑换{'\n'}• SISTIC{'\n'}• 婴儿车，轮椅，雨伞和移动电源租赁{'\n'}
            • 巡回礼宾
          </View>
        </View>
        <View className="rc-wrap-service-cs-card">
          <View className="rc-cs-title">SISTIC</View>
          <View className="rc-cs-details">
            SISTIC柜台营业时间：每天中午12点至晚上8点{'\n'}
            您也可以在www.sistic.com.sg在线购买门票
          </View>
        </View>
        <View className="rc-wrap-service-divider" />

        <View className="rc-wrap-service-customer-service">
          <Image className="nursing-room-icon" src={images.NR} />
          护理室和婴儿室
        </View>
        <View className="rc-wrap-service-nursing-room">
          <View className="rc-nr-content">
            • 地下1层（洗手间隔壁，靠近 Buffet Town, #B1-44E 与 Crumpler, #B1-23）{'\n'}• 三楼（洗手间隔壁，靠近P.S. Cafe ＃03-37）{'\n'}
            主要功能：冷热饮水机，婴儿尿布分配器，沙发与带电源插头的护理室
          </View>
        </View>
        <View className="rc-wrap-service-divider" />

        <View className="rc-wrap-service-customer-service">
          <Image className="atm-icon" src={images.ATM} />
          银行ATM与AXS机器
        </View>
        <View className="rc-wrap-service-atm-card">
          <View className="rc-atm-logos">
            <Image className="rc-atm-logo" src={images.AXS} />
            <Image className="rc-atm-logo" src={images.CITI} />
            <Image className="rc-atm-logo" src={images.DBS} />
            <Image className="rc-atm-logo" src={images.HSBC} />
            <Image className="rc-atm-logo" src={images.OCBC} />
            <Image className="rc-atm-logo" src={images.UOB} />
          </View>
          <View className="rc-atm-names">
            AXS Machine (#B1-K12){'\n'}
            花旗银行 Citibank ATM (#B1-43E){'\n'}
            星展银行 DBS ATM (#B1-43C/D){'\n'}
            汇丰银行 HSBC ATM (#B1-43A){'\n'}
            华侨银行 OCBC ATM (#B1-43B){'\n'}
            大华银行 UOB Bank (#B1-04/05)
          </View>
        </View>
        <View className="rc-wrap-service-divider" />

        <View className="rc-wrap-service-customer-service">
          <Image className="currency-exchange-icon" src={images.CE} />
          货币兑换
        </View>
        <View className="rc-wrap-service-dash-panel">
          <View className="rc-panel-content">• Clifford Gems and Money Exchange (#B2-08){'\n'}• SAJ Exchange (#B1-03)</View>
        </View>
        <View className="rc-wrap-service-divider" />

        <View className="rc-wrap-service-customer-service">
          <Image className="clinic-icon" src={images.CLINIC} />
          诊所
        </View>
        <View className="rc-wrap-service-dash-panel">
          <View className="rc-panel-content">• Drs Singh & Partners (#02-16){'\n'}• Raffles Medical (#02-17)</View>
        </View>
        <View className="rc-wrap-service-divider" />

        <View className="rc-wrap-service-customer-service">
          <Image className="dry-cleaner-icon" src={images.DCS} />
          干洗店
        </View>
        <View className="rc-wrap-service-dash-panel">
          <View className="rc-panel-content">• JEEVES London's Finest Dry Cleaners (#B1-97)</View>
        </View>
        <View className="rc-wrap-service-divider" />

        <View className="rc-wrap-service-customer-service">
          <Image className="repair-shop-icon" src={images.RS} />
          修鞋，配钥匙和皮革制品服务
        </View>
        <View className="rc-wrap-service-dash-panel">
          <View className="rc-panel-content">• Master Fix Services (#B2-09)</View>
        </View>
      </View>
    )}
    {section == 'parking' && (
      <View className="rc-wrap-parking">
        <View className="rc-wrap-parking-fee">
          <Image className="parking-fee-icon" src={images.PARKING} />
          停车收费
        </View>
        <View className="rc-wrap-parking-car-card">
          <Image className="car-card-title" src={images.CAR} />
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周五</Text>
            {'\n'}
            8am to 5.59pm: 第一小时 S$ 2.20{'\n'}
            随后每15分钟S$ 0.55{'\n'}
            6pm to 7.59am（第二天): 每次入场 S$ 3.00
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>星期六，星期日及公众假期</Text>
            {'\n'}
            8am to 7.59am（第二天）首2小时S$ 2.20{'\n'}
            第3和第4小时的后续每15分钟为S$ 0.40{'\n'}
            从第5小时起，每15分钟S$ 0.60
          </View>
        </View>
        <View className="rc-wrap-parking-motor-card">
          <Image className="motor-card-title" src={images.MOTOR} />
          <View className="motor-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周日</Text>
            {'\n'}
            8am to 7.59am (第二天): 每次入场S$ 1.00
          </View>
        </View>
        <View className="rc-wrap-parking-tips">* 现行的GST税率适用于所有停车费。</View>
      </View>
    )}

    {children}
  </View>
);
