import React from 'react';
import './IMM.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { global } from '../../../global.data';

type SelfProps = {
  section: string;
};
export const IMM: React.FC<SelfProps> = ({ section, children }) => (
  <View className="imm-wrap">
    {section == 'summary' && (
      <View className="imm-wrap-summary">
        <Image className="imm-wrap-summary-image" src={global.url + 'imm.png'} />
        <View className="imm-wrap-summary-introduction">
          IMM奥特莱斯是新加坡最大的奥特莱斯，拥有超过90家奥莱，全年活动不断，提供低至2折起的折扣。进驻品牌包括但不仅限：Adidas, Calvin
          Klein, Club 21, COACH, Cole Haan, Converse, ECCO, FILA, FitFlop, Furla, GEOX, Hush Puppies, Kate Spade New York, Michael Kors, New
          Balance, Nike, Onitsuka Tiger, Puma, Sacoor, Samsonite, Timberland, TUMI and Under Armour 等。{'\n'}
          {'\n'}
          IMM奥特莱斯位置十分便利，位于新加坡西部裕廊东交通枢纽。除了来IMM找名牌折扣以外，如果你有装修需求，你也可以来IMM！因为这里的家具、家电和各种装修装饰用品，都一应俱全！除此之外，还有巨型超市和Best
          Denki以及Daiso这种能满足你购买生活用品必需品的店铺，真的超好逛哦！{'\n'}
          {'\n'}
          除了好逛，IMM还很好吃！
          IMM里面有超过50家餐饮店，当中包括新加坡本土美食品牌——长堤海鲜、三盅两件以及超人气王海底捞，除了餐厅，还有很多小吃店，大家可以尽情“扫街”！
          {'\n'}
          {'\n'}
          除了满足成年人买买买、吃吃吃的需求以外，IMM还有一个位于3楼的运动场，满足各个年龄段小朋友快乐玩耍的愿望！IMM奥特莱斯真真是一个家庭乐的好去处！
        </View>
      </View>
    )}
    {section == 'route' && (
      <View className="imm-wrap-route">
        <View className="imm-wrap-route-mrt">
          <Image className="mrt-icon" src={images.MRT} />
          搭乘地铁
        </View>
        <View className="imm-wrap-route-mrt-card">
          <View className="imm-mrt-title">裕廊东地铁站（Jurong East MRT Station）</View>
          <View className="imm-mrt-ns">南北线NS1</View>
          <View className="imm-mrt-ew">东西线EW24</View>
          <View className="imm-mrt-je">裕廊区域线JE5</View>
        </View>
        <View className="imm-wrap-route-divider" />

        <View className="imm-wrap-route-bus">
          <Image className="bus-icon" src={images.BUS} />
          搭乘巴士公交
        </View>
        <View className="imm-wrap-route-bus-card">
          <View className="imm-bus-no">52, 105, 188, 333, 502 & 990</View>
        </View>
      </View>
    )}
    {section == 'service' && (
      <View className="imm-wrap-service">
        <View className="imm-wrap-service-wifi">
          <Image className="wifi-icon" src={images.WIFI} />
          免费网络
        </View>
        <View className="imm-wrap-service-wifi-card">
          <View className="imm-wifi-content">链接到 SSID：CAPITALAND FREE WIFI 即可免费享用无线网络</View>
        </View>
        <View className="imm-wrap-service-divider" />

        <View className="imm-wrap-service-customer-service">
          <Image className="customer-service-icon" src={images.CSS} />
          客户服务
        </View>
        <View className="imm-wrap-service-cs-card">
          <View className="imm-cs-details">
            • 服务柜台：位于商场1层 靠近McDonald's{'\n'}• 服务时间: 11am - 9pm daily{'\n'}• 最后一个排队号在9点发放
          </View>
          <View className="imm-cs-title">其他服务</View>
          <View className="imm-cs-details">• 婴儿车，轮椅租赁{'\n'}• 失物招领</View>
        </View>
        <View className="imm-wrap-service-divider" />

        <View className="imm-wrap-service-customer-service">
          <Image className="family-service-icon" src={images.FS} />
          家庭服务
        </View>
        <View className="imm-wrap-service-family-card">
          <View className="imm-fs-title">游乐场</View>
          <View className="imm-fs-detail">IMM在3楼的户外花园广场（outdoor Garden Plaza）上设有一个干湿的游乐场。</View>
          <View className="imm-fs-content">
            <Text style={{ fontSize: '14px', letterSpacing: '0.5px', lineHeight: 1.643 }}>湿游乐场的营业时间：</Text>
            {'\n'}
            周一至周五：10:30am至9.30pm(逢星期四下午七时起及每月第一个星期三休息, 公共假日维护除外）{'\n'}
            周六和周日：10:00am至9:30pm
          </View>
          <View className="imm-fs-tips">注意：湿式和干式游乐场均关闭，直至另行通知</View>
        </View>
        <View className="imm-wrap-service-family-card">
          <View className="imm-fs-title">护理室</View>
          <View className="imm-fs-detail">
            • 位于2楼 (Kohong Health Care Centre后){'\n'}• 位于2楼 (靠近 OSIM){'\n'}• 位于3楼 (停车场入口附近)
          </View>
        </View>
        <View className="imm-wrap-service-divider" />

        <View className="imm-wrap-service-customer-service">
          <Image className="free-bus-icon" src={images.FBS} />
          免费穿梭巴士服务
        </View>
        <View className="imm-wrap-service-free-bus-card">
          <View className="imm-free-bus-content">
            International Business Park{'\n'}
            来回于 International Business Park{'\n'}
            周一至周五: 11:30am至2:00pm，公共假期除外{'\n'}
            间隔10分钟
          </View>
        </View>
      </View>
    )}
    {section == 'parking' && (
      <View className="imm-wrap-parking">
        <View className="imm-wrap-parking-fee">
          <Image className="parking-fee-icon" src={images.PARKING} />
          停车收费
        </View>
        <View className="imm-wrap-parking-card">
          <Image className="car-card-title" src={images.CAR} />
          <View className="imm-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>工作日停车费用</Text>
            {'\n'}
            第一小时: 免费{'\n'}
            首次进入的第二个小时或随后的同日进入的第一个小时 S$1.07 随后的每15分钟 S$0.30
          </View>
          <View className="imm-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>周六，周日及公众假期停车费</Text>
            {'\n'}
            前2小时: S$1.07 （仅限首次入场）{'\n'}
            首次进入的第三小时或随后同一天进入的第一小时 S$1.07 随后的每15分钟 S$0.30
          </View>
        </View>

        <View className="imm-wrap-parking-card">
          <Image className="motor-card-title" src={images.MOTOR} />
          <View className="imm-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>工作日</Text>
            {'\n'}
            第一小时: 免费（仅限首次进入，公共假期除外）{'\n'}
            首次入场之后的时间：一次性收取 S$1.07{'\n'}
            随后的同一天入场：S$1.07
          </View>
          <View className="imm-parking-card-panel">
            <Text style={{ fontWeight: 500 }}>周六，周日及公众假期停车费</Text>
            {'\n'}
            前2小时：S$1.07 （仅限首次入场）{'\n'}
            首次入场之后的时间：一次性收取 S$1.07{'\n'}
            随后的同一天入场：S$1.07
          </View>
        </View>

        <View className="imm-wrap-parking-gst-card">
          * 现行的GST税率适用于所有停车费。{'\n'}* 宽限期：所有车辆10分钟（自2020年10月1日起）
        </View>
      </View>
    )}

    {children}
  </View>
);
