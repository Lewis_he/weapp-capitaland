import React from 'react';
import './Funan.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { global } from '../../../global.data';

type SelfProps = {
  section: string;
};
export const Funan: React.FC<SelfProps> = ({ section, children }) => (
  <View className="fn-wrap">
    {section == 'summary' && (
      <View className="fn-wrap-summary">
        <Image className="fn-wrap-summary-image" src={global.url + 'funan-bg.png'} />
        <View className="fn-wrap-summary-introduction">
          Funan福南是一个由千禧一代为千禧一代设计的项目。Funan福南位于市中心，交通便利，而且有直接通往市政厅(City
          Hall）地铁换乘站的地下通道且邻近CBD金融中心以及克拉码头。而且作为工、娱双用项目，Funan福南提供购物广场，办公和服务式住宅齐头并进的服务，希望能够吸引更多年轻有活力的消费者。
        </View>
      </View>
    )}
    {section == 'route' && (
      <View className="fn-wrap-route">
        <View className="fn-wrap-route-mrt">
          <Image className="mrt-icon" src={images.MRT} />
          搭乘地铁
        </View>
        <View className="fn-wrap-route-mrt-card">
          <View className="fn-mrt-title">政府大厦地铁站（City Hall MRT station）</View>
          <View className="fn-mrt-ew">东西线EW13</View>
          <View className="fn-mrt-ns">南北线NS25 </View>
        </View>
        <View className="fn-wrap-route-divider" />

        <View className="fn-wrap-route-bus">
          <Image className="bus-icon" src={images.BUS} />
          搭乘巴士公交
        </View>
        <View className="fn-wrap-route-bus-card">
          <View className="fn-bus-title">位于City Hall Station ( B出口 ) 的巴士站</View>
          <View className="fn-bus-no">32, 51, 63, 80, 195, 195A, 851, 851e, 961, 961C</View>
        </View>
        <View className="fn-wrap-route-bus-card">
          <View className="fn-bus-title">Treasury（财政部）对面的巴士站</View>
          <View className="fn-bus-no">
            1N, 2N, 3N, 4N, 5N, 6N, 51, 61, 63, 80, 124, 145, 166, 174, 174E, 197, 851, 851E, 961, 961C, NR1, NR2, NR5, NR6, NR7, NR8{' '}
          </View>
        </View>

        <View className="fn-wrap-route-self-driving">
          <Image className="self-driving-icon" src={images.P} />
          自驾停车
        </View>
        <View className="fn-wrap-route-self-driving-park">从B3到B4有400多个停车位。</View>
      </View>
    )}
    {section == 'service' && (
      <View className="fn-wrap-service">
        <View className="fn-wrap-service-wifi">
          <Image className="wifi-icon" src={images.WIFI} />
          免费网络
        </View>
        <View className="fn-wrap-service-wifi-card">
          <View className="fn-wifi-content">链接到 SSID：CAPITALAND FREE WIFI 即可免费享用无线网络</View>
        </View>
        <View className="fn-wrap-service-divider" />
        <View className="fn-wrap-service-customer-service">
          <Image className="customer-service-icon" src={images.CSS} />
          客户服务
        </View>
        <View className="fn-wrap-service-cs-card">
          <View className="fn-cs-title">客户服务柜台</View>
          <View className="fn-cs-details">• 服务时间: 11am - 9pm daily</View>
        </View>
        <View className="fn-wrap-service-divider" />

        <View className="fn-wrap-service-customer-service">
          <Image className="nursing-room-icon" src={images.NR} />
          护理室
        </View>
        <View className="fn-wrap-service-nursing-room">
          <View className="fn-nr-content">• 护理室位于地下2层以及4层 靠近女士洗手间</View>
        </View>

        <View className="fn-wrap-service-customer-service">
          <Image className="bike-icon" src={images.BIKE} />
          1层的自行车道使用指南
        </View>
        <View className="fn-wrap-service-bike-card">
          <View className="bike-card-detail">
            <Text style={{ fontWeight: 500 }}>室内自行车道</Text>
            {'\n'}
            1. 室内自行车道是单向行驶的。入口仅可从Funan的North Bridge Road入口进入{'\n'}
            2. 仅允许从上午7点到上午10点在室内自行车道上骑自行车和骑自行车。时速限制为10 km / h{'\n'}
            3. 在上午7点至上午10点以外，骑自行车的人和骑手必须卸下自行车和设备才能行走。自行车和设备只能在一楼行走{'\n'}
            4. 除FL3电梯外，任何自动扶梯或乘客电梯均禁止使用自行车和设备{'\n'}
            5. 沿路禁止停车。未停在指定地段的无人看管的自行车将被移除{'\n'}
            <Text style={{ fontWeight: 500 }}>户外自行车道</Text>
            {'\n'}
            1. 户外自行车道是双向的，可24/7全天候骑行{'\n'}
            2. 沿路禁止停车，未停在指定地段的无人看管的自行车将被移除
          </View>
          <View className="bike-card-tips">
            * 根据 Singapore's Active Mobility Act，所有骑自行车者和骑手都必须遵守LTA的 Rules and Code of Conduct for
            cyclists，以及个人出行设备（PMD），电动自行车（PAB）和个人出行辅助设备（PMA）的骑手，以及
            上述规则，作为在商场内使用此类自行车和设备的条件。
            对于在商场中使用此类自行车和设备而直接或间接引起的任何事件，伤害，损坏或损失，Funan概不负责。
          </View>
        </View>
      </View>
    )}
    {section == 'parking' && (
      <View className="fn-wrap-parking">
        <View className="fn-wrap-parking-fee">
          <Image className="parking-fee-icon" src={images.PARKING} />
          停车收费
        </View>
        <View className="fn-wrap-parking-car-card">
          <Image className="car-card-title" src={images.CAR} />
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周四</Text>
            {'\n'}
            8:00am - 5:00pm: 第一小时 S$2.00{'\n'}
            随后每10分钟 S$0.4{'\n'}
            5:00pm之后: 每次入场 S$3.00
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周五</Text>
            {'\n'}
            8:00am - 5:00pm: 首2小时 S$2.00{'\n'}
            随后每10分钟 S$0.4
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周五 5:00pm后，周六，周日及公共假日</Text>
            {'\n'}
            全天：首2小时 S$3.00{'\n'}
            随后每10分钟 S$0.3
          </View>
          <View className="car-card-panel">
            *上述价格包括7％的消费税{'\n'}
            *宽限期(Grace period)：10分钟
          </View>
        </View>

        <View className="fn-wrap-parking-motor-card">
          <Image className="motor-card-title" src={images.MOTOR} />
          <View className="motor-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周日及公众假期（主要停车场和卸货区）</Text>
            {'\n'}
            每次入场 S$1.07
          </View>
          <View className="motor-card-panel">
            *上述价格包括7％的消费税{'\n'}
            *无宽限期 (No grace period)
          </View>
        </View>
      </View>
    )}

    {children}
  </View>
);
