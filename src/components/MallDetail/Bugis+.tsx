import React from 'react';
import './Bugis+.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { global } from '../../../global.data';

type SelfProps = {
  section: string;
};
export const BugisPlus: React.FC<SelfProps> = ({ section, children }) => (
  <View className="bp-wrap">
    {section == 'summary' && (
      <View className="bp-wrap-summary">
        <Image className="bp-wrap-summary-image" src={global.url + 'bugis+summary.png'} />
        <View className="bp-wrap-summary-introduction">
          Bugis+ 白沙浮娱乐广场位于新加坡的文化心脏地带，二层与Bugis Junction 白沙浮广场相连接，大家可以很轻松的前往武吉士地铁站。{'\n'}
          Bugis
          +白沙浮娱乐广场是一个充满活力的购物中心，提供无尽的娱乐设施设备，令人神魂颠倒的美食和时尚品牌，为武吉士周边的年轻人们提供了最“动感”的地带！
        </View>
      </View>
    )}
    {section == 'route' && (
      <View className="bp-wrap-route">
        <View className="bp-wrap-route-mrt">
          <Image className="mrt-icon" src={images.MRT} />
          搭乘地铁
        </View>
        <View className="bp-wrap-route-mrt-card">
          <View className="bp-mrt-title">武吉士地铁站（Bugis MRT station）</View>
          <View className="bp-mrt-ew">东西线EW12</View>
          <View className="bp-mrt-dt">滨海市区线DT14</View>
        </View>
        <View className="bp-wrap-route-divider" />
        <View className="bp-wrap-route-bus">
          <Image className="bus-icon" src={images.BUS} />
          搭乘巴士公交
        </View>

        <View className="bp-wrap-route-bus-card">
          <View className="bp-bus-no">
            SBS : 2, 12, 32, 51, 63, 80, 124, 142, 145, 166, 174, 174E, 197{'\n'}
            SMRT : 61, 851, 960,980, NR7
          </View>
        </View>
      </View>
    )}
    {section == 'service' && (
      <View className="bp-wrap-service">
        <View className="bp-wrap-service-wifi">
          <Image className="wifi-icon" src={images.WIFI} />
          免费网络
        </View>
        <View className="bp-wrap-service-wifi-card">
          <View className="bp-wifi-content">
            链接到 SSID：CAPITALAND FREE WIFI 即可免费享用无线网络{'\n'}
            Wi-Fi密码: freewifi (区分大小写).
          </View>
        </View>
        <View className="bp-wrap-service-divider" />
        <View className="bp-wrap-service-customer-service">
          <Image className="customer-service-icon" src={images.CSS} />
          客户服务
        </View>
        <View className="bp-wrap-service-cs-card">
          <View className="bp-cs-details">
            • 服务柜台：位于Bugis+ 1层 优衣库(Uniqlo)对面 将暂时关闭，直至另行通知。最近的客户服务柜台位于白沙浮(Bugis Junction) 2楼
            (靠近屈臣氏)。访客可透过天桥由Bugis+前往，每日上午11时至晚上9时开放。{'\n'}•
            如需更多查询或报告任何丢失的物品，您可以写信到retail@capitaland.com或在每天上午10点至下午6点之间致电我们的客户服务热线65 6631
            9931。 对于发现的物品，请致电我们的消防指挥中心，电话为65 6634 6824（24小时）。
          </View>
        </View>

        <View className="bp-wrap-service-divider" />

        <View className="bp-wrap-service-customer-service">
          <Image className="family-service-icon" src={images.FS} />
          家庭服务
        </View>
        <View className="bp-wrap-service-family-card">
          <View className="family-service-title">护理室</View>
          <View className="family-service-detail">1层至4层</View>
        </View>
      </View>
    )}
    {section == 'parking' && (
      <View className="bp-wrap-parking">
        <View className="bp-wrap-parking-fee">
          <Image className="parking-fee-icon" src={images.PARKING} />
          停车收费
        </View>
        <View className="bp-wrap-parking-car-card">
          <Image className="car-card-title" src={images.CAR} />
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周一至周四</Text>
            {'\n'}
            8:00am - 5:59pm: 第一小时 S$1.28{'\n'}
            随后每10分钟 S$0.54{'\n'}
            6:00pm - 7:59am(第二天): 每次入场 S$3.21
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周五</Text>
            {'\n'}
            8:00am - 5:59pm: 第一小时 S$1.28{'\n'}
            6:00pm - 7:59am(第二天): 首2小时 S$3.21 随后的每15分钟 S$0.54
          </View>
          <View className="car-card-panel">
            <Text style={{ fontWeight: 500 }}>周六，周日及公共假日</Text>
            {'\n'}
            首2小时 S$3.21 随后的每15分钟 S$0.54
          </View>
          <View className="car-card-panel">
            *上述价格包括7％的消费税{'\n'}
            *宽限期(Grace period)：10分钟
          </View>
        </View>
      </View>
    )}

    {children}
  </View>
);
