import React from 'react';
import './BrandCard.scss';
import { View, Image } from '@tarojs/components';
import { ITouchEvent } from '@tarojs/components/types/common';
import * as images from '../../static/images';

type SelfProps = {
  margin?: string;
  logo: string;
  title: string;
  desc: string;
  promotion?: boolean;
  dgc?: string[];
  payment?: string[];
  category: string;
  mall: string;
  unit: string;
  onClick?: (event: ITouchEvent) => void;
};

export interface DGC {
  name: string;
  style: string;
}

export interface PAY {
  name: string;
  style: string;
  logoStyle?: string;
}

function renderDGC(dgc: string[]) {
  const dgcCopy: string[] = [];
  const firstHalf: DGC[] = [];
  const secondHalf: DGC[] = [];
  if (dgc.indexOf('CapitaVoucher') >= 0) {
    dgcCopy.unshift('CapitaVoucher');
    firstHalf.push({ name: images.CV, style: 'bc-dgc-cv' });
  }
  if (dgc.indexOf('eCapitaVoucher') >= 0) {
    dgcCopy.unshift('eCapitaVoucher');
    firstHalf.push({ name: images.ECV, style: 'bc-dgc-ecv' });
  }
  if (dgc.indexOf('CapitaCard') >= 0) {
    dgcCopy.unshift('CapitaCard');
    firstHalf.push({ name: images.CC, style: 'bc-dgc-cc' });
  }
  if (dgc.indexOf('STARXtra') >= 0) {
    dgcCopy.unshift('STARXtra');
    firstHalf.push({ name: images.CCS, style: 'bc-dgc-sx' });
  }
  const dgcTemp = ['CapitaVoucher', 'eCapitaVoucher', 'CapitaCard', 'STARXtra'];
  const lostDgc = dgcTemp.filter((el) => !dgcCopy.includes(el));
  lostDgc.map((el) => {
    let style, name;
    if (el.startsWith('CapitaV')) {
      name = images.CV;
      style = 'bc-dgc-cv hidden';
    } else if (el.startsWith('eCapitaV')) {
      name = images.ECV;
      style = 'bc-dgc-ecv hidden';
    } else if (el.startsWith('CapitaC')) {
      name = images.CC;
      style = 'bc-dgc-cc hidden';
    } else if (el.startsWith('STAR')) {
      name = images.CCS;
      style = 'bc-dgc-sx hidden';
    }
    secondHalf.push({ name: name, style: style });
  });
  const final = firstHalf.concat(secondHalf);

  return final.map((item) => {
    return <Image key={item.name} className={item.style} src={item.name} />;
  });
}

function renderPay(payment: string[]) {
  const paymentCopy = [...payment];
  const finalPay: PAY[] = [];
  if (paymentCopy.indexOf('WeChatPay') >= 0) {
    finalPay.push({ name: images.WP, style: 'bc-payment-wechatpay' });
  }
  if (paymentCopy.indexOf('Alipay') >= 0) {
    finalPay.push({ name: images.AP, style: 'bc-payment-alipay' });
  }
  if (paymentCopy.indexOf('GrabPay') >= 0) {
    finalPay.push({ name: images.GP, style: 'bc-payment-grabpay' });
  }

  return finalPay.map((item) => {
    return <Image key={item.name} className={item.style} src={item.name} />;
  });
}

export const BrandCard: React.FC<SelfProps> = ({
  onClick,
  margin,
  logo,
  title,
  promotion,
  desc,
  dgc,
  payment,
  category,
  mall,
  unit,
  children
}) => (
  <View className="bc-wrap" onClick={onClick} style={{ margin: margin }}>
    {desc ? (
      <View className="bc-above">
        <View className="bc-above-logo-wrap">
          <Image src={logo} className="bc-above-logo" />
        </View>
        <View className="bc-above-title">{title}</View>
        {promotion && <Image src={images.DB} className="bc-above-promotions" />}

        <View className="bc-above-desc-wrap">
          <View className="bc-desc">{desc}</View>
        </View>
      </View>
    ) : (
      <View className="bc-above-no-desc">
        <View className="bc-above-no-desc-logo-wrap">
          <Image src={logo} className="bc-above-no-desc-logo" />
        </View>
        <View className="bc-above-no-desc-title">{title}</View>
      </View>
    )}

    <View className="bc-divider" />

    {dgc?.[0] && (
      <View className="bc-dgc">
        <View className="bc-dgc-icon" />
        <View className="bc-dgc-content">{renderDGC(dgc)}</View>
      </View>
    )}

    {payment?.[0] && (
      <View className="bc-payment">
        {/*3 Simei Street 6↵#02-K3 Eastpoint Mall↵Singapore 528833*/}
        <View className="bc-payment-icon" />
        <View className="bc-payment-content">
          {renderPay(payment)}
          {/*...*/}
        </View>
      </View>
    )}

    <View className="bc-category">
      <View className="bc-category-icon" />
      <View className="bc-category-content">{category}</View>
    </View>

    <View className="bc-location">
      <View className="bc-location-icon" />
      <View className="bc-location-content">
        <View className="mall-locale">{mall}</View>
        <View className="unit-locale">
          <View className="bc-unit-icon" />
          <View className="unit-number">{unit}</View>
        </View>
      </View>
    </View>
    {children}
  </View>
);
