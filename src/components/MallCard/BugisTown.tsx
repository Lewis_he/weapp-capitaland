import React, { useState } from 'react';
import Taro from '@tarojs/taro';
import './BugisTown.scss';
import { View, Image } from '@tarojs/components';
import { ITouchEvent } from '@tarojs/components/types/common';
import { global } from '../../../global.data';
import { MallCardS } from './MallCradS';

type SelfProps = {
  margin: string;
  logo: string;
  distance: string;
  desc: string;
  siblings: any[];
  onClick?: (event: ITouchEvent) => void;
};
export const BugisTownCard: React.FC<SelfProps> = ({ margin, logo, distance, desc, siblings, children }) => {
  const [expand, setExpand] = useState(false);
  const hiddenStyle = { maxHeight: '0', opacity: 0 };
  const visibleStyle = { maxHeight: '600px', opacity: 1 };

  function clickHandler(e, id) {
    e.stopPropagation();
    Taro.navigateTo({ url: `/subpages/mall/index?id=${id}` });
  }
  function collapseHandler(e) {
    e.stopPropagation();
    setExpand(false);
  }

  return (
    <View className="btc-wrap" style={{ margin }} onClick={() => setExpand(true)}>
      <View className="btc-header">
        <Image className="btc-logo" src={logo} mode="aspectFill" />
        <View className="btc-title">武吉士镇{'\n'}Bugis Town</View>
        <View className="btc-distance">{distance}</View>
      </View>
      <View className="btc-desc">{desc}</View>
      <View className="btc-divider" />
      {!expand && (
        <View className="btc-logos-wrap">
          {siblings?.map((item) => (
            <Image className="btc-logos-wrap-siblings" src={global.live + item.logo.id} />
          ))}
        </View>
      )}
      <View className="btc-malls" style={expand ? visibleStyle : hiddenStyle}>
        {siblings?.map((mall) => {
          const title = (mall.titleCn ? mall.titleCn + ' ' : '') + mall.title;
          const logoBG = global.live + mall.logo?.id;
          const bg = global.live + mall.thumbnail?.id;
          return (
            <MallCardS
              margin="0 auto 20px"
              bg={bg}
              logo={logoBG}
              title={title}
              distance={distance}
              onClick={(event) => clickHandler(event, mall.id)}
            />
          );
        })}
        <View className="btc-malls-collapse" onClick={collapseHandler}>
          <View className="collapse-txt">点击收起</View>
          <View className="collapse-wrap">
            <View className="collapse-icon" />
          </View>
        </View>
      </View>

      {children}
    </View>
  );
};
