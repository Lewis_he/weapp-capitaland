import React, { useState } from 'react';
import Taro from '@tarojs/taro';
import './MallCardXL.scss';
import { View, Image, Text } from '@tarojs/components';
import * as images from '../../static/images/index';
import { checkMallOpen } from '../../utils/util';
// import { ITouchEvent } from '@tarojs/components/types/common';

type SelfProps = {
  id?: number;
  la: number;
  lo: number;
  logo: string;
  address: string;
  title: string;
  onChangeSection: (section: string) => void;
  // onClick?: (event: ITouchEvent) => void;
};

export const MallCardXL: React.FC<SelfProps> = ({ onChangeSection, logo, address, title, la, lo, id, children }) => {
  const [tab, setTab] = useState('search');
  const open = id == 9 || checkMallOpen();

  function openWxLocation() {
    // mock data: {longitude: 103.8789, latitude: 1.3285}
    // const latitude = 1.29400;
    // const longitude = 103.85343;
    Taro.openLocation({
      latitude: la,
      longitude: lo,
      scale: 12,
      name: title,
      address: address
    });
  }

  return (
    <View className="mcxl-wrap">
      <Image className="mcxl-logo" src={logo} />
      <View className="mcxl-title">{title}</View>
      <View className="mcxl-address" onClick={openWxLocation}>
        <Image className="mcxl-address-location-icon" src={images.ML} />
        {address}
        <Image className="mcxl-address-map-icon" src={images.MR} />
      </View>
      <View className="mcxl-divider" />
      <View className="mcxl-tags">
        <View
          className="mcxl-tags-search"
          onClick={() => {
            setTab('search');
            onChangeSection('search');
          }}
        >
          <Image className="tag-icon" src={tab == 'search' ? images.MSA : images.MS} />
          <Text className={tab == 'search' ? 'tag-search-txt section-active' : 'tag-search-txt'}>商户检索</Text>
        </View>
        <View
          className="mcxl-tags-search"
          onClick={() => {
            setTab('summary');
            onChangeSection('summary');
          }}
        >
          <Image className="tag-icon" src={tab == 'summary' ? images.MIA : images.MI} />
          <Text className={tab == 'summary' ? 'tag-search-txt section-active' : 'tag-search-txt'}>商场介绍</Text>
        </View>
        <View
          className="mcxl-tags-search"
          onClick={() => {
            setTab('route');
            onChangeSection('route');
          }}
        >
          <Image className="tag-icon" src={tab == 'route' ? images.TRA : images.TR} />
          <Text className={tab == 'route' ? 'tag-search-txt section-active' : 'tag-search-txt'}>交通路线</Text>
        </View>
        <View
          className="mcxl-tags-es"
          onClick={() => {
            setTab('service');
            onChangeSection('service');
          }}
        >
          <Image className="tag-icon" src={tab == 'service' ? images.ESA : images.ES} />
          <Text className={tab == 'service' ? 'tag-es-txt section-active' : 'tag-es-txt'}>设施/服务</Text>
        </View>
        <View
          className="mcxl-tags-search"
          onClick={() => {
            setTab('parking');
            onChangeSection('parking');
          }}
        >
          <Image className="tag-icon" src={tab == 'parking' ? images.PFA : images.PF} />
          <Text className={tab == 'parking' ? 'tag-search-txt section-active' : 'tag-search-txt'}>停车收费</Text>
        </View>
      </View>
      <View className="mcxl-divider light" />
      <View className="mcxl-operating">
        <Image className="mcxl-operating-icon" src={images.MO} />
        <Text>{id == 9 ? '24 hours' : '10:00 AM - 10:00 PM'}</Text>

        <Text style={open ? { color: '#7CCF86', marginLeft: 'auto' } : { color: 'red', marginLeft: 'auto' }}>
          {open ? 'Opening Now' : 'Closed'}
        </Text>
      </View>
      {children}
    </View>
  );
};
