import React from 'react';
import './MallCardM.scss';
import { View, Image } from '@tarojs/components';
import { ITouchEvent } from '@tarojs/components/types/common';

type SelfProps = {
  margin: string;
  bg: string;
  logo: string;
  title: string;
  distance?: string;
  onClick?: (event: ITouchEvent) => void;
};
export const MallCardM: React.FC<SelfProps> = ({ onClick, margin, bg, logo, title, distance, children }) => (
  <View className="mcm-wrap" style={{ margin: margin }} onClick={onClick}>
    <Image className="mcm-wrap-bg" src={bg} mode="aspectFill" />
    <Image className="mcm-wrap-logo" src={logo} />
    {distance && <View className="mcm-wrap-distance">{distance}</View>}
    <View className="mcm-wrap-title">{title}</View>
    {children}
  </View>
);
