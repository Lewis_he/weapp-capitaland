import React from 'react';
import './MallCardS.scss';
import { View, Image } from '@tarojs/components';
import { ITouchEvent } from '@tarojs/components/types/common';

type SelfProps = {
  margin: string;
  bg: string;
  logo: string;
  title: string;
  distance: string;
  onClick?: (event: ITouchEvent) => void;
};
export const MallCardS: React.FC<SelfProps> = ({ onClick, margin, bg, logo, title, distance, children }) => (
  <View className="mcs-wrap" style={{ margin: margin }} onClick={onClick}>
    <Image className="mcs-wrap-bg" src={bg} mode="aspectFill" />
    <Image className="mcs-wrap-logo" src={logo} />
    <View className="mcs-wrap-distance">{distance}</View>
    <View className="mcs-wrap-title">{title}</View>
    {children}
  </View>
);
