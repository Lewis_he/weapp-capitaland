import React from 'react';
// import Taro from '@tarojs/taro';
import './MallCardL.scss';
import { View, Image } from '@tarojs/components';
import { ITouchEvent } from '@tarojs/components/types/common';
// import * as images from '../../static/images/index';

type SelfProps = {
  margin: string;
  bg: string;
  logo: string;
  distance: string;
  title: string;
  desc: string;
  onClick?: (event: ITouchEvent) => void;
};
export const MallCardL: React.FC<SelfProps> = ({ onClick, margin, bg, logo, distance, title, desc, children }) => {
  return (
    <View className="mcl-wrap" style={{ margin: margin }} onClick={onClick}>
      <Image className="mcl-above" src={bg} mode="aspectFill" lazyLoad />
      {/*<View className="mcl-above">*/}
      {/*  <van-image*/}
      {/*    width="100%"*/}
      {/*    height={Taro.pxTransform(266)}*/}
      {/*    fit="cover"*/}
      {/*    radius="13px 13px 0 0"*/}
      {/*    lazyLoad={true}*/}
      {/*    src={bg}*/}
      {/*    useLoadingSlot={true}*/}
      {/*  >*/}
      {/*    <van-image slot="loading" width="157" height="71" src={images.CAPLL} fit="cover" />*/}
      {/*  </van-image>*/}
      {/*</View>*/}
      <Image className="mcl-logo" src={logo} />
      <View className="mcl-distance">{distance}</View>
      <View className="mcl-title">{title}</View>
      <View className="mcl-divider" />
      <View className="mcl-desc">{desc}</View>
      {children}
    </View>
  );
};
