import React from 'react';
import './MallCardLLoading.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';

type SelfProps = {
  margin: string;
};
export const MallCardLLoading: React.FC<SelfProps> = ({ margin, children }) => (
  <View className="mcll-wrap" style={{ margin: margin }}>
    {/*<Image className="sketch-logo" src={images.CLV} />*/}
    {/*<View className="sketch xl" />*/}
    {/*<View className="sketch l" />*/}
    <View className="mcll-above">
      <Image className="capt-l" src={images.CAPLL} />
    </View>
    <Image className="mcll-logo" src={images.CGWL} />
    <View className="mcll-below">
      <View className="mcll-below-sketch l" />
      <View className="mcll-below-divider" />
      <View className="mcll-below-sketch xl" />
      <View className="mcll-below-sketch s" />
    </View>
    {children}
  </View>
);
