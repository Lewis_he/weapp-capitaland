import React from 'react';
import './MallCardMLoading.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';

type SelfProps = {
  margin: string;
};
export const MallCardMLoading: React.FC<SelfProps> = ({ margin, children }) => (
  <View className="mcml-wrap" style={{ margin: margin }}>
    <View className="mcml-above">
      <Image className="capt-m" src={images.CAPLM} />
    </View>
    <Image className="mcml-logo" src={images.CGWL} />
    <View className="mcml-below">
      <View className="mcml-below-sketch ll" />
    </View>
    {children}
  </View>
);
