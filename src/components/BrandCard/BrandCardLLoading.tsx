import React from 'react';
import './BrandCardLLoading.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';

type SelfProps = {
  margin: string;
};
export const BrandCardLLoading: React.FC<SelfProps> = ({ margin, children }) => (
  <View className="bcll-wrap" style={{ margin: margin }}>
    <View className="bcll-logo-wrap">
      <Image className="bcll-logo" src={images.CGWL} />
    </View>
    <View className="bcll-divider" />
    <View className="bcll-ss" />
    <View className="bcll-sm" />
    <View className="bcll-sxl" />
    <View className="bcll-sl" />
    <View className="bcll-sls" />
    {children}
  </View>
);
