import React from 'react';
import './BrandCardL.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';
import { DGC, PAY } from '../ResponsiveCard/BrandCard';
import { ITouchEvent } from '@tarojs/components/types/common';

type SelfProps = {
  margin?: string;
  logo: string;
  title: string;
  dgc?: string[];
  payment?: string[];
  category: string;
  mall: string;
  unit: string;
  onClick?: (event: ITouchEvent) => void;
};

function renderDGC(dgc: string[]) {
  const firstHalf: DGC[] = [];
  if (dgc.indexOf('CapitaVoucher') >= 0) {
    firstHalf.push({ name: images.CV, style: 'bcl-dgc-cv' });
  }
  if (dgc.indexOf('eCapitaVoucher') >= 0) {
    firstHalf.push({ name: images.ECV, style: 'bcl-dgc-ecv' });
  }
  if (dgc.indexOf('CapitaCard') >= 0) {
    firstHalf.push({ name: images.CC, style: 'bcl-dgc-cc' });
  }
  if (dgc.indexOf('STARXtra') >= 0) {
    firstHalf.push({ name: images.CCS, style: 'bcl-dgc-sx' });
  }

  return firstHalf.map((item) => {
    return <Image key={item.name} className={item.style} src={item.name} />;
  });
}

function renderPay(payment: string[]) {
  const paymentCopy = [...payment];
  const finalPay: PAY[] = [];
  if (paymentCopy.indexOf('WeChatPay') >= 0) {
    finalPay.push({ name: images.WP, style: 'bcl-payment-wechatpay' });
  }
  if (paymentCopy.indexOf('Alipay') >= 0) {
    finalPay.push({ name: images.AP, style: 'bcl-payment-alipay' });
  }
  if (paymentCopy.indexOf('GrabPay') >= 0) {
    finalPay.push({ name: images.GP, style: 'bcl-payment-grabpay' });
  }

  return finalPay.map((item) => {
    return <Image key={item.name} className={item.style} src={item.name} />;
  });
}

export const BrandCardL: React.FC<SelfProps> = ({ onClick, margin, logo, title, dgc, payment, category, mall, unit, children }) => (
  <View className="bcl-wrap" style={{ margin: margin }} onClick={onClick}>
    <View className="bcl-above">
      <View className="bcl-above-logo-wrap">
        <Image className="bcl-above-logo" src={logo} />
      </View>
      <View className="bcl-above-title">{title}</View>
    </View>

    <View className="bcl-divider" />
    {dgc?.[0] && (
      <View className="bcl-dgc">
        <View className="bcl-dgc-icon" />
        <View className="bcl-dgc-content">{renderDGC(dgc)}</View>
      </View>
    )}
    {payment?.[0] && (
      <View className="bcl-payment">
        <View className="bcl-payment-icon" />
        <View className="bcl-payment-content">{renderPay(payment)}</View>
      </View>
    )}
    <View className="bcl-category">
      <View className="bcl-category-icon" />
      <View className="bcl-category-content">{category}</View>
    </View>
    <View className="bcl-location">
      <View className="bcl-location-icon" />
      <View className="bcl-location-mall">{mall}</View>
      <View className="bcl-location-unit-icon" />
      <View className="bcl-location-unit-number">{unit}</View>
    </View>
    {children}
  </View>
);
