import React from 'react';
import './BrandCardXS.scss';
import { View, Image } from '@tarojs/components';
import { ITouchEvent } from '@tarojs/components/types/common';

type SelfProps = {
  margin?: string;
  logo: string;
  title: string;
  category: string;
  onClick?: (event: ITouchEvent) => void;
};

export const BrandCardXS: React.FC<SelfProps> = ({ onClick, margin, logo, title, category, children }) => (
  <View className="bcxs-wrap" style={{ margin: margin }} onClick={onClick}>
    <View className="bcxs-logo-wrap">
      <Image className="bcxs-logo" src={logo} />
    </View>

    <View className="bcxs-title">{title}</View>

    <View className="bcxs-divider" />

    <View className="bcxs-tag" />

    <View className="bcxs-category">{category}</View>
    {children}
  </View>
);
