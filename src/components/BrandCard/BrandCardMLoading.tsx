import React from 'react';
import './BrandCardMLoading.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';

type SelfProps = {
  margin: string;
};
export const BrandCardMLoading: React.FC<SelfProps> = ({ margin, children }) => (
  <View className="bcml-wrap" style={{ margin: margin }}>
    <View className="bcml-logo-wrap">
      <Image className="bcml-logo" src={images.CGWL} />
    </View>
    <View className="bcml-divider" />
    <View className="bcml-ss" />
    <View className="bcml-sm" />
    <View className="bcml-sxl" />
    <View className="bcml-sl" />
    {children}
  </View>
);
