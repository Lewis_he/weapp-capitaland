import React from 'react';
import './SwiperCardLoading.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';

type SelfProps = {
  scale: number;
};
export const SwiperCardLoading: React.FC<SelfProps> = ({ scale, children }) => (
  <View className="spl-wrap" style={{ transform: `scale(${scale})` }}>
    <Image className="sketch-logo" src={images.CLV} />
    <View className="sketch sxl" />
    <View className="sketch sl" />
    {children}
  </View>
);
