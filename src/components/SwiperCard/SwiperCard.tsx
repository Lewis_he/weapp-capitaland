import React from 'react';
import './SwiperCard.scss';
import { Image } from '@tarojs/components';

type SelfProps = {
  scale: number;
  image: string;
};
export const SwiperCard: React.FC<SelfProps> = ({ image, children, scale }) => (
  <Image className="sp-wrap" style={{ transform: `scale(${scale})` }} src={image} mode="aspectFill" lazyLoad>
    {children}
  </Image>
);
