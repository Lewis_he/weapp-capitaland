import React from 'react';
import './EndLine.scss';
import { View } from '@tarojs/components';

type SelfProps = {
  margin: string;
};

export const EndLine: React.FC<SelfProps> = ({ margin, children }) => (
  <View className="el-wrap" style={{ margin: margin }}>
    <View className="el-wrap-line" />
    <View className="el-wrap-circle" />
    <View className="el-wrap-line" />
    {children}
  </View>
);
