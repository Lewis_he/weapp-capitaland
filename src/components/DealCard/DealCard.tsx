import React from 'react';
import './DealCard.scss';
import { Image, View } from '@tarojs/components';

type SelfProps = {
  margin?: string;
  title: string;
  desc: string;
  image: string;
};
export const DealCard: React.FC<SelfProps> = ({ title, desc, image, children, margin }) => (
  <View className="dc-wrap" style={{ margin: margin }}>
    <Image src={image} className="dc-wrap-image" />
    <View className="dc-wrap-title">{title}</View>
    <View className="dc-wrap-desc">{desc}</View>
    <View className="dc-wrap-tag">免费领取</View>
    {children}
  </View>
);
