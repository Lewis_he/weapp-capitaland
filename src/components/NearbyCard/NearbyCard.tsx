import React from 'react';
import './NearbyCard.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';
import { ITouchEvent } from '@tarojs/components/types/common';

type SelfProps = {
  margin: string;
  bg: string;
  distance: string;
  title: string;
  desc: string;
  onClick?: (event: ITouchEvent) => void;
};
export const NearbyCard: React.FC<SelfProps> = ({ onClick, margin, bg, distance, title, desc, children }) => (
  <View className="nbc-wrap" style={{ margin: margin }} onClick={onClick}>
    <Image className="nbc-above" src={bg} mode="aspectFill">
      <View className="nbc-above-distance">{distance}</View>
    </Image>

    <View className="nbc-below">
      <View className="nbc-below-highlight" />
      <View className="nbc-below-title">{title}</View>
      <View className="nbc-below-divider" />
      <View className="nbc-below-desc">{desc}</View>
      <Image className="nbc-below-map-icon" src={images.MAP} />
    </View>
    {children}
  </View>
);
