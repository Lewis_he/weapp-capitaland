import React from 'react';
import './NearbyCardLoading.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';

type SelfProps = {
  margin: string;
};
export const NearbyCardLoading: React.FC<SelfProps> = ({ margin, children }) => (
  <View className="nbcl-wrap" style={{ margin: margin }}>
    <Image className="nbcl-above" src={images.BATIK} />
    <View className="nbcl-below">
      <View className="nbcl-below-divider" />
      <View className="nbcl-below-ss" />
      <View className="nbcl-below-sm" />
      <View className="nbcl-below-sl" />
      <Image className="nbcl-below-map" src={images.MAP} />
    </View>
    {children}
  </View>
);
