import React from 'react';
import './EventCardLoading.scss';
import { View, Image } from '@tarojs/components';
import * as images from '../../static/images/index';

type SelfProps = {
  margin: string;
};
export const EventCardLoading: React.FC<SelfProps> = ({ margin, children }) => (
  <View className="ecl-wrap" style={{ margin: margin }}>
    <View className="ecl-left">
      <Image className="ecl-left-logo" src={images.CAPLS} />
    </View>
    <View className="ecl-right-divider" />
    <View className="ecl-right-sketch a" />
    <View className="ecl-right-sketch b" />
    <View className="ecl-right-sketch c" />
    {children}
  </View>
);
