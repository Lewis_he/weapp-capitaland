import React from 'react';
import './EventCard.scss';
import { View, Image } from '@tarojs/components';
import { ITouchEvent } from '@tarojs/components/types/common';

type SelfProps = {
  margin: string;
  logo: string;
  title: string;
  date: string;
  location: string;
  onClick?: (event: ITouchEvent) => void;
};
export const EventCard: React.FC<SelfProps> = ({ onClick, margin, logo, title, date, location, children }) => (
  <View className="ec-wrap" style={{ margin: margin }} onClick={onClick}>
    <Image className="ec-left" src={logo} />
    <View className="ec-right-divider" />
    <View className="ec-title">{title}</View>

    <View className="ec-valid-period">
      <View className="ec-time-icon" />
      {date}
    </View>

    <View className="ec-location">
      <View className="ec-locale-icon" />
      {location}
    </View>
    {children}
  </View>
);
