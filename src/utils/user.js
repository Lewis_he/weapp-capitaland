import Taro from '@tarojs/taro';
import {authorizeUser, loginByWeChat} from '../service/auth';
import request from "./fetch";
import api from "../config/index";


/**
 * Promise封装wx.checkSession
 */
function checkSession() {
  return new Promise(function (resolve, reject) {
    Taro.checkSession({
      success: function () {
        console.log('wx check session 0...');
        resolve(true);
      },
      fail: function () {
        console.log('wx check session -1...');
        reject();
      }
    })
  });
}

/**
 * Promise封装wx.login
 */
function login() {
  return new Promise(function (resolve, reject) {
    Taro.login({
      success: function (res) {
        if (res.code) {
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function (err) {
        reject(err);
      }
    });
  });
}

/**
 * 调用微信登录
 */
export function loginByWeixin(userInfo) {
  return login().then((res) => {
    return loginByWeChat({
      jsCode: res.code
    }).then(loginRes => {
      //存储用户信息
      Taro.setStorageSync('userInfo', userInfo);
      // call getMe
      getMe().then(serverUser => {
        Taro.setStorageSync('serverUserInfo', serverUser);
        if (serverUser.displayName !== userInfo.nickName
            || serverUser.avatarUrl !== userInfo.avatarUrl) {
          const updateUserInfoData = {
            displayName: userInfo.nickName,
            avatarUrl: userInfo.avatarUrl
          };
          return request(api.AuthUserInfoUpdate, updateUserInfoData, "POST");
        } else {
          return serverUser;
        }
      });
    })
  });
}

/**
 * 判断用户是否登录
 */
export function checkLogin() {
  return new Promise(function (resolve, reject) {
    if (Taro.getStorageSync('userInfo')) {
      console.log('ss user info 0...');
      checkSession().then(() => {
        resolve(true);
      }).catch(() => {
        // Taro.removeStorageSync('userInfo');
        Taro.removeStorageSync('Cookies');
        reject(false);
      });
    } else {
      console.log('ss user info -1...', Taro.getStorageSync('userInfo'));
      // Taro.removeStorageSync('userInfo');
      Taro.removeStorageSync('Cookies');
      reject(false);
    }
  });
}

/**
 * call GetMe
 * */
export function getMe() {
  return new Promise(function (resolve, reject) {
    authorizeUser().then(res => {
      resolve(res);
    }).catch((err) => {
      reject(err);
    })
  })
}

let serverLoginPromise;
async function serverLogin() {
  if (!serverLoginPromise) {
    serverLoginPromise = login().then((wxLoginResp) => {
      return loginByWeChat({
        jsCode: wxLoginResp.code
      }).then(logined => {
        return getMe().then(me => {
          Taro.setStorageSync('serverUserInfo', me);
        });
      })
    });
  }
  return serverLoginPromise;
}

Object.defineProperty(String.prototype, 'hashCode', {
  value: function() {
    var hash = 0, i, chr;
    for (i = 0; i < this.length; i++) {
      chr   = this.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }
});

var cacheMap = {};

/** **/
function isUnauthorized(err) {
  return err.statusCode === 401;
}

async function serverRequest(url, data, method, {cache = true, evict = false} = {}) {
  const cacheKey = [url, method, JSON.stringify(data)].join(" | ");
  if (evict) {
    cacheMap = {};
  }
  if (!cache || !cacheMap[cacheKey]) {
    const promise = requestWithRetry();
    cacheMap[cacheKey] = promise;
    return promise;
  } else {
    return await cacheMap[cacheKey];
  }

  async function requestWithRetry() {
    return requestWithServerLogin().catch(error => {
      if (isUnauthorized(error)) {
        serverLoginPromise = null;
        return requestWithServerLogin();
      } else {
        return Promise.reject(error);
      }
    });
  }

  async function requestWithServerLogin() {
    const start = Date.now();
    return serverLogin()
      .then(logined => {
        return request(url, data, method);
      }).then(resp => {
        const timeElapsed = Date.now() - start;
        if (timeElapsed > 2000) {
        }
        return resp;
      });
  }
}

export const server = {
  get: (url, data, options) => serverRequest(url, data, 'GET', options),
  post: (url, data, options) => serverRequest(url, data, 'POST', options)
};
