import Taro from '@tarojs/taro';

/**
 * 封装微信的的request
 */
function request(url, data = {}, method = "GET", type = "application/json") {

  let cookie = Taro.getStorageSync('Cookies');
  let header = { 'Content-Type': type };
  if (cookie) {
    header.Cookie = cookie;
  }
  return new Promise(function(resolve, reject) {
    Taro.request({
      url: url,
      data: data,
      method: method,
      header: header,
      success: function(res) {
        // console.log(res);
        if (res && res.header && res.header['Set-Cookie']) {
          let cookies = res.header['Set-Cookie'].replace(/,/g, ';');
          Taro.setStorageSync('Cookies', cookies);
        }

        if (res.statusCode === 200) {
          resolve(res.data)
        } else if(res.statusCode === 401 || res.statusCode === 403){
          // 清除登录相关内容 处理401授权认证错误
          try {
            Taro.removeStorageSync('Cookies');
            // Taro.removeStorageSync('userInfo');
          } catch (e) {
            // Do something when catch error
          }

          reject({
            statusCode: res.statusCode,
            error: res.data.error
          });
        } else if(res.statusCode === 500) {
          reject({
            statusCode: res.statusCode,
            error: res.data.error
          });
        } else {
          reject({
            statusCode: res.statusCode,
            error: res
          })
        }

      },
      fail: function(err) {
        reject(err)
      },
      complete: function () {
        // Taro.hideLoading();
        // Taro.hideNavigationBarLoading();
      }
    })
  });
}

request.get = (url, data) => {
  return request(url, data, 'GET');
}

request.post = (url, data) => {
  return request(url, data, 'POST');
}

request.mini_post = (url, data) => {
  return request(url, data, 'POST', 'application/x-www-form-urlencoded');
}
export default request;
