import Taro from '@tarojs/taro';

export function dateFormat(fmt, timestamp) {
  let date = new Date(timestamp);
  let ret;
  const opt = {
    'Y+': date.getFullYear().toString(),        // 年
    'm+': (date.getMonth() + 1).toString(),     // 月
    'd+': date.getDate().toString(),            // 日
    'H+': date.getHours().toString(),           // 时
    'M+': date.getMinutes().toString(),         // 分
    'S+': date.getSeconds().toString()          // 秒
    // 有其他格式化字符需求可以继续添加，必须转化成字符串
  };
  for (let k in opt) {
    ret = new RegExp('(' + k + ')').exec(fmt);
    if (ret) {
      fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, '0')));
    }
  }
  return fmt;
}

export function dateCN(str) {
  const date = new Date(str);
  const month = date.getMonth() + 1;
  const dt = date.getDate();
  return month + '月' + dt + '日';
}

export function distanceCN(decimal) {
  const fl = parseFloat(decimal.toString());
  return fl < 1 ? (fl * 1000).toFixed() + '米' : fl + '公里';
}

export function checkMallOpen() {
  const date = new Date();
  const hour = date.getHours();
  const start = hour >= 10;
  let end = hour < 22;
  const minute = date.getMinutes();
  if(hour === 22) {
    end = minute === 0;
  }
  return start && end;
}

export function timeDiff(endDate) {
  // let endTime = new Date(endDate + ' 23:59:59').getTime();
  let endTime = new Date(endDate).getTime() + 86400000 - 1000;
  let currentTime = new Date().getTime();
  let diff = (endTime - currentTime) / 1000;
  return diff / (24 * 60 * 60);
}

export function handleMoneyString(num) {
  let integer = parseFloat(num);
  return 'S$' + integer;
}

export function handleDiscountString(decimal) {
  decimal = decimal || '';
  if (decimal.toString().indexOf('.') >= 0) {
    let size = decimal.toString().split('.')[1].length;
    return size > 1 ? (decimal * 100).toFixed() + '折' : (decimal * 10).toFixed() + '折';
  } else {
    return '';
  }
}
