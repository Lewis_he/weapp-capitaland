// 请求连接前缀
export const baseUrl = process.env.NODE_ENV === 'production' ? 'https://www.uniweb-marketing.com' : 'http://localhost:5555';

// 输出日志信息
export const noConsole = false;

// 以下是业务服务器API地址
// 局域网测试使用
// var WxApiRoot = 'http://192.168.1.102:8080/wx/';
// 云平台部署时使用
let EndPoints =
  process.env.NODE_ENV === 'production' ? 'https://www.uniwebmarketing.com/capitaland-server/' : 'http://localhost:5555/capitaland-server/';
EndPoints = 'https://www.uniwebmarketing.com/capitaland-server/';

let GatewayEndpoint = 'https://gateway.uniwebpay.com/api/gateway/';

export default {
  // Uniweb --
  UniwebPayUrl: GatewayEndpoint + 'pay',
  UniwebQueryUrl: GatewayEndpoint + 'query',
  UniwebRefundUrl: GatewayEndpoint + 'refund',
  UniwebCancelUrl: GatewayEndpoint + 'cancel',
  // -- Uniweb
  ConfigSearch: EndPoints + 'api/wx/couponConfigs/search',
  MerchantConfig: EndPoints + 'api/wx/merchants/search',
  Banners: EndPoints + 'api/wx/banners/content', //首页轮播数据
  CouponDetail: EndPoints + 'api/wx/couponConfigs', //根据couponId查询当前优惠券详情

  ActiveMerchant: EndPoints + 'api/wx/merchants/byCouponConfig',
  OtherOffer: EndPoints + 'api/wx/couponConfigs/byMerchant',

  PlaceOrder: EndPoints + 'api/wx/orders/placeOrder', // 领取,下单,购买优惠券
  CancelOrder: EndPoints + 'api/wx/orders/cancelOrder', // 取消订单
  CouponOrderPaid: EndPoints + 'api/wx/orders/orderPaid', // 支付成功通知后台
  CouponByOrder: EndPoints + 'api/wx/orders', // user根据订单号获取优惠券
  QRCode: EndPoints + 'api/wx/coupons', // 根据coupon ID获取QR
  MerchantSiblings: EndPoints + 'api/wx/merchants', // 根据

  CouponList: EndPoints + 'api/wx/mine/couponDetails', // 我的优惠券列表

  Tags: EndPoints + 'api/tags',

  Events: EndPoints + 'api/wx/events',
  PlaceGroupOrder: EndPoints + 'api/wx/orders/placeGroupOrder', // 领取,购买优惠券
  CancelGroupOrder: EndPoints + 'api/wx/orders/cancelGroupOrder', // 取消订单
  GroupOrderPaid: EndPoints + 'api/wx/orders/groupOrderPaid', // 支付成功通知后台

  PollingStatus: EndPoints + 'api/wx/mine/coupon', //轮询卡券核销状态

  DeleteCoupon: EndPoints + 'api/wx/mine/archive/add',

  // Capitaland
  AuthLoginByWechat: EndPoints + 'wxLogin',
  AuthUserInfo: EndPoints + 'api/users/me',
  AuthUserInfoUpdate: EndPoints + 'api/users/updateInfo',

  GBSearch: EndPoints + 'api/mp/globalSearch',
  Category: EndPoints + 'api/mp/categories',

  ExploreSearch: EndPoints + 'api/mp/featured',
  BannerSearch: EndPoints + 'api/mp/banners',
  OutletSearch: EndPoints + 'api/mp/outlets/search',
  Outlet: EndPoints + 'api/mp/outlets/',
  AddFavorite: EndPoints + 'api/mp/favourites/add',
  RemoveFavorite: EndPoints + 'api/mp/favourites/remove',
  GetFavorites: EndPoints + 'api/wx/mine/favorite/merchants',

  EventSearch: EndPoints + 'api/mp/events/search',
  Event: EndPoints + 'api/mp/events/',

  MallSearch: EndPoints + 'api/mp/malls',
  LovedSearch: EndPoints + 'api/mp/favourites/outlets'
};
