import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { View, Text, Image, ScrollView, Swiper, SwiperItem } from '@tarojs/components';
import './index.scss';
import * as images from '../../static/images/index';
import { SwiperCardLoading } from '../../components/SwiperCard/SwiperCardLoading';
import { MallCardLLoading } from '../../components/MallCard/MallCardLLoading';
import { MallCardMLoading } from '../../components/MallCard/MallCardMLoading';
import { EventCardLoading } from '../../components/EventCard/EventCardLoading';
import { NearbyCardLoading } from '../../components/NearbyCard/NearbyCardLoading';
import { BrandCardLLoading } from '../../components/BrandCard/BrandCardLLoading';
import { BrandCardMLoading } from '../../components/BrandCard/BrandCardMLoading';
import { EventCard } from '../../components/EventCard/EventCard';
import { MallCardL } from '../../components/MallCard/MallCardL';
import { global } from '../../../global.data';
import { SwiperCard } from '../../components/SwiperCard';
import { NearbyCard } from '../../components/NearbyCard/NearbyCard';
import { connect } from 'react-redux';
import { StateType } from '../../models/accountModel';
import { ConnectProps, ConnectState } from '../../models/connect';
import { BrandCard } from '../../components/ResponsiveCard/BrandCard';
import { dateCN, distanceCN } from '../../utils/util';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;
@connect(({ tabs, loading }: ConnectState) => ({
  ...tabs,
  ...loading
}))
class Index extends React.Component<IProps, OwnState> {
  lastPos = 0;
  state = {
    currentItemId: 0,
    showTab: true,
    triggered: false,
    open: false,
    hasMore: true,
    pageNo: 0,
    oncePageNo: -1
  };

  componentWillMount() {}

  componentDidShow() {
    // @ts-ignore
    if (typeof getCurrentInstance().page.getTabBar === 'function') {
      // @ts-ignore
      const tabBar = getCurrentInstance().page.getTabBar();
      if (tabBar) {
        tabBar.setData({ selected: 0 });
      }
    }
    Taro.setStorageSync('opBackTo', false);
  }

  componentDidMount() {
    this.getSwiperData();
    this.getExploreData();
    const _this = this;
    // 用户位置信息授权
    Taro.getSetting({
      success: function(res) {
        if (!res.authSetting['scope.userLocation']) {
          Taro.authorize({
            scope: 'scope.userLocation',
            success: function() {
              _this.fetchDataWithGeo();
            },
            fail: function(err) {
              // 处理用户拒绝后的引导...
              if (err.errMsg === 'authorize:fail auth deny') {
                // console.log('firstly get userLocation fail', errMsg);
                Taro.showToast({
                  title: '定位失败，请重试。',
                  icon: 'none',
                  duration: 1500
                });
              } else {
                // console.log('other time get userLocation fail', errMsg);
                _this.openSettingToAuthorize();
              }
            }
          });
        } else {
          _this.fetchDataWithGeo();
        }
      }
    });
  }

  componentWillUnmount() {}

  componentDidHide() {}

  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    return {
      title: '新加坡凯德商场',
      path: '/pages/explore/index'
      // imageUrl: images.CGWL
    };
  }

  openSettingToAuthorize = () => {
    const that = this;
    Taro.showModal({
      title: '温馨提示',
      confirmText: '去授权',
      content: '您没有授权地理位置信息，是否授权?',
      success: function({ confirm }) {
        if (confirm) {
          Taro.openSetting({
            success: function(res) {
              // console.log('other time userL openSetting ok: ', res.authSetting)//true ,false
              if (res.authSetting['scope.userLocation']) {
                that.fetchDataWithGeo();
              } else {
                Taro.showToast({
                  title: '授权失败，请重试',
                  icon: 'none',
                  duration: 1000
                });
              }
            }
          });
        } else {
          Taro.showToast({
            title: '定位失败，请重试',
            icon: 'none',
            duration: 1000
          });
        }
      }
    });
  };

  fetchDataWithGeo = () => {
    console.log('fetching data with GEO...');
    const that = this;
    Taro.getLocation({
      type: 'wgs84',
      success: function(result) {
        const longitude = result.longitude;
        const latitude = result.latitude;
        const local = { longitude, latitude };
        Taro.setStorageSync('location', local);
        that.getExploreData();
      }
    });
  };

  getExploreData = () => {
    const location = Taro.getStorageSync('location');
    const payload = {};
    if (location.latitude) payload['latitude'] = location.latitude;
    if (location.longitude) payload['longitude'] = location.longitude;
    this.props.dispatch({
      type: 'tabs/search',
      payload: payload
    });
  };

  getSwiperData = () => {
    this.props.dispatch({
      type: 'tabs/getBanners'
    });
  };

  onSwiperChange = (e) => {
    this.setState({ currentItemId: e.detail.current });
  };

  onPageScroll = (e) => {
    let y = e.detail.deltaY;
    // @ts-ignore
    const tabBar = getCurrentInstance().page.getTabBar();
    if (y < 0 && this.lastPos >= 0) {
      // console.log('scroll down!', this.lastPos);
      if (tabBar) {
        tabBar.setData({ show: false });
      }
      this.setState({ showTab: false });
    } else if (y > 0 && this.lastPos < 0) {
      // console.log('scroll up!', this.lastPos);
      if (tabBar) {
        tabBar.setData({ show: true });
      }
      this.setState({ showTab: true });
    }
    this.lastPos = y;
  };

  renderExploreCard = () => {
    const { explore } = this.props;
    return explore.map((item) => {
      let el;
      if (item.entityType == 'MALL') {
        const bg = global.live + item.thumbnail?.id;
        const logo = global.live + item.logo.id;
        const distance = item.distance ? distanceCN(item.distance) : '暂无距离';
        const title = (item.titleCn ? item.titleCn + ' ' : '') + item.title;
        const description = item.description;
        el = (
          <MallCardL
            margin="0 auto 22px"
            bg={bg}
            logo={logo}
            distance={distance}
            title={title}
            desc={description}
            onClick={() => {
              Taro.navigateTo({ url: `/subpages/mall/index?id=${item.id}` });
            }}
          />
        );
      } else if (item.entityType == 'EVENT') {
        const logo = global.live + item.thumbnail.id;
        const title = item.title;
        const date = dateCN(item.startDate) + '  -  ' + dateCN(item.endDate);
        let locale;
        if (item.malls.length > 1) {
          locale = '多个商场';
        } else {
          locale = (item.malls?.[0]?.titleCn ? item.malls[0].titleCn + ' ' : '') + item.malls?.[0]?.title;
        }
        el = (
          <EventCard
            margin="0 auto 22px"
            logo={logo}
            title={title}
            date={date}
            location={locale}
            onClick={() => {
              Taro.navigateTo({ url: `/subpages/event/index?id=${item.id}` });
            }}
          />
        );
      } else if (item.entityType == 'OUTLET') {
        const dgc: string[] = [];
        const pay: string[] = [];
        const tag = (item.category?.titleCn ?? '') + (item.subCategory?.titleCn ? ' / ' + item.subCategory?.titleCn : '');
        const mallName = (item.mall?.titleCn ? item.mall.titleCn + ' ' : '') + item.mall?.title;
        const title = (item?.brand.titleCn ? item.brand.titleCn + ' ' : '') + item?.brand.title;
        const logo = item.brand.logo ? global.live + item.brand.logo.id : images.CGWL;
        const desc = item.brand?.description;
        if (item.wechatPay) {
          pay.push('WeChatPay');
        }
        if (item.aliPay) {
          pay.push('Alipay');
        }
        if (item.grabPay) {
          pay.push('GrabPay');
        }
        if (item.capitaVoucher) {
          dgc.push('CapitaVoucher');
        }
        if (item.capitaCard) {
          dgc.push('CapitaCard');
        }
        if (item.eCapitaVoucher) {
          dgc.push('eCapitaVoucher');
        }
        if (item.starXtra) {
          dgc.push('STARXtra');
        }
        el = (
          <BrandCard
            margin="0 auto 22px"
            logo={logo}
            title={title}
            desc={desc}
            dgc={dgc}
            payment={pay}
            category={tag}
            mall={mallName}
            unit={item?.storeCode}
            onClick={() => Taro.navigateTo({ url: `/subpages/brand/index?oid=${item.id}&bid=${item.brand.id}` })}
          />
        );
      } else if (item.entityType == 'PLACE') {
        const bg = global.live + item.thumbnail?.id;
        const distance = item.distance ? distanceCN(item.distance) : '暂无距离';
        const title = (item.titleCn ? item.titleCn + ' ' : '') + item.title;
        const description = item.description;
        el = (
          <NearbyCard
            margin="0 auto 22px"
            bg={bg}
            distance={distance}
            title={title}
            desc={description}
            onClick={() => item.link && this.jump2OA(item.link)}
          />
        );
      }
      return el;
    });
  };

  renderCardsLoading = () => {
    let loaders: number[] = [0, 1, 2, 3, 4, 5];
    // for (let i = 0; i < 10; i++) {
    //   let number = Math.floor(Math.random() * 7);
    //   loaders.push(number);
    // }
    return loaders.map((item) => {
      let el;
      if (item == 0) {
        el = <MallCardLLoading margin="0 auto 22px" />;
      } else if (item == 1) {
        el = <MallCardMLoading margin="0 auto 22px" />;
      } else if (item == 2) {
        el = <EventCardLoading margin="0 auto 22px" />;
      } else if (item == 3) {
        el = <NearbyCardLoading margin="0 auto 22px" />;
      } else if (item == 4) {
        el = <BrandCardLLoading margin="0 auto 22px" />;
      } else if (item == 5) {
        el = <BrandCardMLoading margin="0 auto 22px" />;
      }
      return el;
    });
  };

  upperLoadMore = (e) => {
    let { hasMore, pageNo, oncePageNo } = this.state;
    if (!hasMore || oncePageNo == pageNo) {
      return;
    }

    this.setState({ open: true, oncePageNo: pageNo });

    pageNo++;

    const local = Taro.getStorageSync('location');
    const payload = { page: pageNo };
    if (local.latitude) payload['latitude'] = local.latitude;
    if (local.longitude) payload['longitude'] = local.longitude;
    this.props
      .dispatch({
        type: 'tabs/search',
        payload: payload
      })
      .then((res) => {
        this.setState({ pageNo, open: false, hasMore: res });
      });
  };

  refresherRefresh = () => {
    this.setState({ triggered: true });
    this.getNewGEOInfo();
  };

  getNewGEOInfo = () => {
    const that = this;
    Taro.getSetting({
      success: function(res) {
        if (res.authSetting['scope.userLocation']) {
          Taro.getLocation({
            type: 'wgs84',
            success: function(result) {
              const longitude = result.longitude;
              const latitude = result.latitude;
              const local = { longitude, latitude };
              Taro.setStorageSync('location', local);
            }
          });
        }
      },
      complete: function(res) {
        that.fetchBannersNoCache();
        that.fetchExploreNoCache();
      }
    });
  };

  fetchBannersNoCache = () => {
    this.props.dispatch({
      type: 'tabs/getBanners',
      payload: {
        refresh: true
      }
    });
  };

  fetchExploreNoCache = () => {
    const local = Taro.getStorageSync('location');
    const payload = { refresh: true };
    if (local.latitude) payload['latitude'] = local.latitude;
    if (local.longitude) payload['longitude'] = local.longitude;
    this.props
      .dispatch({
        type: 'tabs/search',
        payload: payload
      })
      .then(() => {
        this.setState({ triggered: false, pageNo: 0, hasMore: true, oncePageNo: -1 });
      });
  };

  jump2OA = (link) => {
    Taro.navigateTo({ url: `/subpages/oa/article?url=${link}` });
  };

  navigate2Mp = () => {
    Taro.navigateToMiniProgram({
      appId: 'wx887a6fc1e95cd883',
      path: '/pages/actpage/actpage?actId=b68e3877-bba0-4d50-9959-d0ff3296c3ae',
      success: function(res) {
        // 打开成功
      }
    });
  };

  render() {
    const { explore, banners } = this.props;
    const { currentItemId, triggered, open, hasMore } = this.state;
    const loaders = ['red', 'blue', 'green'];
    return (
      <View className="explore-index">
        {/*status bar*/}
        <View className="status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
        {/*nav bar*/}
        <View className="nav-bar">
          <Text className="nav-bar-title">新加坡凯德商场</Text>
        </View>
        <ScrollView
          className="explore-sv"
          scrollY
          scrollWithAnimation
          refresherEnabled
          refresherTriggered={triggered}
          // refresherBackground="#2e58ff"
          // refresherDefaultStyle="white"
          onRefresherRefresh={this.refresherRefresh}
          onScrollToLower={this.upperLoadMore}
        >
          <Image className="oval-mask" src={images.ABS} mode="aspectFill">
            <View className="search-bar" onClick={() => Taro.navigateTo({ url: '/subpages/search/index' })}>
              <View className="sb-icon" />
              <View className="sb-placeholder">搜索品牌/商户/活动/优惠…</View>
            </View>
          </Image>
          <Image src={images.COS} className="explore-nearby">
            <View className="explore-icon" />
            <Text className="explore-txt">发现附近</Text>
          </Image>
          <View className="cover-view">
            <Swiper
              className="sp"
              previousMargin="50px"
              nextMargin="50px"
              interval={2500}
              duration={500}
              circular
              autoplay
              onChange={this.onSwiperChange}
            >
              {banners.length > 0 && explore.length > 0
                ? banners.map((_item, index) => {
                    return (
                      <SwiperItem className="demo-sp-1" onClick={() => this.jump2OA(_item.link)}>
                        <SwiperCard scale={index == currentItemId ? 1 : 0.9} image={global.live + _item.thumbnail.id} />
                      </SwiperItem>
                    );
                  })
                : loaders.map((item, index) => {
                    return (
                      <SwiperItem className="demo-sp-1">
                        <SwiperCardLoading scale={index == currentItemId ? 1 : 0.9} />
                      </SwiperItem>
                    );
                  })}
            </Swiper>
            {/*<Image*/}
            {/*  className="event-wechatpay"*/}
            {/*  src={images.PWP}*/}
            {/*  onClick={() => Taro.navigateTo({ url: `/subpages/promotion/index` })}*/}
            {/*></Image>*/}

            {/*<ScrollView scrollX className="bsp">*/}
            {/*  <Image src={images.GBP} className="bsp-image" onClick={() => Taro.navigateTo({ url: `/subpages/promotion/index?id=2` })} />*/}
            {/*  <Image src={images.RBP} className="bsp-image" onClick={this.navigate2Mp} />*/}
            {/*  <Image src={images.BBP} className="bsp-image" onClick={() => Taro.navigateTo({ url: `/subpages/promotion/index?id=0` })} />*/}
            {/*</ScrollView>*/}
            {banners.length > 0 && explore.length > 0 ? this.renderExploreCard() : this.renderCardsLoading()}

            {hasMore ? (
              <van-loading customClass={open ? 'ep-loading' : 'ep-loading dismiss'} type="spinner" vertical={true}>
                加载中...
              </van-loading>
            ) : (
              <View className="ep-loading">没有更多了</View>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}
export default Index as ComponentClass<OwnProps>;
