export default {
  navigationBarTitleText: '账户',
  navigationStyle: 'custom',
  enableShareAppMessage: true,
  usingComponents: {
    'van-toast': '../../components/vant-weapp/dist/toast/index',
    'van-popup': '../../components/vant-weapp/dist/popup/index'
  }
};
