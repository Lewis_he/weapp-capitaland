import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { Button, Image, View } from '@tarojs/components';
import Toast from '../../components/vant-weapp/dist/toast/toast';

import { connect } from 'react-redux';
import { StateType } from '../../models/accountModel';

import { ConnectProps, ConnectState } from '../../models/connect';
import './index.scss';
import * as images from '../../static/images/index';
import * as user from '../../utils/user';
import { get as getGlobalData, global, set as setGlobalData } from '../../../global.data';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;
@connect(({ account, loading }: ConnectState) => ({
  ...account,
  ...loading
}))
class Me extends React.Component<IProps, OwnState> {
  state = {
    login: false,
    show: false,
    userInfo: {
      nickName: '新加坡凯德商场用户',
      avatarUrl: images.CGWL
    }
  };

  componentDidMount() {
    // console.log(this.props.accountState);
  }
  componentDidShow() {
    // @ts-ignore
    if (typeof getCurrentInstance().page.getTabBar === 'function') {
      // @ts-ignore
      const tabBar = getCurrentInstance().page.getTabBar();
      if (tabBar) {
        tabBar.setData({ selected: 3 });
      }
    }
    console.log('me has login: ', getGlobalData('hasLogin'));
    if (getGlobalData('hasLogin')) {
      let userInfo = Taro.getStorageSync('userInfo');
      this.setState({
        userInfo: userInfo,
        login: true
      });
    } else {
      this.setState({
        login: false
      });
    }
  }

  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    return {
      title: '新加坡凯德商场',
      path: '/pages/me/index'
    };
  }

  wxLogin = () => {
    Taro.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        user
          .checkLogin()
          .then(() => {
            console.log('storage has login, check ss 0...');
            setGlobalData('hasLogin', true);
            this.setState({ login: true });
          })
          .catch(() => {
            user
              .loginByWeixin(res.userInfo)
              .then(() => {
                setGlobalData('hasLogin', true);
                Toast.success('微信登录成功');
                this.setState({ login: true, userInfo: res.userInfo });
              })
              .catch(() => {
                setGlobalData('hasLogin', false);
                Toast.fail('微信登录失败');
              });
          });
      },
      fail: () => {
        setGlobalData('hasLogin', false);
        Toast.fail('微信登录失败');
      }
    });
  };

  showPopup = () => {
    this.setState({ show: true });
    setTimeout(() => {
      // @ts-ignore
      const tabBar = getCurrentInstance().page.getTabBar();
      if (tabBar) tabBar.setData({ cover: true });
    }, 200);
  };

  closePopup = () => {
    this.setState({ show: false });
    setTimeout(() => {
      // @ts-ignore
      const tabBar = getCurrentInstance().page.getTabBar();
      if (tabBar) tabBar.setData({ cover: false });
    }, 200);
  };

  render() {
    const {} = this.props;
    const { login, show, userInfo } = this.state;
    return (
      <View className="account-page">
        <van-toast id="van-toast" />
        <van-popup customStyle="background-color: transparent; text-align: center" show={show}>
          <Image className="ap-popup" src={global.url + 'popup.png'} />
          <Image className="ap-close" src={images.CLOSE} onClick={this.closePopup} />
        </van-popup>
        <View className="ap-head">
          <Image className="ap-head-bg" src={global.url + 'account-bg.png'} />
          <View className="ap-head-nav" style={{ top: Taro.$statusBarHeight + 'px' }}>
            新加坡凯德商场
          </View>
          <View className="ap-head-avatar-wrap">
            <Image src={login ? userInfo.avatarUrl : images.CGWL} className="ap-avatar" />
          </View>
          {login && <View className="ap-head-username">{userInfo.nickName}</View>}
        </View>

        <View className="ap-panel">
          {login && (
            <View className="ap-panel-loved" onClick={() => Taro.navigateTo({ url: '/subpages/love/index' })}>
              <Image className="ap-panel-head-icon" src={images.ALO} />
              收藏的商户
              <Image className="ap-panel-tail-icon" src={images.MR} />
            </View>
          )}
          {login && (
            <View className="ap-panel-vouchers" onClick={this.showPopup}>
              <Image className="ap-panel-head-icon" src={images.AVC} />
              我的优惠券
              <Image className="ap-panel-tail-icon" src={images.MR} />
            </View>
          )}

          <View className="ap-panel-qa" onClick={this.showPopup}>
            <Image className="ap-panel-head-icon" src={images.AQA} />
            常见问题
            <Image className="ap-panel-tail-icon" src={images.MR} />
          </View>
          {/*<Button className="ap-panel-feedback" openType="contact">*/}
          {/*  <Image className="ap-panel-head-icon" src={images.AFB} />*/}
          {/*  使用问题反馈*/}
          {/*  <Image className="ap-panel-tail-icon" src={images.MR} />*/}
          {/*</Button>*/}
        </View>

        {!login && (
          <Button className="ap-login" type="primary" onClick={this.wxLogin}>
            微信登录
          </Button>
        )}
        <Image className="ap-footer" src={images.PBU} />
      </View>
    );
  }
}

export default Me as ComponentClass<OwnProps>;
