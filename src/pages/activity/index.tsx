import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { Image, Picker, ScrollView, Text, View } from '@tarojs/components';

import { connect } from 'react-redux';
import { StateType } from '../../models/accountModel';

import { ConnectProps, ConnectState } from '../../models/connect';
import './index.scss';
import * as images from '../../static/images';
import { EventCard } from '../../components/EventCard/EventCard';
import { global } from '../../../global.data';
import { dateCN } from '../../utils/util';
import { EventCardLoading } from '../../components/EventCard/EventCardLoading';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}

interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;

@connect(({ event, loading }: ConnectState) => ({
  ...event,
  ...loading
}))
class Activity extends React.Component<IProps, OwnState> {
  lastPos = 0;
  state = {
    showTab: true,
    triggered: false,
    open: false,
    hasMore: true,
    pageNo: 0,
    oncePageNo: -1,
    mallSelector: [
      { id: undefined, title: '所有商场', cn: '所有商场' },
      { id: 6, title: '来福士城 Raffles City', cn: '来福士城' },
      { id: 12, title: '白沙浮广场 Bugis Junction', cn: '白沙浮广场' },
      { id: 13, title: '白沙浮娱乐广场 Bugis+', cn: '白沙浮娱乐广场' },
      { id: 18, title: '福南 Funan', cn: '福南' },
      { id: 9, title: '克拉码头 Clarke Quay', cn: '克拉码头' },
      { id: 3, title: '奥特莱斯 IMM', cn: '奥特莱斯' },
      { id: 15, title: '狮城大厦 Plaza Singapura', cn: '狮城大厦' }
    ],
    mallChecked: { id: undefined, title: '所有商场', cn: '所有商场' },
    typeSelector: [
      { type: undefined, title: '所有类别' },
      { type: 'EVENT', title: '活动' },
      { type: 'DEAL', title: '优惠' }
    ],
    typeChecked: { type: undefined, title: '所有类别' }
  };

  componentDidMount() {
    this.getEvents();
  }

  componentDidShow() {
    // @ts-ignore
    if (typeof getCurrentInstance().page.getTabBar === 'function') {
      // @ts-ignore
      const tabBar = getCurrentInstance().page.getTabBar();
      if (tabBar) {
        tabBar.setData({ selected: 2 });
      }
    }
  }

  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    return {
      title: '新加坡凯德商场',
      path: '/pages/activity/index'
    };
  }

  getEvents = () => {
    this.props.dispatch({
      type: 'event/search',
      payload: {}
    });
  };

  onPageScroll = (e) => {
    let y = e.detail.deltaY;
    // @ts-ignore
    const tabBar = getCurrentInstance().page.getTabBar();
    if (y < 0 && this.lastPos >= 0) {
      if (tabBar) {
        tabBar.setData({ show: false });
      }
      this.setState({ showTab: false });
    } else if (y > 0 && this.lastPos < 0) {
      if (tabBar) {
        tabBar.setData({ show: true });
      }
      this.setState({ showTab: true });
    }
    this.lastPos = y;
  };

  onMallPickerChange = (e) => {
    const index = e.detail.value;
    const { mallSelector } = this.state;
    this.setState(
      {
        mallChecked: mallSelector[index],
        pageNo: 0,
        hasMore: true,
        oncePageNo: -1
      },
      () => {
        this.getEventsWithParams();
      }
    );
  };

  onTypePickerChange = (e) => {
    const index = e.detail.value;
    const { typeSelector } = this.state;
    this.setState(
      {
        typeChecked: typeSelector[index],
        pageNo: 0,
        hasMore: true,
        oncePageNo: -1
      },
      () => {
        this.getEventsWithParams();
      }
    );
  };

  getEventsWithParams = () => {
    const { mallChecked, typeChecked } = this.state;
    const payload = {};
    if (mallChecked.id) payload['mallId'] = mallChecked.id;
    if (typeChecked.type) payload['type'] = typeChecked.type;
    this.props.dispatch({
      type: 'event/search',
      payload: payload
    });
  };

  renderEventLoading = () => {
    let loaders: number[] = [];
    for (let i = 0; i < 10; i++) {
      loaders.push(i);
    }
    return loaders.map((item) => {
      return <EventCardLoading margin="0 auto 20px" />;
    });
  };

  renderEvents = () => {
    const { events } = this.props;
    return events.map((item) => {
      const logo = global.live + item.thumbnail?.id;
      const title = item?.title;
      const start = dateCN(item.startDate);
      const end = dateCN(item.endDate);
      const date = start + '  -  ' + end;
      let locale;
      if (item.malls.length > 1) {
        locale = '多个商场';
      } else {
        locale = (item.malls?.[0]?.titleCn ? item.malls[0].titleCn + ' ' : '') + item.malls?.[0]?.title;
      }
      return (
        <EventCard
          margin="0 auto 20px"
          logo={logo}
          title={title}
          date={date}
          location={locale}
          onClick={() => {
            Taro.navigateTo({ url: `/subpages/event/index?id=${item.id}` });
          }}
        />
      );
    });
  };

  upperLoadMore = (e) => {
    let { hasMore, pageNo, oncePageNo, mallChecked, typeChecked } = this.state;
    if (!hasMore || oncePageNo == pageNo) {
      return;
    }

    this.setState({ open: true, oncePageNo: pageNo });

    pageNo++;

    const payload = { page: pageNo };
    if (mallChecked.id) payload['mallId'] = mallChecked.id;
    if (typeChecked.type) payload['type'] = typeChecked.type;
    this.props
      .dispatch({
        type: 'event/search',
        payload: payload
      })
      .then((res) => {
        this.setState({ pageNo, open: false, hasMore: res });
      });
  };

  refresherRefresh = () => {
    const { mallChecked, typeChecked } = this.state;
    const payload = { refresh: true };
    if (mallChecked.id) payload['mallId'] = mallChecked.id;
    if (typeChecked.type) payload['type'] = typeChecked.type;
    this.setState({ triggered: true });
    this.props
      .dispatch({
        type: 'event/search',
        payload: payload
      })
      .then((res) => {
        this.setState({ triggered: false, pageNo: 0, hasMore: true, oncePageNo: -1 });
      });
  };

  render() {
    const { events } = this.props;
    const { showTab, mallSelector, mallChecked, typeSelector, typeChecked, triggered, open, hasMore } = this.state;
    return (
      <View className="activity-index">
        {/*status bar*/}
        <View className="status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
        {/*nav bar*/}
        <View className="nav-bar">
          <Text className="nav-bar-title">新加坡凯德商场</Text>
        </View>
        <ScrollView
          className="activity-sv"
          scrollY
          scrollWithAnimation
          refresherEnabled
          refresherTriggered={triggered}
          onRefresherRefresh={this.refresherRefresh}
          onScrollToLower={this.upperLoadMore}
        >
          <Image className="activity-oval" src={images.ALS} mode="aspectFill">
            <Picker className="left-all-mall" mode="selector" rangeKey="title" range={mallSelector} onChange={this.onMallPickerChange}>
              <View className="ap-picker">
                {mallChecked.cn}
                <View className="drop-down-icon" />
              </View>
            </Picker>

            <Picker className="right-all-category" mode="selector" rangeKey="title" range={typeSelector} onChange={this.onTypePickerChange}>
              <View className="ap-picker">
                {typeChecked.title}
                <View className="drop-down-icon" />
              </View>
            </Picker>
          </Image>
          <View className="activity-cover-view">
            {events.length > 0 ? this.renderEvents() : this.renderEventLoading()}
            {hasMore ? (
              <van-loading customClass={open ? 'ap-loading' : 'ap-loading dismiss'} type="spinner" vertical={true}>
                加载中...
              </van-loading>
            ) : (
              <View className="ap-loading">没有更多了</View>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Activity as ComponentClass<OwnProps>;
