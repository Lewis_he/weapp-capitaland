import React, { ComponentClass } from 'react';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import { Image, Picker, ScrollView, Text, View } from '@tarojs/components';

import { connect } from 'react-redux';
import { StateType } from '../../models/accountModel';

import { ConnectProps, ConnectState } from '../../models/connect';
import './index.scss';
import * as images from '../../static/images';
import { global } from '../../../global.data';
import { MallCardL } from '../../components/MallCard/MallCardL';
import { distanceCN } from '../../utils/util';
import { BugisTownCard } from '../../components/MallCard/BugisTown';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;
@connect(({ mall, loading }: ConnectState) => ({
  ...mall,
  ...loading
}))
class Mall extends React.Component<IProps, OwnState> {
  lastPos = 0;
  state = {
    showTab: true,
    triggered: false,
    selector: [
      { id: '', title: '所有商场', cn: '所有商场' },
      { id: 12, title: '白沙浮广场 Bugis Junction', cn: '白沙浮广场' },
      { id: 13, title: '白沙浮娱乐广场 Bugis+', cn: '白沙浮娱乐广场' },
      { id: 9, title: '克拉码头 Clarke Quay', cn: '克拉码头' },
      { id: 18, title: '福南 Funan', cn: '福南' },
      { id: 3, title: '奥特莱斯 IMM', cn: '奥特莱斯' },
      { id: 15, title: '狮城大厦 Plaza Singapura', cn: '狮城大厦' },
      { id: 6, title: '来福士城 Raffles City', cn: '来福士城' }
    ],
    selectorChecked: { id: '', title: '所有商场', cn: '所有商场' }
  };
  componentDidMount() {
    this.getAllMalls();
    const _this = this;
    Taro.authorize({
      scope: 'scope.userLocation',
      success: function() {
        // console.log('do have scope.userLocation');
        _this.fetchDataWithLBS();
      },
      fail: function(err) {
        // 处理用户拒绝后的引导...
        if (err.errMsg === 'authorize:fail auth deny') {
          // console.log('firstly get userLocation fail', errMsg);
          Taro.showToast({
            title: '定位失败，请重试。',
            icon: 'none',
            duration: 1500
          });
        } else {
          // console.log('other time get userLocation fail', errMsg);
          _this.openSettingToAuthorize();
        }
      }
    });
  }

  componentDidShow() {
    // @ts-ignore
    if (typeof getCurrentInstance().page.getTabBar === 'function') {
      // @ts-ignore
      const tabBar = getCurrentInstance().page.getTabBar();
      if (tabBar) {
        tabBar.setData({ selected: 1 });
      }
    }
  }

  onShareAppMessage(res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target);
    }
    return {
      title: '新加坡凯德商场',
      path: '/pages/mall/index'
    };
  }

  openSettingToAuthorize = () => {
    const that = this;
    Taro.showModal({
      title: '温馨提示',
      confirmText: '去授权',
      content: '您没有授权地理位置信息，是否授权?',
      success: function({ confirm }) {
        if (confirm) {
          Taro.openSetting({
            success: function(res) {
              // console.log('other time userL openSetting ok: ', res.authSetting)//true ,false
              if (res.authSetting['scope.userLocation']) {
                that.fetchDataWithLBS();
              } else {
                Taro.showToast({
                  title: '授权失败，请重试',
                  icon: 'none',
                  duration: 1000
                });
              }
            }
          });
        } else {
          Taro.showToast({
            title: '定位失败，请重试',
            icon: 'none',
            duration: 1000
          });
        }
      }
    });
  };

  fetchDataWithLBS = () => {
    console.log('fetching data with LBS...');
    const that = this;
    Taro.getLocation({
      type: 'wgs84',
      success: function(result) {
        const longitude = result.longitude;
        const latitude = result.latitude;
        const local = { longitude, latitude };
        Taro.setStorageSync('location', local);
        that.getAllMalls();
      }
    });
  };

  getAllMalls = () => {
    const location = Taro.getStorageSync('location');
    this.props.dispatch({
      type: 'mall/search',
      payload: {
        latitude: location.latitude || '',
        longitude: location.longitude || ''
      }
    });
  };

  getSpecificMall = (id) => {
    const location = Taro.getStorageSync('location');
    this.props.dispatch({
      type: 'mall/getMall',
      payload: {
        locale: {
          latitude: location.latitude || '',
          longitude: location.longitude || ''
        },
        id: id
      }
    });
  };

  onPageScroll = (e) => {
    let y = e.detail.deltaY;
    // @ts-ignore
    const tabBar = getCurrentInstance().page.getTabBar();
    if (y < 0 && this.lastPos >= 0) {
      if (tabBar) {
        tabBar.setData({ show: false });
      }
      this.setState({ showTab: false });
    } else if (y > 0 && this.lastPos < 0) {
      if (tabBar) {
        tabBar.setData({ show: true });
      }
      this.setState({ showTab: true });
    }
    this.lastPos = y;
  };

  onPickerChange = (e) => {
    const index = e.detail.value;
    this.setState(
      {
        selectorChecked: this.state.selector[index]
      },
      () => {
        index > 0 ? this.getSpecificMall(this.state.selectorChecked.id) : this.getAllMalls();
      }
    );
  };

  resetMallFilter = () => {
    this.setState(
      {
        selectorChecked: this.state.selector[0]
      },
      () => {
        this.getAllMalls();
      }
    );
  };

  renderMalls = () => {
    const { malls } = this.props;
    return malls.map((item) => {
      let el;
      if (item.id === 'bt') {
        const distance = item.distance ? distanceCN(item.distance) : '暂无距离';
        el = (
          <BugisTownCard
            margin="0 auto 20px"
            logo={images.BTL}
            desc="白沙浮广场，白沙浮娱乐广场和武吉士街共同构成了Bugis Town 。一个充满活力的生活方式目的地，这里提供无尽的美食，易负担的奢侈品，最潮流的视觉与娱乐场所。"
            distance={distance}
            siblings={item.siblings}
          />
        );
      } else {
        const bg = global.live + item.thumbnail?.id;
        const logo = global.live + item.logo?.id;
        const distance = item.distance ? distanceCN(item.distance) : '暂无距离';
        const title = (item.titleCn ? item.titleCn + ' ' : '') + item.title;
        const description = item.description;
        el = (
          <MallCardL
            margin="0 auto 20px"
            bg={bg}
            logo={logo}
            distance={distance}
            title={title}
            desc={description}
            onClick={() => {
              Taro.navigateTo({ url: `/subpages/mall/index?id=${item.id}` });
            }}
          />
        );
      }

      return el;
    });
  };

  refresherRefresh = () => {
    this.setState({ triggered: true });
    this.getNewLBSInfo();
  };

  getNewLBSInfo = () => {
    const that = this;
    Taro.getSetting({
      success: function(res) {
        if (res.authSetting['scope.userLocation']) {
          Taro.getLocation({
            type: 'wgs84',
            success: function(result) {
              const longitude = result.longitude;
              const latitude = result.latitude;
              const local = { longitude, latitude };
              Taro.setStorageSync('location', local);
            }
          });
        }
      },
      complete: function(res) {
        that.fetchMallsNoCache();
      }
    });
  };

  fetchMallsNoCache = () => {
    const local = Taro.getStorageSync('location');
    const payload = { refresh: true };
    if (local.latitude) payload['latitude'] = local.latitude;
    if (local.longitude) payload['longitude'] = local.longitude;
    this.props
      .dispatch({
        type: 'mall/search',
        payload: payload
      })
      .then(() => {
        this.setState({ triggered: false });
      });
  };

  render() {
    const { showTab, triggered, selector, selectorChecked } = this.state;
    return (
      <View className="mall-index">
        {/*status bar*/}
        <View className="status-bar" style={{ height: Taro.$statusBarHeight + 'px' }} />
        {/*nav bar*/}
        <View className="nav-bar">
          <View className="nav-bar-toggle">
            {/*{selectorChecked.cn == '所有商场' && <View className="mpick-icon" />}*/}
            {selectorChecked.cn == '所有商场' ? (
              <Picker mode="selector" rangeKey="title" range={selector} onChange={this.onPickerChange}>
                <View className="mp-picker-wrap">
                  <View className="mpick-icon" />
                  所有商场
                </View>
              </Picker>
            ) : (
              <View onClick={this.resetMallFilter}>重置筛选</View>
            )}
          </View>
          <Text className="nav-bar-title">新加坡凯德商场</Text>
        </View>
        <ScrollView
          className="mall-sv"
          scrollY
          scrollWithAnimation
          refresherEnabled
          refresherTriggered={triggered}
          onRefresherRefresh={this.refresherRefresh}
        >
          <Image className="oval-mask" src={images.ABS} mode="aspectFill">
            <View className="nearest-wrap">
              <View className="nearest-icon" />
              Nearest
            </View>
          </Image>
          <View className="mall-cover-view">{this.renderMalls()}</View>
        </ScrollView>
      </View>
    );
  }
}

export default Mall as ComponentClass<OwnProps>;
