import React from 'react';
import Taro from '@tarojs/taro';
import { Provider } from 'react-redux';
import dva from './utils/dva';
import models from './models';
import * as user from './utils/user';

import './app.scss';
import { set as setGlobalData } from '../global.data';

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }
const dvaApp = dva.createApp({
  initialState: {},
  models: models
});
const store = dvaApp.getStore();

class App extends React.Component {
  componentWillMount() {
    // const scene = Taro.getLaunchOptionsSync().scene;
    // scene != 1154 && this.versionUpdate();
    this.loadFontSrc();
    this.versionUpdate();
  }

  componentDidMount() {
    user
      .checkLogin()
      .then((res) => {
        setGlobalData('hasLogin', true);
      })
      .catch((err) => {
        // show401Toast();
        setGlobalData('hasLogin', false);
      });
  }

  componentDidShow() {
    try {
      const res = Taro.getSystemInfoSync();
      console.log(res.model);
      console.log(res.statusBarHeight);
      // Taro.$sensorHousing = res.windowHeight != res.safeArea.bottom;
      Taro.$statusBarHeight = res.statusBarHeight;
    } catch (e) {
      console.log('error when getSystemInfoSync');
    }

    Taro.getSetting({
      success: function(res) {
        if (!res.authSetting['scope.userLocation']) {
          Taro.removeStorageSync('location');
        }
      }
    });
  }

  componentDidHide() {}

  componentDidCatchError() {}

  loadFontSrc() {
    Taro.loadFontFace({
      global: true,
      family: 'TX',
      source: 'url("https://www.uniwebmarketing.com/fonts/TanXianWeiZhengDaHeiJianTi-1.ttf")'
    });
    Taro.loadFontFace({
      global: true,
      family: 'TXZH',
      source: 'url("https://www.uniwebmarketing.com/fonts/TanXianWeiZhengZhongHeiJianTi-1.ttf")'
    });
    Taro.loadFontFace({
      global: true,
      family: 'MF',
      source: 'url("https://www.uniwebmarketing.com/fonts/MFFangHei_Noncommercial-Regular.ttf")'
    });
  }

  versionUpdate() {
    if (process.env.TARO_ENV === 'weapp') {
      const updateManager = Taro.getUpdateManager();
      updateManager.onCheckForUpdate(function(res) {
        // 请求完新版本信息的回调
        console.log(res.hasUpdate);
      });
      updateManager.onUpdateReady(function() {
        Taro.showModal({
          title: '更新提示',
          content: '新版本已经准备好，是否重启应用？',
          success: function(res) {
            if (res.confirm) {
              // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
              updateManager.applyUpdate();
            }
          }
        });
      });
      updateManager.onUpdateFailed(function() {
        // 新的版本下载失败
      });
    }
  }

  render() {
    return <Provider store={store}>{this.props.children}</Provider>;
  }
}

export default App;
