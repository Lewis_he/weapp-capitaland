Component({
  data: {
    show: true,
    cover: false,
    selected: 0,
    selectedColor: '#5C7EFF',
    color: '#5E5E69',
    list: [
      {
        pagePath: '/pages/explore/index',
        iconPath: '../static/images/explore.png',
        selectedIconPath: '../static/images/explore-a.png',
        text: '发现'
      },
      {
        pagePath: '/pages/mall/index',
        iconPath: '../static/images/mall.png',
        selectedIconPath: '../static/images/mall-a.png',
        text: '商场'
      },
      {
        pagePath: '/pages/activity/index',
        iconPath: '../static/images/activity.png',
        selectedIconPath: '../static/images/activity-a.png',
        text: '活动'
      },
      {
        pagePath: '/pages/me/index',
        iconPath: '../static/images/me.png',
        selectedIconPath: '../static/images/me-a.png',
        text: '账户'
      }
    ]
  },
  attached() {
  },
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset;
      const url = data.path;
      wx.switchTab({ url });
      // this.setData({
      //   selected: data.index
      // });
    }
  }
});
